package middleware

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"net/http"
	"time"
)

// AuthUser used for get data from authenticated investor
var AuthUser models.Investor

// Auth used for middleware auth
func Auth(db *sql.DB, acl []string, req *http.Request) {
	var (
		serverMaintenance, serverAllowedRole sql.NullString
	)

	errServerMaintenance := db.QueryRow(`SELECT atn_servers.maintenance, atn_servers."allowedWhileMaintenance" FROM atn_servers WHERE atn_servers.name = $1`, "s1").Scan(&serverMaintenance, &serverAllowedRole)

	if errServerMaintenance != nil {
		panic(errServerMaintenance.Error())
	}

	if serverMaintenance.String == "1" {
		panic("Server sedang maintenance")
	}

	var (
		accessToken, accessTokenExpiry, UID, email, fullname, phone, deviceToken, role, isVerified sql.NullString
	)

	request := struct {
		Token string `validate:"required,max:200"`
	}{req.Header.Get("token")}

	errUserNotFound := db.QueryRow(`
	SELECT atn_users."accessToken", atn_users."accessTokenExpiry", atn_users."UID", atn_users.email, atn_users.fullname, atn_users.phone, atn_users."deviceToken", atn_users.role, atn_users."isVerified"
	FROM atn_users 
	WHERE 
		atn_users."accessToken" = $1 AND 
		atn_users."isDelete" = $2
	`, request.Token, utils.DefaultIsDelete).Scan(&accessToken, &accessTokenExpiry, &UID, &email, &fullname, &phone, &deviceToken, &role, &isVerified)

	if errUserNotFound == sql.ErrNoRows {

	} else if errUserNotFound != nil {
		panic(errUserNotFound.Error())
	}

	if accessToken.String == "" || len(accessToken.String) < 1 {
		panic("Silahkan login terlebih dahulu")
	}

	tokenExpiryDate, _ := time.Parse("2006-01-02 15:04:05", accessTokenExpiry.String)
	currentDatetime, _ := time.Parse("2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05"))

	if tokenExpiryDate.Before(currentDatetime) {
		panic(utils.CreateErrorResponse(419, "Sesi login telah berakhir, silahkan login kembali", "Sesi login telah berakhir, silahkan login kembali"))
	}

	isAllowed := false

	for _, val := range acl {
		if val == role.String {
			isAllowed = true

			break
		}
	}

	if !isAllowed {
		panic("Akses ditolak")
	}

	AuthUser = models.Investor{
		UID:               UID.String,
		Email:             email.String,
		Fullname:          fullname.String,
		Phone:             phone.String,
		AccessToken:       accessToken.String,
		AccessTokenExpiry: accessTokenExpiry.String,
		Role:              role.String,
		IsVerified:        isVerified.String,
	}
}
