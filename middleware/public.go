package middleware

import (
	"ayotani/utils"
	"database/sql"
)

// Public used for middleware public
func Public(db *sql.DB) {
	var (
		serverMaintenance, serverAllowedRole sql.NullString
	)

	err := db.QueryRow(`SELECT atn_servers.maintenance, atn_servers."allowedWhileMaintenance" FROM atn_servers WHERE atn_servers.name = $1 AND atn_servers."isDelete" = $2`, "s1", "0").Scan(&serverMaintenance, &serverAllowedRole)

	utils.HandleError(err, "")

	if serverMaintenance.String == "1" {
		panic("Server sedang maintenance")
	}
}
