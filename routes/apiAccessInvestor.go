package routes

import (
	"ayotani/controllers"
	"ayotani/utils"
	"database/sql"
	"net/http"

	"golang.org/x/time/rate"
)

// APIAccessInvestor used for handle all API that access data investor
func APIAccessInvestor(db *sql.DB) {
	const prefix = "/s/a/iv/api/v1"

	http.HandleFunc(prefix+"/login", controllers.PostIvLogin(db, "POST", utils.ACLPublic, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/logout", controllers.PostIvLogout(db, "POST", utils.ACLPublic, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/register", controllers.PostRegisterInvestor(db, "POST", utils.ACLPublic, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/reset-password", controllers.PostResetPassword(db, "POST", utils.ACLPublic, rate.NewLimiter(2, 2)))

	http.HandleFunc(prefix+"/change-password", controllers.PostChangePasswordInvestor(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/profile", controllers.GetProfileInvestor(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-profile", controllers.PostEditProfileInvestor(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/balance", controllers.GetBalanceInvestor(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/fund", controllers.GetInvestorSourceOfFund(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-fund", controllers.PostEditInvestorSourceOfFund(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/bank-account", controllers.GetInvestorBankAccount(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-bank-account", controllers.PostEditInvestorBankAccount(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/document", controllers.GetInvestorDocument(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-document", controllers.PostEditInvestorDocument(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/heir", controllers.GetInvestorHeir(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-heir", controllers.PostEditInvestorHeir(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/device-token", controllers.PostUpdateDeviceToken(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))

	// Request Account Verification
	http.HandleFunc(prefix+"/request-verification", controllers.PostRequestVerificationAccount(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))

	// Upload Topup Transfer
	http.HandleFunc(prefix+"/upload-topup-transfer", controllers.PostUploadTopupTransfer(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))

	// Withdrawals
	http.HandleFunc(prefix+"/withdraw", controllers.PostWithdraw(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))

	// Reference / Berita
	http.HandleFunc(prefix+"/reference", controllers.GetIvReference(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Product
	http.HandleFunc(prefix+"/product", controllers.GetIvProduct(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Product Activity
	http.HandleFunc(prefix+"/product-activity", controllers.GetIvProductActivity(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
}
