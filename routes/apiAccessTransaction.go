package routes

import (
	"ayotani/controllers"
	"ayotani/utils"
	"database/sql"
	"net/http"

	"golang.org/x/time/rate"
)

// APIAccessTransaction used for handle all API that handle transaction data
func APIAccessTransaction(db *sql.DB) {
	const prefix = "/s/a/trx/api/v1"

	http.HandleFunc(prefix+"/balance-log", controllers.BalanceLog(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/history", controllers.TransactionHistory(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/investment-purchase", controllers.InvestmentPurchase(db, "POST", utils.ACLInvestor, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/topup", controllers.Topup(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/share-investment-profit", controllers.PostShareInvestmentProfit(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
}
