package routes

import (
	"ayotani/controllers"
	"ayotani/utils"
	"database/sql"
	"net/http"

	"golang.org/x/time/rate"
)

// APIAccessMasterData used for handle all API that access master data
func APIAccessMasterData(db *sql.DB) {
	const prefix = "/s/a/admin-access/api/v1"

	// Admin
	http.HandleFunc(prefix+"/login", controllers.PostAdminLogin(db, "POST", utils.ACLPublic, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/logout", controllers.PostAdminLogout(db, "POST", utils.ACLPublic, rate.NewLimiter(2, 2)))

	// Data Unique Code
	http.HandleFunc(prefix+"/unique-code", controllers.GetUniqueCode(db, "GET", utils.ACLSuperadmin, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-unique-code", controllers.PostAddUniqueCode(db, "POST", utils.ACLSuperadmin, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-unique-code", controllers.PostDeleteUniqueCode(db, "POST", utils.ACLSuperadmin, rate.NewLimiter(2, 2)))

	// Data Admin
	http.HandleFunc(prefix+"/superadmin", controllers.GetSuperadmin(db, "GET", utils.ACLSuperadmin, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-superadmin", controllers.PostAddSuperadmin(db, "POST", utils.ACLSuperadmin, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-superadmin", controllers.PostEditSuperadmin(db, "POST", utils.ACLSuperadmin, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-superadmin", controllers.PostDeleteSuperadmin(db, "POST", utils.ACLSuperadmin, rate.NewLimiter(2, 2)))

	// Data Investor
	http.HandleFunc(prefix+"/investor", controllers.GetInvestor(db, "GET", utils.ACLGetInvestor, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/select2-investor", controllers.GetSelect2Investor(db, "GET", utils.ACLGetInvestor, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-investor", controllers.PostAddInvestor(db, "POST", utils.ACLPostAddInvestor, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-investor", controllers.PostEditInvestor(db, "POST", utils.ACLPostAddInvestor, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-investor", controllers.PostDeleteInvestor(db, "POST", utils.ACLPostAddInvestor, rate.NewLimiter(2, 2)))

	// Data Pekerjaan
	http.HandleFunc(prefix+"/job", controllers.GetJob(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-job", controllers.PostAddJob(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-job", controllers.PostEditJob(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-job", controllers.PostDeleteJob(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Source Of Funds
	http.HandleFunc(prefix+"/sof", controllers.GetSOF(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-sof", controllers.PostAddSOF(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-sof", controllers.PostEditSOF(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-sof", controllers.PostDeleteSOF(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Bank
	http.HandleFunc(prefix+"/bank", controllers.GetBank(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-bank", controllers.PostAddBank(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-bank", controllers.PostEditBank(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-bank", controllers.PostDeleteBank(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Partner
	http.HandleFunc(prefix+"/partner", controllers.GetPartner(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-partner", controllers.PostAddPartner(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-partner", controllers.PostEditPartner(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-partner", controllers.PostDeletePartner(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Reference / Berita
	http.HandleFunc(prefix+"/reference", controllers.GetReference(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-reference", controllers.PostAddReference(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-reference", controllers.PostEditReference(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-reference", controllers.PostDeleteReference(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Reference / Berita Images
	http.HandleFunc(prefix+"/reference-image", controllers.GetReferenceImage(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-reference-image", controllers.PostAddReferenceImage(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-reference-image", controllers.PostDeleteReferenceImage(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Product
	http.HandleFunc(prefix+"/product", controllers.GetProduct(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-product", controllers.PostAddProduct(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/edit-product", controllers.PostEditProduct(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-product", controllers.PostDeleteProduct(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Product Activity
	http.HandleFunc(prefix+"/product-activity", controllers.GetProductActivity(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/add-product-activity", controllers.PostAddProductActivity(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/delete-product-activity", controllers.PostDeleteProductActivity(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Data Request Verification Account
	http.HandleFunc(prefix+"/request-verification", controllers.GetRequestVerificationAccount(db, "GET", utils.ACLAS, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/action-request-verification", controllers.PostActionRequestVerificationAccount(db, "POST", utils.ACLAS, rate.NewLimiter(2, 2)))

	// Upload Topup Transfer
	http.HandleFunc(prefix+"/upload-topup-transfer", controllers.GetUploadTopupTransfer(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/action-upload-topup-transfer", controllers.PostActionUploadTopupTransfer(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))

	// Withdrawals
	http.HandleFunc(prefix+"/withdraw", controllers.GetWithdraw(db, "GET", utils.ACLAll, rate.NewLimiter(2, 2)))
	http.HandleFunc(prefix+"/action-withdraw", controllers.PostActionWithdraw(db, "POST", utils.ACLAll, rate.NewLimiter(2, 2)))
}
