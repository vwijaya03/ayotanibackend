package configs

import (
	"ayotani/utils"
	"database/sql"
	"fmt"
	"time"
)

// Open used for create connection to database
func Open() *sql.DB {
	const (
		host                 = "localhost"
		port                 = 5432
		user                 = "ayotani"
		password             = "KuKofWfVaoLcl1ufd0xxy6Ow9Q7HoUe"
		dbname               = "ayotanidb"
		dbDriver             = "postgres"
		dbMaxConnections     = 50
		dbMaxIdleConnections = 10
		dbIdleTime           = time.Minute * 30
	)

	fmt.Println("Checking connection to psql...")
	utils.Logger("Checking connection to psql...")

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, errPsqlInfo := sql.Open(dbDriver, psqlInfo)

	if errPsqlInfo != nil {
		panic(errPsqlInfo)
	}

	errDbPing := db.Ping()

	if errDbPing != nil {
		panic(errDbPing)
	}

	db.SetMaxOpenConns(dbMaxConnections)
	db.SetMaxIdleConns(dbMaxIdleConnections)
	db.SetConnMaxLifetime(dbIdleTime)

	//dt := time.Now()
	//currentDatetime := dt.Format("01-02-2006")
	// fmt.Println("Current date and time is: ", dt.String())
	// fmt.Println()

	// userBalance := models.UserBalance{
	// 	UID:       "UID123",
	// 	UserUID:   "UserUID123",
	// 	Balance:   0,
	// 	IsDelete:  "0",
	// 	CreatedAt: currentDatetime,
	// 	UpdatedAt: currentDatetime,
	// }

	// utils.Validate(userBalance)

	fmt.Println("Successfully connected!")
	utils.Logger("Successfully connected!")

	return db
}
