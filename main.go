package main

import (
	"ayotani/configs"
	"ayotani/routes"
	"ayotani/utils"
	"flag"
	"log"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

func init() {
	var tz string
	flag.StringVar(&tz, "timezone", "Asia/Jakarta", "timezone") // default to Indonesia Jakarta's timezone.
	flag.Parse()

	_ = os.Setenv("TZ", tz)
}

func main() {
	log.Println("Server started on: http://localhost:8080")
	utils.Logger("Server started on: http://localhost:8080")

	db := configs.Open()
	defer db.Close()

	const prefix = "/s/a"

	// http StripPrefix gunanya untuk remove exist /ktp/ lalu diganti dengan path assets/img/ktp
	ktpFileServe := http.FileServer(utils.NeuteredFileSystem{Fs: http.Dir("assets/img/ktp")})
	npwpFileServe := http.FileServer(utils.NeuteredFileSystem{Fs: http.Dir("assets/img/npwp")})
	topupTransferFileServe := http.FileServer(utils.NeuteredFileSystem{Fs: http.Dir("assets/img/topup-transfer")})
	referenceThumbnailFileServe := http.FileServer(utils.NeuteredFileSystem{Fs: http.Dir("assets/img/reference-thumbnail")})
	productThumbnailFileServe := http.FileServer(utils.NeuteredFileSystem{Fs: http.Dir("assets/img/product-thumbnail-image")})
	productActivityThumbnailFileServe := http.FileServer(utils.NeuteredFileSystem{Fs: http.Dir("assets/img/product-activity-thumbnail-image")})

	http.Handle(prefix+"/ktp/", http.StripPrefix(prefix+"/ktp/", ktpFileServe))
	http.Handle(prefix+"/npwp/", http.StripPrefix(prefix+"/npwp/", npwpFileServe))
	http.Handle(prefix+"/topup-transfer/", http.StripPrefix(prefix+"/topup-transfer/", topupTransferFileServe))
	http.Handle(prefix+"/ref-thumbnail/", http.StripPrefix(prefix+"/ref-thumbnail/", referenceThumbnailFileServe))
	http.Handle(prefix+"/product-thumbnail/", http.StripPrefix(prefix+"/product-thumbnail/", productThumbnailFileServe))
	http.Handle(prefix+"/product-activity-thumbnail/", http.StripPrefix(prefix+"/product-activity-thumbnail/", productActivityThumbnailFileServe))

	routes.APIAccessMasterData(db)
	routes.APIAccessInvestor(db)
	routes.APIAccessTransaction(db)

	http.ListenAndServe("localhost:8080", nil)
}
