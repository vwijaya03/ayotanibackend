package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
)

// GetOneUserDocument used for get one data document information from user
func GetOneUserDocument(db *sql.DB, userUID string, isDelete string) models.UserDocumentInformation {
	var (
		UID, ktp, npwp, ktpImg, npwpImg sql.NullString
		userDocument                    models.UserDocumentInformation
	)

	err := db.QueryRow(`SELECT "atn_userDocumentInformations"."UID", "atn_userDocumentInformations"."ktp", "atn_userDocumentInformations"."npwp", "atn_userDocumentInformations"."ktpImg", "atn_userDocumentInformations"."npwpImg" FROM "atn_userDocumentInformations" WHERE "atn_userDocumentInformations"."userUID" = $1 AND "atn_userDocumentInformations"."isDelete" = $2`, userUID, isDelete).Scan(&UID, &ktp, &npwp, &ktpImg, &npwpImg)

	utils.HandleError(err, "")

	userDocument = models.UserDocumentInformation{UID: UID.String, Ktp: ktp.String, Npwp: npwp.String, KtpImg: ktpImg.String, NpwpImg: npwpImg.String}

	return userDocument
}
