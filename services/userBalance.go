package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
)

// GetUserBalance used for get user balance
func GetUserBalance(db *sql.DB, userUID string, isDelete string) models.UserBalance {
	var (
		resultUID, resultUserUID sql.NullString
		balance                  sql.NullFloat64
		userBalance              models.UserBalance
	)

	err := db.QueryRow(`SELECT "atn_userBalance"."UID", "atn_userBalance"."userUID", "atn_userBalance"."balance" FROM "atn_userBalance" WHERE "atn_userBalance"."userUID" = $1 AND "atn_userBalance"."isDelete" = $2`, userUID, isDelete).Scan(&resultUID, &resultUserUID, &balance)

	utils.HandleError(err, "")

	userBalance = models.UserBalance{UID: resultUID.String, UserUID: resultUserUID.String, Balance: balance.Float64}

	return userBalance
}

// UpdateUserBalance used for update user balance wether + or -
func UpdateUserBalance(transaction *sql.Tx, userUID string, balance float64, isDelete string) {
	_, errUpdateUserBalance := transaction.Exec(`UPDATE "atn_userBalance" SET "balance" = $1 WHERE "userUID" = $2 AND "isDelete" = $3`, balance, userUID, isDelete)
	utils.HandleError(errUpdateUserBalance, "")
}
