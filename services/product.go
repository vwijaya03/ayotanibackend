package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataProduct used for get total data product
func GetTotalDataProduct(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT("atn_products"."UID") as totalData FROM "atn_products" WHERE "atn_products"."isDelete" = $1`, isDelete).Scan(&totalData)

	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredProduct used for get total data filtered product
func GetTotalDataFilteredProduct(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT("atn_products"."UID") as totalData FROM "atn_products" WHERE "atn_products"."title" like '%' || $1 || '%' AND "atn_products"."isDelete" = $2`, search, isDelete).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetProducts used for get data product
func GetProducts(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.Product {
	var (
		price, roi, lat, lng                                                                     sql.NullFloat64
		stock, totalStock, period                                                                sql.NullInt64
		resultUID, resultUserUID, resultPartnerUID, title, desc, thumbnail, available, createdAt sql.NullString
		dataProducts                                                                             []models.Product
		resultProducts                                                                           *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_products."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY atn_products."createdAt" DESC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `atn_products."isDelete"`, Value: `0`},
			{Key: `atn_products."title":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_products."UID", atn_products."userUID", atn_products."partnerUID", atn_products."title", atn_products."desc", atn_products."stock", atn_products."totalStock", atn_products."price", atn_products."roi", atn_products."period", atn_products."lat", atn_products."lng", atn_products."thumbnail", atn_products."available", to_char("atn_products"."createdAt", 'yyyy-mm-dd HH24:MI:SS') as createdAt
	FROM atn_products 
	WHERE %s`, queryBuilder)

	products, errGetProducts := db.Query(fullQuery, values...)
	utils.HandleError(errGetProducts, "")
	resultProducts = products

	for resultProducts.Next() {
		resultProducts.Scan(&resultUID, &resultUserUID, &resultPartnerUID, &title, &desc, &stock, &totalStock, &price, &roi, &period, &lat, &lng, &thumbnail, &available, &createdAt)

		decodedTitle, errDecodedTitle := url.QueryUnescape(title.String)
		decodedDesc, errDecodedDesc := url.QueryUnescape(desc.String)

		utils.HandleError(errDecodedTitle, "")
		utils.HandleError(errDecodedDesc, "")

		dataProducts = append(dataProducts, models.Product{UID: resultUID.String, PartnerUID: resultPartnerUID.String, Title: decodedTitle, Desc: decodedDesc, Stock: int(stock.Int64), TotalStock: int(totalStock.Int64), Price: price.Float64, Roi: roi.Float64, Period: int(period.Int64), Lat: lat.Float64, Lng: lng.Float64, Thumbnail: thumbnail.String, Available: available.String, CreatedAt: createdAt.String})
	}
	resultProducts.Close()

	return dataProducts
}

// GetProductsForAdmin used for get data product for admin
func GetProductsForAdmin(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.Product {
	var (
		price, roi, lat, lng                                                          sql.NullFloat64
		stock, totalStock, period                                                     sql.NullInt64
		resultUID, resultUserUID, resultPartnerUID, title, desc, thumbnail, available sql.NullString
		dataProducts                                                                  []models.Product
		resultProducts                                                                *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_products."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY atn_products."createdAt" DESC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `atn_products."isDelete"`, Value: `0`},
			{Key: `atn_products."title":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_products."UID", atn_products."userUID", atn_products."partnerUID", atn_products."title", atn_products."desc", atn_products."stock", atn_products."totalStock", atn_products."price", atn_products."roi", atn_products."period", atn_products."lat", atn_products."lng", atn_products."thumbnail", atn_products."available" 
	FROM atn_products 
	WHERE %s`, queryBuilder)

	products, errGetProducts := db.Query(fullQuery, values...)
	utils.HandleError(errGetProducts, "")
	resultProducts = products

	for resultProducts.Next() {
		resultProducts.Scan(&resultUID, &resultUserUID, &resultPartnerUID, &title, &desc, &stock, &totalStock, &price, &roi, &period, &lat, &lng, &thumbnail, &available)

		dataProducts = append(dataProducts, models.Product{UID: resultUID.String, PartnerUID: resultPartnerUID.String, Title: title.String, Desc: desc.String, Stock: int(stock.Int64), TotalStock: int(totalStock.Int64), Price: price.Float64, Roi: roi.Float64, Period: int(period.Int64), Lat: lat.Float64, Lng: lng.Float64, Thumbnail: thumbnail.String, Available: available.String})
	}
	resultProducts.Close()

	return dataProducts
}

// GetOneProduct used for get one data product
func GetOneProduct(db *sql.DB, productUID string, isDelete string) models.Product {
	var (
		price, roi, lat, lng                                                                     sql.NullFloat64
		stock, period                                                                            sql.NullInt64
		resultUID, resultUserUID, resultPartnerUID, title, desc, thumbnail, available, createdAt sql.NullString
		product                                                                                  models.Product
	)

	err := db.QueryRow(`SELECT atn_products."UID", atn_products."userUID", atn_products."partnerUID", atn_products."title", atn_products."desc", atn_products."stock", atn_products."price", atn_products."roi", atn_products."period", atn_products."lat", atn_products."lng", atn_products."thumbnail", atn_products."available", atn_products."createdAt" FROM atn_products WHERE atn_products."UID" = $1 AND atn_products."isDelete" = $2`, productUID, isDelete).Scan(&resultUID, &resultUserUID, &resultPartnerUID, &title, &desc, &stock, &price, &roi, &period, &lat, &lng, &thumbnail, &available, &createdAt)

	utils.HandleError(err, "")

	decodedTitle, errDecodedTitle := url.QueryUnescape(title.String)
	decodedDesc, errDecodedDesc := url.QueryUnescape(desc.String)

	utils.HandleError(errDecodedTitle, "")
	utils.HandleError(errDecodedDesc, "")

	product = models.Product{UID: resultUID.String, PartnerUID: resultPartnerUID.String, Title: decodedTitle, Desc: decodedDesc, Stock: int(stock.Int64), Price: price.Float64, Roi: roi.Float64, Period: int(period.Int64), Lat: lat.Float64, Lng: lng.Float64, Thumbnail: thumbnail.String, Available: available.String, CreatedAt: createdAt.String}

	return product
}

// GetOneProductForTransaction used for get one data product for transaction
func GetOneProductForTransaction(db *sql.DB, productUID string, isAvailable string, isDelete string) models.Product {
	var (
		price, roi, lat, lng                                                                     sql.NullFloat64
		stock, period                                                                            sql.NullInt64
		resultUID, resultUserUID, resultPartnerUID, title, desc, thumbnail, available, createdAt sql.NullString
		product                                                                                  models.Product
	)

	err := db.QueryRow(`SELECT atn_products."UID", atn_products."userUID", atn_products."partnerUID", atn_products."title", atn_products."desc", atn_products."stock", atn_products."price", atn_products."roi", atn_products."period", atn_products."lat", atn_products."lng", atn_products."thumbnail", atn_products."available", atn_products."createdAt" FROM atn_products WHERE atn_products."UID" = $1 AND atn_products."available" = $2 AND atn_products."isDelete" = $3`, productUID, isAvailable, isDelete).Scan(&resultUID, &resultUserUID, &resultPartnerUID, &title, &desc, &stock, &price, &roi, &period, &lat, &lng, &thumbnail, &available, &createdAt)

	utils.HandleError(err, "")

	decodedTitle, errDecodedTitle := url.QueryUnescape(title.String)
	decodedDesc, errDecodedDesc := url.QueryUnescape(desc.String)

	utils.HandleError(errDecodedTitle, "")
	utils.HandleError(errDecodedDesc, "")

	product = models.Product{UID: resultUID.String, PartnerUID: resultPartnerUID.String, Title: decodedTitle, Desc: decodedDesc, Stock: int(stock.Int64), Price: price.Float64, Roi: roi.Float64, Period: int(period.Int64), Lat: lat.Float64, Lng: lng.Float64, Thumbnail: thumbnail.String, Available: available.String, CreatedAt: createdAt.String}

	return product
}
