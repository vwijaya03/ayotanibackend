package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetOneUser used for get one data user
func GetOneUser(db *sql.DB, userUID string, userRole string, isDelete string) models.User {
	var (
		UID, email, fullname, phone, gender, pob, dob, address, city, province, postalCode, nationallity, religion, maritalStatus, role, deviceToken, accessToken, accessTokenExpiry, isVerified sql.NullString
		user                                                                                                                                                                                     models.User
	)

	err := db.QueryRow(`SELECT atn_users."UID", atn_users.email, atn_users.fullname, atn_users.phone, atn_users.gender, atn_users.pob, atn_users.dob, atn_users.address, atn_users.city, atn_users.province, atn_users."postalCode", atn_users.nationallity, atn_users.religion, atn_users."maritalStatus", atn_users.role, atn_users."deviceToken", atn_users."accessToken", atn_users."accessTokenExpiry", atn_users."isVerified" FROM atn_users WHERE atn_users."UID" = $1 AND atn_users."role" = $2 AND atn_users."isDelete" = $3`, userUID, userRole, isDelete).Scan(&UID, &email, &fullname, &phone, &gender, &pob, &dob, &address, &city, &province, &postalCode, &nationallity, &religion, &maritalStatus, &role, &deviceToken, &accessToken, &accessTokenExpiry, &isVerified)

	utils.HandleError(err, "")

	user = models.User{UID: UID.String, Email: email.String, Fullname: fullname.String, Phone: phone.String, Gender: gender.String, Pob: pob.String, Dob: dob.String, Address: address.String, City: city.String, Province: province.String, PostalCode: postalCode.String, Nationallity: nationallity.String, Religion: religion.String, MaritalStatus: maritalStatus.String, Role: role.String, DeviceToken: deviceToken.String, AccessToken: accessToken.String, AccessTokenExpiry: accessTokenExpiry.String, IsVerified: isVerified.String}

	return user
}

// GetOneUserAuthentication used for get one data user password, salt, and etc
func GetOneUserAuthentication(db *sql.DB, userUID string, userRole string, isDelete string) models.User {
	var (
		salt     sql.NullString
		password []byte
		user     models.User
	)

	err := db.QueryRow(`SELECT atn_users."password", atn_users.salt FROM atn_users WHERE atn_users."UID" = $1 AND atn_users."role" = $2 AND atn_users."isDelete" = $3`, userUID, userRole, isDelete).Scan(&password, &salt)

	utils.HandleError(err, "")

	user = models.User{Password: password, Salt: salt.String}

	return user
}

// GetExistEmailUser used for get data exclude current user by email
func GetExistEmailUser(db *sql.DB, userUID string, userEmail string) models.User {
	var (
		email sql.NullString
		user  models.User
	)

	err := db.QueryRow(`SELECT atn_users.email FROM atn_users WHERE atn_users."UID" != $1 AND atn_users."email" = $2`, userUID, userEmail).Scan(&email)

	utils.HandleError(err, "")

	user = models.User{Email: email.String}

	return user
}

// GetExistPhoneUser used for get data exclude current user by phone
func GetExistPhoneUser(db *sql.DB, userUID string, userPhone string) models.User {
	var (
		phone sql.NullString
		user  models.User
	)

	err := db.QueryRow(`SELECT atn_users.phone FROM atn_users WHERE atn_users."UID" != $1 AND atn_users."phone" = $2`, userUID, userPhone).Scan(&phone)

	utils.HandleError(err, "")

	user = models.User{Phone: phone.String}

	return user
}

// GetTotalDataUsers used for get total data user
func GetTotalDataUsers(db *sql.DB, role string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	if role != "" {
		sWhere = append(sWhere, models.QueryBuilder{Key: `"atn_users"."role"`, Value: role})
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_users"."UID") as totalData
	FROM "atn_users"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredUsers used for get total data filtered user
func GetTotalDataFilteredUsers(db *sql.DB, search string, role string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_users"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
		{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
		{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
	}

	if role != "" {
		sWhere = append(sWhere, models.QueryBuilder{Key: `"atn_users"."role"`, Value: role})
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_users"."UID") as totalData
	FROM "atn_users"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetUsers used for show data users
func GetUsers(db *sql.DB, search string, role string, orderBy string, limit int, offset int, isDelete string) []models.User {
	var (
		UID, email, fullname, phone, gender, pob, dob, address, city, province, postalCode sql.NullString
		nationallity, religion, maritalStatus, resultRole, isVerified                      sql.NullString
		dataUsers                                                                          []models.User
		resultUsers                                                                        *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_users"."fullname" ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if role != "" {
		sWhere = append(sWhere, models.QueryBuilder{Key: `"atn_users"."role"`, Value: role})
	}

	if search != "" {
		sWhere = append(sWhere, models.QueryBuilder{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + search + `%`})
		sWhere = append(sWhere, models.QueryBuilder{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`})
		sWhere = append(sWhere, models.QueryBuilder{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`})
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_users."UID", atn_users.email, atn_users.fullname, atn_users.phone, atn_users.gender, atn_users.pob, atn_users.dob, atn_users.address, atn_users.city, atn_users.province, atn_users."postalCode", atn_users.nationallity, atn_users.religion, atn_users."maritalStatus", atn_users.role, atn_users."isVerified"
	FROM atn_users
	WHERE %s`, queryBuilder)

	users, errGetUsers := db.Query(fullQuery, values...)
	utils.HandleError(errGetUsers, "")
	resultUsers = users

	for resultUsers.Next() {
		resultUsers.Scan(&UID, &email, &fullname, &phone, &gender, &pob, &dob, &address, &city, &province, &postalCode, &nationallity, &religion, &maritalStatus, &resultRole, &isVerified)

		dataUsers = append(dataUsers, models.User{UID: UID.String, Email: email.String, Fullname: fullname.String, Phone: phone.String, Gender: gender.String, Pob: pob.String, Dob: dob.String, Address: address.String, City: city.String, Province: province.String, PostalCode: postalCode.String, Nationallity: nationallity.String, Religion: religion.String, MaritalStatus: maritalStatus.String, Role: resultRole.String, IsVerified: isVerified.String})
	}
	resultUsers.Close()

	return dataUsers
}
