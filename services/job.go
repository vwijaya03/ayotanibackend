package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataJob used for get total data job
func GetTotalDataJob(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT(atn_jobs."UID") as totalData FROM atn_jobs WHERE atn_jobs."isDelete" = $1`, isDelete).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredJob used for get total data job
func GetTotalDataFilteredJob(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT(atn_jobs."UID") as totalData FROM atn_jobs WHERE atn_jobs."name" like '%' || $1 || '%' AND atn_jobs."isDelete" = $2`, search, isDelete).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetJobs used for get all data job
func GetJobs(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.Job {
	var (
		UID, name  sql.NullString
		dataJobs   []models.Job
		resultJobs *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_jobs."isDelete"`, Value: isDelete},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY atn_jobs.name ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `atn_jobs."isDelete"`, Value: isDelete},
			{Key: `atn_jobs."name":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_jobs."UID", atn_jobs.name
	FROM atn_jobs
	WHERE %s`, queryBuilder)

	jobs, errGetJobs := db.Query(fullQuery, values...)
	utils.HandleError(errGetJobs, "")
	resultJobs = jobs

	for resultJobs.Next() {
		resultJobs.Scan(&UID, &name)

		decodedName, errDecodedName := url.QueryUnescape(name.String)

		utils.HandleError(errDecodedName, "")

		dataJobs = append(dataJobs, models.Job{UID: UID.String, Name: decodedName})
	}
	resultJobs.Close()

	return dataJobs
}

// GetOneJob used for get one data job
func GetOneJob(db *sql.DB, jobUID string, isDelete string) models.Job {
	var (
		UID, name sql.NullString
		job       models.Job
	)

	err := db.QueryRow(`SELECT atn_jobs."UID", atn_jobs.name FROM atn_jobs WHERE atn_jobs."UID" = $1 AND atn_jobs."isDelete" = $2`, jobUID, isDelete).Scan(&UID, &name)

	utils.HandleError(err, "")

	job = models.Job{UID: UID.String, Name: name.String}

	return job
}
