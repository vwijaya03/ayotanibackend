package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"net/url"
)

// GetOneUserHeir used for get one data heir from user
func GetOneUserHeir(db *sql.DB, userUID string, isDelete string) models.UserHeir {
	var (
		UID, fullname, phone, address sql.NullString
		userHeir                      models.UserHeir
	)

	err := db.QueryRow(`SELECT "atn_userHeirs"."UID", "atn_userHeirs"."fullname", "atn_userHeirs"."phone", "atn_userHeirs"."address" FROM "atn_userHeirs" WHERE "atn_userHeirs"."userUID" = $1 AND "atn_userHeirs"."isDelete" = $2`, userUID, isDelete).Scan(&UID, &fullname, &phone, &address)

	utils.HandleError(err, "")

	decodedFullname, errDecodedFullname := url.QueryUnescape(fullname.String)
	decodedAddress, errDecodedAddress := url.QueryUnescape(address.String)

	utils.HandleError(errDecodedFullname, "")
	utils.HandleError(errDecodedAddress, "")

	userHeir = models.UserHeir{UID: UID.String, Fullname: decodedFullname, Phone: phone.String, Address: decodedAddress}

	return userHeir
}
