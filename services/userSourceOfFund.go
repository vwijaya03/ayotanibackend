package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
)

// GetOneUserSourceOfFund used for get one data source of fund from user
func GetOneUserSourceOfFund(db *sql.DB, userUID string, isDelete string) models.UserSourceOfFunds {
	var (
		UID, jobUID, sourceOfFundUID sql.NullString
		userSourceOfFund             models.UserSourceOfFunds
	)

	err := db.QueryRow(`SELECT "atn_userSourceOfFunds"."UID", "atn_userSourceOfFunds"."jobUID", "atn_userSourceOfFunds"."sourceOfFundUID" FROM "atn_userSourceOfFunds" WHERE "atn_userSourceOfFunds"."userUID" = $1 AND "atn_userSourceOfFunds"."isDelete" = $2`, userUID, isDelete).Scan(&UID, &jobUID, &sourceOfFundUID)

	utils.HandleError(err, "")

	userSourceOfFund = models.UserSourceOfFunds{UID: UID.String, JobUID: jobUID.String, SourceOfFundUID: sourceOfFundUID.String}

	return userSourceOfFund
}
