package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
)

// GetOneUserBankAccountInformation used for get one data bank account information from user
func GetOneUserBankAccountInformation(db *sql.DB, userUID string, isDelete string) models.UserBankAccountInformation {
	var (
		UID, bankUID, branch, accountNumber, accountName sql.NullString
		userBankAccountInformation                       models.UserBankAccountInformation
	)

	err := db.QueryRow(`SELECT "atn_userBankAccountInformations"."UID", "atn_userBankAccountInformations"."bankUID", "atn_userBankAccountInformations"."branch", "atn_userBankAccountInformations"."accountNumber", "atn_userBankAccountInformations"."accountName" FROM "atn_userBankAccountInformations" WHERE "atn_userBankAccountInformations"."userUID" = $1 AND "atn_userBankAccountInformations"."isDelete" = $2`, userUID, isDelete).Scan(&UID, &bankUID, &branch, &accountNumber, &accountName)

	utils.HandleError(err, "")

	userBankAccountInformation = models.UserBankAccountInformation{UID: UID.String, BankUID: bankUID.String, Branch: branch.String, AccountNumber: accountNumber.String, AccountName: accountName.String}

	return userBankAccountInformation
}
