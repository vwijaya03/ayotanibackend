package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataRequestVerificationAccount used for get total data request verification account
func GetTotalDataRequestVerificationAccount(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_requestVerificationAccount"."isDelete"`, Value: `0`},
		{Key: `"atn_userDocumentInformations"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_requestVerificationAccount"."UID") as totalData
	FROM "atn_requestVerificationAccount"
	INNER JOIN atn_users ON atn_users."UID" = "atn_requestVerificationAccount"."userUID"
	INNER JOIN "atn_userDocumentInformations" ON "atn_userDocumentInformations"."userUID" = "atn_requestVerificationAccount"."userUID"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredRequestVerificationAccount used for get total data filtered request verification account
func GetTotalDataFilteredRequestVerificationAccount(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_requestVerificationAccount"."isDelete"`, Value: `0`},
		{Key: `"atn_userDocumentInformations"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
		{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
		{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
		{Key: `"atn_userDocumentInformations"."ktp":multipleLike`, Value: `%` + search + `%`},
		{Key: `"atn_userDocumentInformations"."npwp":multipleLike`, Value: `%` + search + `%`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_requestVerificationAccount"."UID") as totalData
	FROM "atn_requestVerificationAccount"
	INNER JOIN atn_users ON atn_users."UID" = "atn_requestVerificationAccount"."userUID"
	INNER JOIN "atn_userDocumentInformations" ON "atn_userDocumentInformations"."userUID" = "atn_requestVerificationAccount"."userUID"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetRequestVerificationAccountForAdmin used for get data investor that requested to verified their account
func GetRequestVerificationAccountForAdmin(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.RequestVerificationAccountWithUserData {
	var (
		UID, userUID, fullname, email, phone, ktp, npwp, ktpImg, npwpImg, status, createdAt sql.NullString
		dataRequestVerificationAccount                                                      []models.RequestVerificationAccountWithUserData
		resultRequestVerificationAccount                                                    *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_requestVerificationAccount"."isDelete"`, Value: `0`},
		{Key: `"atn_userDocumentInformations"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_requestVerificationAccount"."createdAt" ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `"atn_requestVerificationAccount"."isDelete"`, Value: `0`},
			{Key: `"atn_users"."isDelete"`, Value: `0`},
			{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
			{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
			{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
			{Key: `"atn_userDocumentInformations"."ktp":multipleLike`, Value: `%` + search + `%`},
			{Key: `"atn_userDocumentInformations"."npwp":multipleLike`, Value: `%` + search + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		"atn_requestVerificationAccount"."UID", "atn_requestVerificationAccount"."userUID", atn_users."fullname", atn_users."email", atn_users."phone", "atn_userDocumentInformations"."ktp", "atn_userDocumentInformations"."npwp", "atn_userDocumentInformations"."ktpImg", "atn_userDocumentInformations"."npwpImg", "atn_requestVerificationAccount"."status", to_char("atn_requestVerificationAccount"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM "atn_requestVerificationAccount"
	INNER JOIN atn_users ON atn_users."UID" = "atn_requestVerificationAccount"."userUID"
	INNER JOIN "atn_userDocumentInformations" ON "atn_userDocumentInformations"."userUID" = "atn_requestVerificationAccount"."userUID"
	WHERE %s`, queryBuilder)

	requestVerificationAccounts, errRequestVerificationAccounts := db.Query(fullQuery, values...)
	utils.HandleError(errRequestVerificationAccounts, "")
	resultRequestVerificationAccount = requestVerificationAccounts

	for resultRequestVerificationAccount.Next() {
		resultRequestVerificationAccount.Scan(&UID, &userUID, &fullname, &email, &phone, &ktp, &npwp, &ktpImg, &npwpImg, &status, &createdAt)

		dataRequestVerificationAccount = append(dataRequestVerificationAccount, models.RequestVerificationAccountWithUserData{UID: UID.String, UserUID: userUID.String, Fullname: fullname.String, Email: email.String, Phone: phone.String, Ktp: ktp.String, Npwp: npwp.String, KtpImg: ktpImg.String, NpwpImg: npwpImg.String, Status: status.String, CreatedAt: createdAt.String})
	}
	resultRequestVerificationAccount.Close()

	return dataRequestVerificationAccount
}

// GetRequestVerificationAccount used for get data investor that requested to verified their account
func GetRequestVerificationAccount(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.RequestVerificationAccountWithUserData {
	var (
		UID, userUID, fullname, email, phone, createdAt sql.NullString
		dataRequestVerificationAccount                  []models.RequestVerificationAccountWithUserData
		resultRequestVerificationAccount                *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_requestVerificationAccount"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_requestVerificationAccount".title ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `"atn_requestVerificationAccount"."isDelete"`, Value: `0`},
			{Key: `"atn_requestVerificationAccount"."userUID":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		"atn_requestVerificationAccount"."UID", "atn_requestVerificationAccount"."userUID", atn_users."fullname", atn_users."email", atn_users."phone", to_char("atn_requestVerificationAccount"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM "atn_requestVerificationAccount"
	INNER JOIN atn_users ON atn_users."UID" = "atn_requestVerificationAccount"."userUID"
	WHERE %s`, queryBuilder)

	requestVerificationAccounts, errRequestVerificationAccounts := db.Query(fullQuery, values...)
	utils.HandleError(errRequestVerificationAccounts, "")
	resultRequestVerificationAccount = requestVerificationAccounts

	for resultRequestVerificationAccount.Next() {
		resultRequestVerificationAccount.Scan(&UID, &userUID, &fullname, &email, &phone, &createdAt)

		decodedFullname, errDecodedFullname := url.QueryUnescape(fullname.String)

		utils.HandleError(errDecodedFullname, "")

		dataRequestVerificationAccount = append(dataRequestVerificationAccount, models.RequestVerificationAccountWithUserData{UID: UID.String, UserUID: userUID.String, Fullname: decodedFullname, Email: email.String, Phone: phone.String, CreatedAt: createdAt.String})
	}
	resultRequestVerificationAccount.Close()

	return dataRequestVerificationAccount
}

// GetOneRequestVerificationAccount used for get one user data request verification account
func GetOneRequestVerificationAccount(db *sql.DB, UID string, isDelete string) models.RequestVerificationAccount {
	var (
		resultUID, resultUserUID   sql.NullString
		requestVerificationAccount models.RequestVerificationAccount
	)

	err := db.QueryRow(`SELECT "atn_requestVerificationAccount"."UID", "atn_requestVerificationAccount"."userUID" FROM "atn_requestVerificationAccount" WHERE "atn_requestVerificationAccount"."UID" = $1 AND "atn_requestVerificationAccount"."isDelete" = $2`, UID, isDelete).Scan(&resultUID, &resultUserUID)

	utils.HandleError(err, "")

	requestVerificationAccount = models.RequestVerificationAccount{UID: resultUID.String, UserUID: resultUserUID.String}

	return requestVerificationAccount
}

// GetOneUserRequestVerificationAccount used for get one user data request verification account
func GetOneUserRequestVerificationAccount(db *sql.DB, userUID string, isDelete string) models.RequestVerificationAccount {
	var (
		UID, resultUserUID         sql.NullString
		requestVerificationAccount models.RequestVerificationAccount
	)

	err := db.QueryRow(`SELECT "atn_requestVerificationAccount"."UID", "atn_requestVerificationAccount"."userUID" FROM "atn_requestVerificationAccount" WHERE "atn_requestVerificationAccount"."userUID" = $1 AND "atn_requestVerificationAccount"."isDelete" = $2`, userUID, isDelete).Scan(&UID, &resultUserUID)

	utils.HandleError(err, "")

	requestVerificationAccount = models.RequestVerificationAccount{UID: UID.String, UserUID: resultUserUID.String}

	return requestVerificationAccount
}
