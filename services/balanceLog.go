package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"time"
)

// GetBalanceLogs used for show balance from investor
func GetBalanceLogs(db *sql.DB, userUID string, orderBy string, limit int, offset int, isDelete string) []models.BalanceLog {
	var (
		totalAmount                                   sql.NullFloat64
		resultUID, description, resultType, createdAt sql.NullString
		dataBalanceLogs                               []models.BalanceLog
		resultBalanceLogs                             *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_balanceLogs"."isDelete"`, Value: `0`},
	}
	sOrderBy := []models.QueryBuilder{
		{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, `"atn_balanceLogs"."createdAt" DESC`), Value: ``},
	}
	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if len(userUID) > 9 {
		sWhere = []models.QueryBuilder{
			{Key: `"atn_balanceLogs"."isDelete"`, Value: `0`},
			{Key: `"atn_balanceLogs"."toUserUID"`, Value: userUID},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		"atn_balanceLogs"."UID", "atn_balanceLogs"."totalAmount", "atn_balanceLogs"."desc", "atn_balanceLogs"."type", to_char("atn_balanceLogs"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM "atn_balanceLogs" 
	WHERE %s`, queryBuilder)

	balanceLogs, errGetBalanceLogs := db.Query(fullQuery, values...)
	utils.HandleError(errGetBalanceLogs, "")
	resultBalanceLogs = balanceLogs

	for resultBalanceLogs.Next() {
		resultBalanceLogs.Scan(&resultUID, &totalAmount, &description, &resultType, &createdAt)

		dataBalanceLogs = append(dataBalanceLogs, models.BalanceLog{
			UID:         resultUID.String,
			TotalAmount: totalAmount.Float64,
			Description: description.String,
			Type:        resultType.String,
			CreatedAt:   createdAt.String,
		})
	}
	resultBalanceLogs.Close()

	return dataBalanceLogs
}

// InsertBalanceLogs used for insert balance log to investor transaction history
func InsertBalanceLogs(transaction *sql.Tx, balanceLog models.BalanceLog) {
	uidBalanceLog := utils.GenerateUID()
	currentDatetime := time.Now().Format("2006-01-02 15:04:05")

	balanceLog.UID = uidBalanceLog
	balanceLog.CreatedAt = currentDatetime
	balanceLog.UpdatedAt = currentDatetime

	_, errInsertBalanceLog := transaction.Exec(`insert into "atn_balanceLogs" ("UID", "fromUserUID", "toUserUID", "totalAmount", "desc", "descSystem", "type", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`, balanceLog.UID, balanceLog.FromUserUID, balanceLog.ToUserUID, balanceLog.TotalAmount, balanceLog.Description, balanceLog.DescriptionSystem, balanceLog.Type, balanceLog.IsDelete, balanceLog.CreatedAt, balanceLog.UpdatedAt)

	utils.HandleError(errInsertBalanceLog, "")
}
