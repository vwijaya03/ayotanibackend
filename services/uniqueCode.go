package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataUniqueCode used for get total data unique code
func GetTotalDataUniqueCode(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_uniqueCodes"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_uniqueCodes"."UID") as totalData
	FROM "atn_uniqueCodes"
	INNER JOIN atn_users ON atn_users."UID" = "atn_uniqueCodes"."userUID"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredUniqueCode used for get total data filtered unique code
func GetTotalDataFilteredUniqueCode(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_uniqueCodes"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
		{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
		{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
		{Key: `"atn_uniqueCodes"."code":multipleLike`, Value: `%` + search + `%`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_uniqueCodes"."UID") as totalData
	FROM "atn_uniqueCodes"
	INNER JOIN atn_users ON atn_users."UID" = "atn_uniqueCodes"."userUID"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetUniqueCodes used for show data unique codes
func GetUniqueCodes(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.UniqueCode {
	var (
		UID, userUID, code, fullname, email, phone, createdAt sql.NullString
		dataUniqueCodes                                       []models.UniqueCode
		resultUniqueCodes                                     *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_uniqueCodes"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_uniqueCodes"."createdAt" ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `"atn_uniqueCodes"."isDelete"`, Value: `0`},
			{Key: `"atn_users"."isDelete"`, Value: `0`},
			{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
			{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
			{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
			{Key: `"atn_uniqueCodes"."code":multipleLike`, Value: `%` + search + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		"atn_uniqueCodes"."UID", "atn_uniqueCodes"."userUID", atn_users."fullname", atn_users."email", atn_users."phone", "atn_uniqueCodes"."code", to_char("atn_uniqueCodes"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM "atn_uniqueCodes"
	INNER JOIN atn_users ON atn_users."UID" = "atn_uniqueCodes"."userUID"
	WHERE %s`, queryBuilder)

	uniqueCodes, errGetUniqueCodes := db.Query(fullQuery, values...)
	utils.HandleError(errGetUniqueCodes, "")
	resultUniqueCodes = uniqueCodes

	for resultUniqueCodes.Next() {
		resultUniqueCodes.Scan(&UID, &userUID, &fullname, &email, &phone, &code, &createdAt)

		dataUniqueCodes = append(dataUniqueCodes, models.UniqueCode{UID: UID.String, UserUID: userUID.String, Fullname: fullname.String, Email: email.String, Phone: phone.String, Code: code.String, CreatedAt: createdAt.String})
	}
	resultUniqueCodes.Close()

	return dataUniqueCodes
}

// GetOneUniqueCodeByTheCode used for get one unique code data
func GetOneUniqueCodeByTheCode(db *sql.DB, code string, isDelete string) models.UniqueCode {
	var (
		UID, userUID, resultCode sql.NullString
		uniqueCode               models.UniqueCode
	)

	err := db.QueryRow(`SELECT "atn_uniqueCodes"."UID", "atn_uniqueCodes"."userUID", "atn_uniqueCodes"."code" FROM "atn_uniqueCodes" WHERE "atn_uniqueCodes"."code" = $1 AND "atn_uniqueCodes"."isDelete" = $2`, code, isDelete).Scan(&UID, &userUID, &resultCode)

	utils.HandleError(err, "")

	uniqueCode = models.UniqueCode{UID: UID.String, UserUID: userUID.String, Code: userUID.String}

	return uniqueCode
}
