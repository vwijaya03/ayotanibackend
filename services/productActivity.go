package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataProductActivity used for get total data product activity
func GetTotalDataProductActivity(db *sql.DB, productUID string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_productActivities"."productUID"`, Value: productUID},
		{Key: `"atn_productActivities"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_productActivities"."UID") as totalData
	FROM "atn_productActivities"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredProductActivity used for get total data filtered product activity
func GetTotalDataFilteredProductActivity(db *sql.DB, search string, productUID string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_productActivities"."productUID"`, Value: productUID},
		{Key: `"atn_productActivities"."isDelete"`, Value: `0`},
		{Key: `"atn_productActivities"."desc":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_productActivities"."UID") as totalData
	FROM "atn_productActivities"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetProductActivityForAdmin used for show data product activity
func GetProductActivityForAdmin(db *sql.DB, search string, productUID string, orderBy string, limit int, offset int, isDelete string) []models.ProductActivity {
	var (
		UID, resultProductUID, description, thumbnail, createdAt sql.NullString
		dataProductActivities                                    []models.ProductActivity
		resultProductActivities                                  *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_productActivities"."productUID"`, Value: productUID},
		{Key: `"atn_productActivities"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_productActivities"."createdAt" DESC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = append(sWhere, models.QueryBuilder{Key: `"atn_productActivities"."desc":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`})
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		"atn_productActivities"."UID", "atn_productActivities"."productUID", "atn_productActivities"."desc", "atn_productActivities"."thumbnail", to_char("atn_productActivities"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM "atn_productActivities"
	WHERE %s`, queryBuilder)

	productActivities, errGetProductActivities := db.Query(fullQuery, values...)
	utils.HandleError(errGetProductActivities, "")
	resultProductActivities = productActivities

	for resultProductActivities.Next() {
		resultProductActivities.Scan(&UID, &resultProductUID, &description, &thumbnail, &createdAt)

		dataProductActivities = append(dataProductActivities, models.ProductActivity{UID: UID.String, ProductUID: resultProductUID.String, Description: description.String, Thumbnail: thumbnail.String, CreatedAt: createdAt.String})
	}
	resultProductActivities.Close()

	return dataProductActivities
}

// GetProductActivity used for show data product activity
func GetProductActivity(db *sql.DB, search string, productUID string, orderBy string, limit int, offset int, isDelete string) []models.ProductActivity {
	var (
		UID, resultProductUID, description, thumbnail, createdAt sql.NullString
		dataProductActivities                                    []models.ProductActivity
		resultProductActivities                                  *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_productActivities"."productUID"`, Value: productUID},
		{Key: `"atn_productActivities"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_productActivities"."createdAt" DESC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = append(sWhere, models.QueryBuilder{Key: `"atn_productActivities"."desc":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`})
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		"atn_productActivities"."UID", "atn_productActivities"."productUID", "atn_productActivities"."desc", "atn_productActivities"."thumbnail", to_char("atn_productActivities"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM "atn_productActivities"
	WHERE %s`, queryBuilder)

	productActivities, errGetProductActivities := db.Query(fullQuery, values...)
	utils.HandleError(errGetProductActivities, "")
	resultProductActivities = productActivities

	for resultProductActivities.Next() {
		resultProductActivities.Scan(&UID, &resultProductUID, &description, &thumbnail, &createdAt)

		decodedDescription, errDecodedDescription := url.QueryUnescape(description.String)
		utils.HandleError(errDecodedDescription, "")

		dataProductActivities = append(dataProductActivities, models.ProductActivity{UID: UID.String, ProductUID: resultProductUID.String, Description: decodedDescription, Thumbnail: thumbnail.String, CreatedAt: createdAt.String})
	}
	resultProductActivities.Close()

	return dataProductActivities
}
