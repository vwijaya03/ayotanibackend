package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataUploadTopupTransfer used for get total data upload topup transfer
func GetTotalDataUploadTopupTransfer(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_uploadTopupTransfers"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_uploadTopupTransfers"."UID") as totalData
	FROM "atn_uploadTopupTransfers"
	INNER JOIN atn_users ON atn_users."UID" = "atn_uploadTopupTransfers"."userUID"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredUploadTopupTransfer used for get total data filtered upload topup transfer
func GetTotalDataFilteredUploadTopupTransfer(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_uploadTopupTransfers"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
		{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
		{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_uploadTopupTransfers"."UID") as totalData
	FROM "atn_uploadTopupTransfers"
	INNER JOIN atn_users ON atn_users."UID" = "atn_uploadTopupTransfers"."userUID"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetUploadTopupTransfers used for show data from investor that submitted topup transfer
func GetUploadTopupTransfers(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.UploadTopupTransfer {
	var (
		UID, userUID, fullname, email, phone, img, status, createdAt sql.NullString
		amount                                                       sql.NullFloat64
		dataUploadTopupTransfers                                     []models.UploadTopupTransfer
		resultUploadTopupTransfers                                   *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_uploadTopupTransfers"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_uploadTopupTransfers"."createdAt" DESC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
			{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
			{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
			{Key: `"atn_uploadTopupTransfers"."isDelete"`, Value: `0`},
			{Key: `"atn_users"."isDelete"`, Value: `0`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		"atn_uploadTopupTransfers"."UID", "atn_uploadTopupTransfers"."userUID", atn_users."fullname", atn_users."email", atn_users."phone", "atn_uploadTopupTransfers"."amount", "atn_uploadTopupTransfers"."img", "atn_uploadTopupTransfers"."status", to_char("atn_uploadTopupTransfers"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM "atn_uploadTopupTransfers"
	INNER JOIN atn_users ON atn_users."UID" = "atn_uploadTopupTransfers"."userUID"
	WHERE %s`, queryBuilder)

	uploadTopupTransfers, errGetUploadTopupTransfer := db.Query(fullQuery, values...)
	utils.HandleError(errGetUploadTopupTransfer, "")
	resultUploadTopupTransfers = uploadTopupTransfers

	for resultUploadTopupTransfers.Next() {
		resultUploadTopupTransfers.Scan(&UID, &userUID, &fullname, &email, &phone, &amount, &img, &status, &createdAt)

		decodedFullname, errDecodedFullname := url.QueryUnescape(fullname.String)

		utils.HandleError(errDecodedFullname, "")

		dataUploadTopupTransfers = append(dataUploadTopupTransfers, models.UploadTopupTransfer{UID: UID.String, UserUID: userUID.String, Fullname: decodedFullname, Email: email.String, Phone: phone.String, Amount: amount.Float64, Img: img.String, Status: status.String, CreatedAt: createdAt.String})
	}
	resultUploadTopupTransfers.Close()

	return dataUploadTopupTransfers
}

// GetOneUploadTopupTransfers used for show one data from investor that submitted topup transfer
func GetOneUploadTopupTransfers(db *sql.DB, uid string, status string) models.UploadTopupTransfer {
	var (
		resultUID, userUID, img, resultStatus sql.NullString
		amount                                sql.NullFloat64
		uploadTopupTransfer                   models.UploadTopupTransfer
	)

	err := db.QueryRow(`
		SELECT 
			"atn_uploadTopupTransfers"."UID", "atn_uploadTopupTransfers"."userUID", "atn_uploadTopupTransfers"."amount", "atn_uploadTopupTransfers"."img", "atn_uploadTopupTransfers"."status" 
		FROM "atn_uploadTopupTransfers"
		WHERE "atn_uploadTopupTransfers"."UID" = $1 AND "atn_uploadTopupTransfers"."status" = $2`, uid, status).Scan(&resultUID, &userUID, &amount, &img, &resultStatus)

	utils.HandleError(err, "")

	uploadTopupTransfer = models.UploadTopupTransfer{UID: resultUID.String, UserUID: userUID.String, Amount: amount.Float64, Img: img.String, Status: resultStatus.String}

	return uploadTopupTransfer
}
