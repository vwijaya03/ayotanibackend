package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataBank used for get total data bank
func GetTotalDataBank(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT(atn_banks."UID") as totalData FROM atn_banks WHERE atn_banks."isDelete" = $1`, isDelete).Scan(&totalData)

	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredBank used for get total data filtered bank
func GetTotalDataFilteredBank(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT(atn_banks."UID") as totalData FROM atn_banks WHERE atn_banks."name" like '%' || $1 || '%' AND atn_banks."isDelete" = $2`, search, isDelete).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetBanks used for get all data bank
func GetBanks(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.Bank {
	var (
		UID, name   sql.NullString
		dataBanks   []models.Bank
		resultBanks *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_banks."isDelete"`, Value: isDelete},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY atn_banks.name ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `atn_banks."isDelete"`, Value: isDelete},
			{Key: `atn_banks."name":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_banks."UID", atn_banks.name
	FROM atn_banks
	WHERE %s`, queryBuilder)

	banks, errGetBanks := db.Query(fullQuery, values...)
	utils.HandleError(errGetBanks, "")
	resultBanks = banks

	for resultBanks.Next() {
		resultBanks.Scan(&UID, &name)

		decodedName, errDecodedName := url.QueryUnescape(name.String)

		utils.HandleError(errDecodedName, "")

		dataBanks = append(dataBanks, models.Bank{UID: UID.String, Name: decodedName})
	}
	resultBanks.Close()

	return dataBanks
}

// GetOneBank used for get one data bank
func GetOneBank(db *sql.DB, bankUID string, isDelete string) models.Bank {
	var (
		UID, name sql.NullString
		bank      models.Bank
	)

	err := db.QueryRow(`SELECT atn_banks."UID", atn_banks.name FROM atn_banks WHERE atn_banks."UID" = $1 AND atn_banks."isDelete" = $2`, bankUID, isDelete).Scan(&UID, &name)

	utils.HandleError(err, "")

	bank = models.Bank{UID: UID.String, Name: name.String}

	return bank
}
