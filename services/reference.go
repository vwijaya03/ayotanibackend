package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataReference used for get total data reference
func GetTotalDataReference(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT("atn_references"."UID") as totalData FROM "atn_references" WHERE "atn_references"."isDelete" = $1`, isDelete).Scan(&totalData)

	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredNews used for get total data filtered news
func GetTotalDataFilteredNews(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT("atn_references"."UID") as totalData FROM "atn_references" WHERE "atn_references"."title" like '%' || $1 || '%' AND "atn_references"."isDelete" = $2`, search, isDelete).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetOneReference used for get one data reference
func GetOneReference(db *sql.DB, referenceUID string, isDelete string) models.Reference {
	var (
		UID, title, desc, thumbnail sql.NullString
		reference                   models.Reference
	)

	err := db.QueryRow(`SELECT "atn_references"."UID", "atn_references".title, "atn_references".desc, "atn_references".thumbnail FROM "atn_references" WHERE "atn_references"."UID" = $1 AND "atn_references"."isDelete" = $2`, referenceUID, isDelete).Scan(&UID, &title, &desc, &thumbnail)

	utils.HandleError(err, "")

	decodedTitle, errDecodedTitle := url.QueryUnescape(title.String)
	decodedDesc, errDecodedDesc := url.QueryUnescape(desc.String)

	utils.HandleError(errDecodedTitle, "")
	utils.HandleError(errDecodedDesc, "")

	reference = models.Reference{UID: UID.String, Title: decodedTitle, Desc: decodedDesc, Thumbnail: thumbnail.String}

	return reference
}

// GetReferences used for get data reference
func GetReferences(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.Reference {
	var (
		UID, title, desc, thumbnail, createdAt sql.NullString
		dataReferences                         []models.Reference
		resultReferences                       *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_references."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY atn_references.title ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `atn_references."isDelete"`, Value: `0`},
			{Key: `atn_references."title":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_references."UID", atn_references."title", atn_references."desc", atn_references."thumbnail", to_char("atn_references"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM atn_references
	WHERE %s`, queryBuilder)

	references, errGetReference := db.Query(fullQuery, values...)
	utils.HandleError(errGetReference, "")
	resultReferences = references

	for resultReferences.Next() {
		resultReferences.Scan(&UID, &title, &desc, &thumbnail, &createdAt)

		decodedTitle, errDecodedTitle := url.QueryUnescape(title.String)
		decodedDesc, errDecodedDesc := url.QueryUnescape(desc.String)

		utils.HandleError(errDecodedTitle, "")
		utils.HandleError(errDecodedDesc, "")

		dataReferences = append(dataReferences, models.Reference{UID: UID.String, Title: decodedTitle, Desc: decodedDesc, Thumbnail: thumbnail.String, CreatedAt: createdAt.String})
	}
	resultReferences.Close()

	// if search != "" {
	// 	references, errGetReference := db.Query(`SELECT atn_references."UID", atn_references."title", atn_references."desc", atn_references."thumbnail" FROM atn_references
	// 	WHERE
	// 		(title like '%' || $1 || '%') AND
	// 		atn_references."isDelete" = $5
	// 	ORDER BY $2 OFFSET $3 ROWS FETCH FIRST $4 ROW ONLY`, search, orderBy, offset, limit, isDelete)
	// 	utils.HandleError(errGetReference, "")
	// 	resultReferences = references
	// } else {
	// 	references, errGetReference := db.Query(`SELECT atn_references."UID", atn_references."title", atn_references."desc", atn_references."thumbnail" FROM atn_references
	// 	WHERE
	// 		atn_references."isDelete" = $4
	// 	ORDER BY $1 OFFSET $2 ROWS FETCH FIRST $3 ROW ONLY`, orderBy, offset, limit, isDelete)
	// 	utils.HandleError(errGetReference, "")
	// 	resultReferences = references
	// }

	// for resultReferences.Next() {
	// 	resultReferences.Scan(&UID, &title, &desc, &thumbnail)

	// 	decodedTitle, errDecodedTitle := url.QueryUnescape(title.String)
	// 	decodedDesc, errDecodedDesc := url.QueryUnescape(desc.String)

	// 	utils.HandleError(errDecodedTitle, "")
	// 	utils.HandleError(errDecodedDesc, "")

	// 	dataReferences = append(dataReferences, models.Reference{UID: UID.String, Title: decodedTitle, Desc: decodedDesc, Thumbnail: thumbnail.String})
	// }

	return dataReferences
}

// GetReferencesForAdmin used for get data reference for admin
func GetReferencesForAdmin(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.Reference {
	var (
		UID, title, desc, thumbnail, createdAt sql.NullString
		dataReferences                         []models.Reference
		resultReferences                       *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_references."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY atn_references.title ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `atn_references."isDelete"`, Value: `0`},
			{Key: `atn_references."title":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_references."UID", atn_references."title", atn_references."desc", atn_references."thumbnail", to_char("atn_references"."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM atn_references
	WHERE %s`, queryBuilder)

	references, errGetReference := db.Query(fullQuery, values...)
	utils.HandleError(errGetReference, "")
	resultReferences = references

	for resultReferences.Next() {
		resultReferences.Scan(&UID, &title, &desc, &thumbnail, &createdAt)

		dataReferences = append(dataReferences, models.Reference{UID: UID.String, Title: title.String, Desc: desc.String, Thumbnail: thumbnail.String, CreatedAt: createdAt.String})
	}
	resultReferences.Close()

	return dataReferences
}
