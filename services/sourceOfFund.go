package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataSourceOfFund used for get total data source of fund
func GetTotalDataSourceOfFund(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT("atn_sourceOfFunds"."UID") as totalData FROM "atn_sourceOfFunds" WHERE "atn_sourceOfFunds"."isDelete" = $1`, isDelete).Scan(&totalData)

	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredSourceOfFund used for get total data filtered source of fund
func GetTotalDataFilteredSourceOfFund(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT("atn_sourceOfFunds"."UID") as totalData FROM "atn_sourceOfFunds" WHERE "atn_sourceOfFunds"."name" like '%' || $1 || '%' AND "atn_sourceOfFunds"."isDelete" = $2`, search, isDelete).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetSourceOfFunds used for get all data source of funds
func GetSourceOfFunds(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.SourceOfFunds {
	var (
		UID, name           sql.NullString
		dataSourceOfFunds   []models.SourceOfFunds
		resultSourceOfFunds *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_sourceOfFunds"."isDelete"`, Value: isDelete},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_sourceOfFunds".name ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `"atn_sourceOfFunds"."isDelete"`, Value: isDelete},
			{Key: `"atn_sourceOfFunds"."name":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		"atn_sourceOfFunds"."UID", "atn_sourceOfFunds".name
	FROM "atn_sourceOfFunds"
	WHERE %s`, queryBuilder)

	sourceOfFunds, errGetSourceOfFunds := db.Query(fullQuery, values...)
	utils.HandleError(errGetSourceOfFunds, "")
	resultSourceOfFunds = sourceOfFunds

	for resultSourceOfFunds.Next() {
		resultSourceOfFunds.Scan(&UID, &name)

		decodedName, errDecodedName := url.QueryUnescape(name.String)

		utils.HandleError(errDecodedName, "")

		dataSourceOfFunds = append(dataSourceOfFunds, models.SourceOfFunds{UID: UID.String, Name: decodedName})
	}
	resultSourceOfFunds.Close()

	return dataSourceOfFunds
}

// GetOneSourceOfFund used for get one data source of fund
func GetOneSourceOfFund(db *sql.DB, sourceOfFundUID string, isDelete string) models.SourceOfFunds {
	var (
		UID, name    sql.NullString
		sourceOfFund models.SourceOfFunds
	)

	err := db.QueryRow(`SELECT "atn_sourceOfFunds"."UID", "atn_sourceOfFunds".name FROM "atn_sourceOfFunds" WHERE "atn_sourceOfFunds"."UID" = $1 AND "atn_sourceOfFunds"."isDelete" = $2`, sourceOfFundUID, isDelete).Scan(&UID, &name)

	utils.HandleError(err, "")

	sourceOfFund = models.SourceOfFunds{UID: UID.String, Name: name.String}

	return sourceOfFund
}
