package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
)

// GetOneReferenceImg used for get one data reference image
func GetOneReferenceImg(db *sql.DB, UID string, isDelete string) models.ReferenceImage {
	var (
		resultUID, resultReferenceUID, resultImg sql.NullString
		referenceImage                           models.ReferenceImage
	)

	err := db.QueryRow(`SELECT "atn_referenceImages"."UID", "atn_referenceImages"."referenceUID", "atn_referenceImages"."img" FROM "atn_referenceImages" WHERE "atn_referenceImages"."UID" = $1 AND "atn_referenceImages"."isDelete" = $2`, UID, isDelete).Scan(&resultUID, &resultReferenceUID, &resultImg)

	utils.HandleError(err, "")

	referenceImage = models.ReferenceImage{UID: resultUID.String, ReferenceUID: resultReferenceUID.String, Img: resultImg.String}

	return referenceImage
}

// GetReferenceImages used for get data reference images
func GetReferenceImages(db *sql.DB, referenceUID string, isDelete string) []models.ReferenceImage {
	var (
		resultUID, resultReferenceUID, resultImg sql.NullString
		dataReferenceImages                      []models.ReferenceImage
	)

	resultReferenceImages, errGetReference := db.Query(`SELECT "atn_referenceImages"."UID", "atn_referenceImages"."referenceUID", "atn_referenceImages"."img" FROM "atn_referenceImages"
	WHERE
		"atn_referenceImages"."referenceUID" = $1 AND
		"atn_referenceImages"."isDelete" = $2
	`, referenceUID, isDelete)
	utils.HandleError(errGetReference, "")

	for resultReferenceImages.Next() {
		resultReferenceImages.Scan(&resultUID, &resultReferenceUID, &resultImg)

		dataReferenceImages = append(dataReferenceImages, models.ReferenceImage{UID: resultUID.String, ReferenceUID: resultReferenceUID.String, Img: resultImg.String})
	}

	return dataReferenceImages
}
