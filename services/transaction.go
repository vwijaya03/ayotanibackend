package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
)

// GetTransactions used for get data transaction
func GetTransactions(db *sql.DB, userUID string, orderBy string, limit int, offset int, isDelete string) []models.Transaction {
	var (
		amount                                                                                     sql.NullFloat64
		quantity                                                                                   sql.NullInt64
		resultUID, resultUserUID, resultProductUID, productTitle, resultType, createdAt, updatedAt sql.NullString
		dataTransactions                                                                           []models.Transaction
		resultTransactions                                                                         *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_transactions."isDelete"`, Value: `0`},
	}
	sOrderBy := []models.QueryBuilder{
		{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, `atn_transactions."createdAt" DESC`), Value: ``},
	}
	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if len(userUID) > 9 {
		sWhere = []models.QueryBuilder{
			{Key: `atn_transactions."isDelete"`, Value: `0`},
			{Key: `atn_transactions."userUID"`, Value: userUID},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_transactions."UID", atn_transactions."userUID", atn_transactions."productUID", atn_transactions."quantity", atn_transactions."amount", atn_transactions."type", to_char(atn_transactions."createdAt", 'dd-mm-yyyy HH24:MI:SS') as createdAt, to_char(atn_transactions."createdAt", 'dd-mm-yyyy HH24:MI:SS') as updatedAt, atn_products."title" as productTitle
	FROM atn_transactions 
	INNER JOIN atn_products ON atn_products."UID" = atn_transactions."productUID"
	WHERE %s`, queryBuilder)

	transactions, errGetReference := db.Query(fullQuery, values...)
	utils.HandleError(errGetReference, "")
	resultTransactions = transactions

	for resultTransactions.Next() {
		resultTransactions.Scan(&resultUID, &resultUserUID, &resultProductUID, &quantity, &amount, &resultType, &createdAt, &updatedAt, &productTitle)

		// decodedTitle, errDecodedTitle := url.QueryUnescape(title.String)
		// decodedDesc, errDecodedDesc := url.QueryUnescape(desc.String)

		// utils.HandleError(errDecodedTitle, "")
		// utils.HandleError(errDecodedDesc, "")

		dataTransactions = append(dataTransactions, models.Transaction{
			UID:          resultUID.String,
			UserUID:      resultUserUID.String,
			ProductUID:   resultProductUID.String,
			ProductTitle: productTitle.String,
			Quantity:     int(quantity.Int64),
			Amount:       amount.Float64,
			Type:         resultType.String,
			CreatedAt:    createdAt.String,
			UpdatedAt:    updatedAt.String,
		})
	}
	resultTransactions.Close()

	return dataTransactions
}

// GetTransactionsForShareInvestmentProfit used for get data transaction for share investment profit
func GetTransactionsForShareInvestmentProfit(db *sql.DB, productUID string, transactionType string, isDelete string) []models.Transaction {
	var (
		amount                                                sql.NullFloat64
		quantity                                              sql.NullInt64
		UID, userUID, resultProductUID, resultType, createdAt sql.NullString
		dataTransactions                                      []models.Transaction
		resultTransactions                                    *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_transactions."isDelete"`, Value: `0`},
		{Key: `atn_transactions."productUID"`, Value: productUID},
		{Key: `atn_transactions."type"`, Value: transactionType},
	}
	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_transactions."UID", atn_transactions."userUID", atn_transactions."productUID", atn_transactions."quantity", atn_transactions."amount", atn_transactions."type", atn_transactions."createdAt"
	FROM atn_transactions 
	WHERE %s`, queryBuilder)

	transactions, errGetReference := db.Query(fullQuery, values...)
	utils.HandleError(errGetReference, "")
	resultTransactions = transactions

	for resultTransactions.Next() {
		resultTransactions.Scan(&UID, &userUID, &resultProductUID, &quantity, &amount, &resultType, &createdAt)

		dataTransactions = append(dataTransactions, models.Transaction{
			UID:        UID.String,
			UserUID:    userUID.String,
			ProductUID: resultProductUID.String,
			Quantity:   int(quantity.Int64),
			Amount:     amount.Float64,
			Type:       resultType.String,
			CreatedAt:  createdAt.String,
		})
	}
	resultTransactions.Close()

	return dataTransactions
}
