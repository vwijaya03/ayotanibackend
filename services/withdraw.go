package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataWithdraw used for get total data withdraw
func GetTotalDataWithdraw(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_withdrawals"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_withdrawals"."UID") as totalData
	FROM "atn_withdrawals"
	INNER JOIN atn_users ON atn_users."UID" = "atn_withdrawals"."userUID"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredWithdraw used for get total data filtered withdraw
func GetTotalDataFilteredWithdraw(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_withdrawals"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."isDelete"`, Value: `0`},
		{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
		{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
		{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
	}

	sOrderBy := []models.QueryBuilder{}
	sPagination := []models.QueryBuilder{}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		COUNT("atn_withdrawals"."UID") as totalData
	FROM "atn_withdrawals"
	INNER JOIN atn_users ON atn_users."UID" = "atn_withdrawals"."userUID"
	WHERE %s`, queryBuilder)

	err := db.QueryRow(fullQuery, values...).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetWithdraws used for show data from investor that request withdraw balance
func GetWithdraws(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.Withdraw {
	var (
		UID, userUID, fullname, email, phone, status, createdAt sql.NullString
		amount                                                  sql.NullFloat64
		dataWithdraws                                           []models.Withdraw
		resultWithdraws                                         *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `atn_withdrawals."isDelete"`, Value: `0`},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY atn_withdrawals."createdAt" ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `"atn_users"."fullname":multipleLike`, Value: `%` + url.QueryEscape(search) + `%`},
			{Key: `"atn_users"."email":multipleLike`, Value: `%` + search + `%`},
			{Key: `"atn_users"."phone":multipleLike`, Value: `%` + search + `%`},
			{Key: `atn_withdrawals."isDelete"`, Value: `0`},
			{Key: `"atn_users"."isDelete"`, Value: `0`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_withdrawals."UID", atn_withdrawals."userUID", atn_users."fullname", atn_users."email", atn_users."phone", atn_withdrawals."amount", atn_withdrawals."status", to_char(atn_withdrawals."createdAt", 'dd Mon yyyy, HH24:MI:SS') as createdAt
	FROM atn_withdrawals
	INNER JOIN atn_users ON atn_users."UID" = atn_withdrawals."userUID"
	WHERE %s`, queryBuilder)

	withdraws, errGetWithdraws := db.Query(fullQuery, values...)
	utils.HandleError(errGetWithdraws, "")
	resultWithdraws = withdraws

	for resultWithdraws.Next() {
		resultWithdraws.Scan(&UID, &userUID, &fullname, &email, &phone, &amount, &status, &createdAt)

		decodedFullname, errDecodedFullname := url.QueryUnescape(fullname.String)

		utils.HandleError(errDecodedFullname, "")

		dataWithdraws = append(dataWithdraws, models.Withdraw{UID: UID.String, UserUID: userUID.String, Fullname: decodedFullname, Email: email.String, Phone: phone.String, Amount: amount.Float64, Status: status.String, CreatedAt: createdAt.String})
	}
	resultWithdraws.Close()

	return dataWithdraws
}

// GetOneWithdraw used for show one data from investor that requested to withdraw balance
func GetOneWithdraw(db *sql.DB, uid string, status string) models.Withdraw {
	var (
		resultUID, userUID, resultStatus sql.NullString
		amount                           sql.NullFloat64
		withdrawal                       models.Withdraw
	)

	err := db.QueryRow(`
		SELECT 
			atn_withdrawals."UID", atn_withdrawals."userUID", atn_withdrawals."amount", atn_withdrawals."status" 
		FROM atn_withdrawals
		WHERE atn_withdrawals."UID" = $1 AND atn_withdrawals."status" = $2`, uid, status).Scan(&resultUID, &userUID, &amount, &resultStatus)

	utils.HandleError(err, "")

	withdrawal = models.Withdraw{UID: resultUID.String, UserUID: userUID.String, Amount: amount.Float64, Status: resultStatus.String}

	return withdrawal
}
