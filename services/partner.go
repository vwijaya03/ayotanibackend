package services

import (
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"fmt"
	"net/url"
)

// GetTotalDataPartner used for get total data partner
func GetTotalDataPartner(db *sql.DB, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT("atn_partners"."UID") as totalData FROM "atn_partners" WHERE "atn_partners"."isDelete" = $1`, isDelete).Scan(&totalData)

	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetTotalDataFilteredPartner used for get total data filtered partner
func GetTotalDataFilteredPartner(db *sql.DB, search string, isDelete string) int {
	var (
		totalData sql.NullInt64
	)

	err := db.QueryRow(`SELECT COUNT(atn_partners."UID") as totalData FROM atn_partners WHERE atn_partners."fullname" like '%' || $1 || '%' AND atn_partners."isDelete" = $2`, search, isDelete).Scan(&totalData)
	utils.HandleError(err, "")

	return int(totalData.Int64)
}

// GetOnePartner used for get one data partner
func GetOnePartner(db *sql.DB, partnerUID string, isDelete string) models.Partner {
	var (
		UID, fullname sql.NullString
		partner       models.Partner
	)

	err := db.QueryRow(`SELECT atn_partners."UID", atn_partners.fullname FROM atn_partners WHERE atn_partners."UID" = $1 AND atn_partners."isDelete" = $2`, partnerUID, isDelete).Scan(&UID, &fullname)

	utils.HandleError(err, "")

	partner = models.Partner{UID: UID.String, Fullname: fullname.String}

	return partner
}

// GetPartners used for get data partner
func GetPartners(db *sql.DB, search string, orderBy string, limit int, offset int, isDelete string) []models.Partner {
	var (
		UID, fullname  sql.NullString
		dataPartners   []models.Partner
		resultPartners *sql.Rows
	)

	sWhere := []models.QueryBuilder{
		{Key: `"atn_partners"."isDelete"`, Value: isDelete},
	}

	sOrderBy := []models.QueryBuilder{
		{Key: `ORDER BY "atn_partners"."fullname" ASC:keyOnly`, Value: ``},
	}

	sPagination := []models.QueryBuilder{
		{Key: `OFFSET`, Value: offset},
		{Key: `ROWS FETCH FIRST`, Value: limit},
		{Key: `ROW ONLY:keyOnly`, Value: ``},
	}

	if search != "" {
		sWhere = []models.QueryBuilder{
			{Key: `"atn_partners"."isDelete"`, Value: isDelete},
			{Key: `"atn_partners"."fullname":like`, Value: `%` + url.QueryEscape(search) + `%`},
		}
	}

	if len(orderBy) > 4 {
		sOrderBy = []models.QueryBuilder{
			{Key: fmt.Sprintf(`ORDER BY %s :keyOnly`, orderBy), Value: ``},
		}
	}

	queryBuilder, values := utils.SelectQueryBuilder(sWhere, sOrderBy, sPagination)

	fullQuery := fmt.Sprintf(`
	SELECT 
		atn_partners."UID", atn_partners."fullname"
	FROM "atn_partners"
	WHERE %s`, queryBuilder)

	partners, errGetPartners := db.Query(fullQuery, values...)
	utils.HandleError(errGetPartners, "")
	resultPartners = partners

	for resultPartners.Next() {
		resultPartners.Scan(&UID, &fullname)

		// decodedFullname, errDecodedFullname := url.QueryUnescape(fullname.String)
		// utils.HandleError(errDecodedFullname, "")

		dataPartners = append(dataPartners, models.Partner{UID: UID.String, Fullname: fullname.String})
	}

	return dataPartners
}
