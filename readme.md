BalanceLog type 0 = Pembelian unit investasi (-)
BalanceLog type 1 = Topup balance Via Admin (+)
BalanceLog type 2 = Topup balance Via Bank Transfer (+)
BalanceLog type 3 = Withdraw balance (-)
BalanceLog type 4 = Pembagian hasil investasi (+)

RequestVerificationAccount status 0 = Pending
RequestVerificationAccount status 1 = Approved
RequestVerificationAccount status 2 = Rejected

UploadTopupTransfer status 0 = Pending
UploadTopupTransfer status 1 = Approved
UploadTopupTransfer status 2 = Rejected

Withdrawals status 0 = Pending
Withdrawals status 1 = Approved
Withdrawals status 2 = Rejected

Transaction type 0 = Pembelian unit product investasi (AyoTani)

URL Prefix /s/a/.... untuk reverse proxy nginx

<!-- Third Party Packages -->
go get github.com/lib/pq
go get github.com/nfnt/resize
go get golang.org/x/crypto/bcrypt
go get golang.org/x/time/rate
go get -u gopkg.in/gomail.v2
go get firebase.google.com/go