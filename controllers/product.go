package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"net/url"
	"path/filepath"
	"time"

	"golang.org/x/time/rate"
)

// GetProduct show all data product
func GetProduct(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataProducts := 0

		dataProducts := services.GetProductsForAdmin(db, search, completeSortBy, limit, offset, "0")
		totalDataProducts := services.GetTotalDataProduct(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataProducts = services.GetTotalDataFilteredProduct(db, search, utils.DefaultIsDelete)
		}

		if len(dataProducts) < 1 {
			dataProducts = []models.Product{}
		}

		responseGetProducts := struct {
			Code              int              `json:"code"`
			TotalData         int              `json:"totalData"`
			TotalFilteredData int              `json:"totalFilteredData"`
			Products          []models.Product `json:"products"`
		}{200, totalDataProducts, totalFilteredDataProducts, dataProducts}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetProducts)
	}
}

// PostAddProduct used for add new data product
func PostAddProduct(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			targetThumbnailPath, thumbnailFileExtension string = "", ""
		)

		uidProduct := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		defaultIsDelete := "0"
		thumbnailFileSize := 0

		price := utils.ParseFloat64(req.FormValue("price"))
		roi := utils.ParseFloat64(req.FormValue("roi"))
		lat := utils.ParseFloat64(req.FormValue("lat"))
		lng := utils.ParseFloat64(req.FormValue("lng"))
		stock := utils.ParseInt(req.FormValue("stock"))
		period := utils.ParseInt(req.FormValue("period"))
		available := req.FormValue("available")

		thumbnailFile, handlerThumbnailFile, errThumbnailImg := req.FormFile("thumbnailImg")

		if errThumbnailImg != http.ErrMissingFile {
			thumbnailFileExtension = filepath.Ext(handlerThumbnailFile.Filename)
			thumbnailFileSize = int(handlerThumbnailFile.Size)
		}

		request := struct {
			PartnerUID             string  `validate:"required, max:200"`
			Title                  string  `validate:"required, max:200"`
			Description            string  `validate:"required, max:5000"`
			Stock                  int     `validate:"required, lt:2000"`
			TotalStock             int     `validate:"required, lt:2000"`
			Price                  float64 `validate:"required"`
			ROI                    float64 `validate:"lt:30"`
			Period                 int     `validate:"required"`
			Lat                    float64 `validate:"required"`
			Lng                    float64 `validate:"required"`
			Available              string  `validate:"required, max:100"`
			ThumbnailFileSize      int     `validate:"required"`
			ThumbnailFileExtension string  `validate:"required, img"`
		}{req.FormValue("partnerUID"), url.QueryEscape(req.FormValue("title")), url.QueryEscape(req.FormValue("description")), stock, stock, price, roi, period, lat, lng, available, thumbnailFileSize, thumbnailFileExtension}

		utils.Validate(request)

		partner := services.GetOnePartner(db, request.PartnerUID, "0")

		if partner.UID == "" || len(partner.UID) < 1 {
			panic("Data partner tidak ditemukan")
		}

		filenameThumbnail := "product-thumbnail-" + utils.GenerateUID() + thumbnailFileExtension
		targetThumbnailPath = utils.UploadImage(thumbnailFile, handlerThumbnailFile, utils.ProductThumbnailImgPath, filenameThumbnail, utils.ProductThumbnailImgAliasPath)

		_, errInsertProduct := transaction.Exec(`insert into atn_products ("UID", "userUID", "partnerUID", "title", "desc", "stock", "totalStock", "price", "roi", "period", "lat", "lng", "thumbnail", "available", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)`, uidProduct, middleware.AuthUser.UID, request.PartnerUID, request.Title, request.Description, request.Stock, request.TotalStock, request.Price, request.ROI, request.Period, request.Lat, request.Lng, targetThumbnailPath, request.Available, defaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertProduct, "")

		responseAddProduct := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data product berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddProduct)
	}
}

// PostEditProduct used for to change data product
func PostEditProduct(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			targetThumbnailPath, thumbnailFileExtension string = "", ""
		)

		price := utils.ParseFloat64(req.FormValue("price"))
		roi := utils.ParseFloat64(req.FormValue("roi"))
		lat := utils.ParseFloat64(req.FormValue("lat"))
		lng := utils.ParseFloat64(req.FormValue("lng"))
		stock := utils.ParseInt(req.FormValue("stock"))
		period := utils.ParseInt(req.FormValue("period"))
		available := req.FormValue("available")

		defaultIsDelete := "0"
		thumbnailFile, handlerThumbnailFile, errThumbnailImg := req.FormFile("thumbnailImg")

		if errThumbnailImg != http.ErrMissingFile {
			thumbnailFileExtension = filepath.Ext(handlerThumbnailFile.Filename)
		}

		request := struct {
			ProductUID string  `validate:"required, max:200"`
			PartnerUID string  `validate:"required, max:200"`
			Title      string  `validate:"required, max:200"`
			Desc       string  `validate:"required, max:5000"`
			Stock      int     `validate:"lt:2000"`
			Price      float64 `validate:"required"`
			ROI        float64 `validate:"lt:30"`
			Period     int     `validate:"required"`
			Lat        float64 `validate:"required"`
			Lng        float64 `validate:"required"`
			Available  string  `validate:"required, max:100"`
		}{req.FormValue("productUID"), req.FormValue("partnerUID"), url.QueryEscape(req.FormValue("title")), url.QueryEscape(req.FormValue("description")), stock, price, roi, period, lat, lng, available}

		utils.Validate(request)

		product := services.GetOneProduct(db, request.ProductUID, "0")

		if product.UID == "" || len(product.UID) < 1 {
			panic("Data produk tidak ditemukan")
		}

		partner := services.GetOnePartner(db, request.PartnerUID, "0")

		if partner.UID == "" || len(partner.UID) < 1 {
			panic("Data partner tidak ditemukan")
		}

		if thumbnailFile != nil {
			if !(thumbnailFileExtension == utils.ExtJPG || thumbnailFileExtension == utils.ExtJPEG || thumbnailFileExtension == utils.ExtPNG) {
				panic("Format thumbnail salah")
			}

			filenameThumbnail := "product-thumbnail-" + utils.GenerateUID() + thumbnailFileExtension
			targetThumbnailPath = utils.UploadImage(thumbnailFile, handlerThumbnailFile, utils.ProductThumbnailImgPath, filenameThumbnail, utils.ProductThumbnailImgAliasPath)

			_, errUpdateproduct := transaction.Exec(`UPDATE atn_products SET "userUID" = $1, "partnerUID" = $2, "title" = $3, "desc" = $4, "stock" = $5, "price" = $6, "roi" = $7, "period" = $8, "lat" = $9, "lng" = $10, "thumbnail" = $11, "available" = $12 WHERE "UID" = $13 AND "isDelete" = $14`, middleware.AuthUser.UID, request.PartnerUID, request.Title, request.Desc, request.Stock, request.Price, request.ROI, request.Period, request.Lat, request.Lng, targetThumbnailPath, request.Available, request.ProductUID, defaultIsDelete)

			utils.HandleError(errUpdateproduct, "")
			utils.DeleteImage(utils.ProductThumbnailImgPath + product.Thumbnail)
		} else {
			_, errUpdateproduct := transaction.Exec(`UPDATE atn_products SET "userUID" = $1, "partnerUID" = $2, "title" = $3, "desc" = $4, "stock" = $5, "price" = $6, "roi" = $7, "period" = $8, "lat" = $9, "lng" = $10, "available" = $11 WHERE "UID" = $12 AND "isDelete" = $13`, middleware.AuthUser.UID, request.PartnerUID, request.Title, request.Desc, request.Stock, request.Price, request.ROI, request.Period, request.Lat, request.Lng, request.Available, request.ProductUID, defaultIsDelete)

			utils.HandleError(errUpdateproduct, "")
		}

		responseEditProduct := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data product berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditProduct)
	}
}

// PostDeleteProduct used for to delete data product
func PostDeleteProduct(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		defaultIsDelete := "0"
		defaultIsDeleted := "1"

		request := struct {
			ProductUID string `validate:"required,max:200"`
		}{req.FormValue("productUID")}

		utils.Validate(request)

		_, errDeleteProduct := transaction.Exec(`UPDATE atn_products SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, defaultIsDeleted, request.ProductUID, defaultIsDelete)

		utils.HandleError(errDeleteProduct, "")

		responseDeleteProduct := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data product berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteProduct)
	}
}

// GetIvProduct show all data product to public
func GetIvProduct(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `atn_products."createdAt"`
			sortBy = "desc"
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy

		dataProducts := services.GetProducts(db, search, completeSortBy, limit, offset, "0")

		if len(dataProducts) < 1 {
			dataProducts = []models.Product{}
		}

		responseGetProducts := struct {
			Code   int              `json:"code"`
			Result []models.Product `json:"results"`
		}{200, dataProducts}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetProducts)
	}
}

// GetIvProductActivity used for show data product activity to investor
func GetIvProductActivity(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		request := struct {
			ProductUID string `validate:"required, max:200"`
		}{req.FormValue("productUID")}

		utils.Validate(request)

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `"atn_productActivities"."createdAt"`
			sortBy = `desc`
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataProductActivities := 0

		dataProductActivities := services.GetProductActivity(db, search, request.ProductUID, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataProductActivities := services.GetTotalDataProductActivity(db, request.ProductUID, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataProductActivities = services.GetTotalDataFilteredProductActivity(db, search, request.ProductUID, utils.DefaultIsDelete)
		}

		if len(dataProductActivities) < 1 {
			dataProductActivities = []models.ProductActivity{}
		}

		responseGetProductActivities := struct {
			Code              int                      `json:"code"`
			TotalData         int                      `json:"totalData"`
			TotalFilteredData int                      `json:"totalFilteredData"`
			ProductActivities []models.ProductActivity `json:"productActivities"`
		}{200, totalDataProductActivities, totalFilteredDataProductActivities, dataProductActivities}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetProductActivities)
	}
}

// GetProductActivity used for show data product activity
func GetProductActivity(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		request := struct {
			ProductUID string `validate:"required, max:200"`
		}{req.FormValue("productUID")}

		utils.Validate(request)

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `"atn_productActivities"."createdAt"`
			sortBy = `desc`
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataProductActivities := 0

		dataProductActivities := services.GetProductActivityForAdmin(db, search, request.ProductUID, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataProductActivities := services.GetTotalDataProductActivity(db, request.ProductUID, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataProductActivities = services.GetTotalDataFilteredProductActivity(db, search, request.ProductUID, utils.DefaultIsDelete)
		}

		if len(dataProductActivities) < 1 {
			dataProductActivities = []models.ProductActivity{}
		}

		responseGetProductActivities := struct {
			Code              int                      `json:"code"`
			TotalData         int                      `json:"totalData"`
			TotalFilteredData int                      `json:"totalFilteredData"`
			ProductActivities []models.ProductActivity `json:"productActivities"`
		}{200, totalDataProductActivities, totalFilteredDataProductActivities, dataProductActivities}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetProductActivities)
	}
}

// PostAddProductActivity used for add new product activity
func PostAddProductActivity(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			targetThumbnailPath, thumbnailFileExtension string = "", ""
			thumbnailFileSize                           int    = 0
		)

		uidProductActivity := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		thumbnailFile, handlerThumbnailFile, _ := req.FormFile("thumbnailImg")
		thumbnailFileType := utils.GetFileContentTypeViaFormFile(thumbnailFile)

		request := struct {
			ProductUID  string `validate:"required, max:200"`
			Description string `validate:"required, max:5000"`
			Thumbnail   string `validate:"required, imageType"`
		}{req.FormValue("productUID"), url.QueryEscape(req.FormValue("description")), thumbnailFileType}

		utils.Validate(request)

		thumbnailFileExtension = filepath.Ext(handlerThumbnailFile.Filename)
		thumbnailFileSize = int(handlerThumbnailFile.Size)

		if thumbnailFileSize > 2000000 {
			panic("Ukuran gambar terlalu besar")
		}

		filenameThumbnail := "product-activity-thumbnail-" + utils.GenerateUID() + thumbnailFileExtension
		targetThumbnailPath = utils.UploadImage(thumbnailFile, handlerThumbnailFile, utils.ProductActivityThumbnailImgPath, filenameThumbnail, utils.ProductActivityThumbnailImgAliasPath)

		product := services.GetOneProduct(db, request.ProductUID, utils.DefaultIsDelete)

		if product.UID == "" || len(product.UID) < 1 {
			panic("Data produk tidak ditemukan")
		}

		_, errInsertProductActivity := transaction.Exec(`insert into "atn_productActivities" ("UID", "productUID", "desc", "thumbnail", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7)`, uidProductActivity, product.UID, request.Description, targetThumbnailPath, utils.DefaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertProductActivity, "")

		responseAddProductActivity := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Aktifitas produk berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddProductActivity)
	}
}

// PostDeleteProductActivity used for to delete product activity
func PostDeleteProductActivity(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			ProductActivityUID string `validate:"required,max:200"`
		}{req.FormValue("productActivityUID")}

		utils.Validate(request)

		_, errDeleteProductActivity := transaction.Exec(`UPDATE "atn_productActivities" SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, utils.DefaultIsDeleted, request.ProductActivityUID, utils.DefaultIsDelete)

		utils.HandleError(errDeleteProductActivity, "")

		responseDeleteProductActivity := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Aktifitas produk berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteProductActivity)
	}
}
