package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"path/filepath"
	"time"

	"golang.org/x/time/rate"
)

// GetReferenceImage show all data reference image
func GetReferenceImage(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			ReferenceUID string `validate:"required,max:200"`
		}{req.FormValue("referenceUID")}

		utils.Validate(request)

		reference := services.GetOneReference(db, request.ReferenceUID, "0")

		if reference.UID == "" || len(reference.UID) < 10 {
			panic("Data reference tidak ditemukan")
		}

		dataReferenceImages := services.GetReferenceImages(db, request.ReferenceUID, "0")

		if len(dataReferenceImages) < 1 {
			dataReferenceImages = []models.ReferenceImage{}
		}

		responseGetReferenceImages := struct {
			Result []models.ReferenceImage `json:"results"`
		}{dataReferenceImages}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetReferenceImages)
	}
}

// PostAddReferenceImage used for add new data reference image
func PostAddReferenceImage(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			targetReferenceImgPath, referenceImgFileExtension string = "", ""
		)

		uidReferenceImg := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		defaultIsDelete := "0"
		referenceImgFileSize := 0

		referenceImgFile, handlerReferenceImgFile, errReferenceImg := req.FormFile("referenceImg")

		if errReferenceImg != http.ErrMissingFile {
			referenceImgFileExtension = filepath.Ext(handlerReferenceImgFile.Filename)
			referenceImgFileSize = int(handlerReferenceImgFile.Size)
		}

		request := struct {
			ReferenceUID              string `validate:"required, max:200"`
			ReferenceImgFileSize      int    `validate:"required, lt:1000000"`
			ReferenceImgFileExtension string `validate:"required"`
		}{req.FormValue("referenceUID"), referenceImgFileSize, referenceImgFileExtension}

		utils.Validate(request)

		reference := services.GetOneReference(db, request.ReferenceUID, "0")

		if reference.UID == "" || len(reference.UID) < 10 {
			panic("Data reference tidak ditemukan")
		}

		filenameReferenceImg := "ref-image-" + utils.GenerateUID() + referenceImgFileExtension
		targetReferenceImgPath = utils.UploadImage(referenceImgFile, handlerReferenceImgFile, utils.ReferenceImgPath, filenameReferenceImg, utils.ReferenceImgAliasPath)

		_, errInsertReference := transaction.Exec(`insert into "atn_referenceImages" ("UID", "referenceUID", "img", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6)`, uidReferenceImg, request.ReferenceUID, targetReferenceImgPath, defaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertReference, "")

		responseAddReferenceImg := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data reference image berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddReferenceImg)
	}
}

// PostDeleteReferenceImage used for to delete data reference image
func PostDeleteReferenceImage(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		defaultIsDelete := "0"
		defaultIsDeleted := "1"

		request := struct {
			ReferenceImageUID string `validate:"required,max:200"`
		}{req.FormValue("referenceImageUID")}

		utils.Validate(request)

		referenceImage := services.GetOneReferenceImg(db, request.ReferenceImageUID, "0")

		if referenceImage.Img != "" {
			utils.DeleteImage(utils.ReferenceImgPath + referenceImage.Img)
		}

		_, errDeleteReference := transaction.Exec(`UPDATE atn_references SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, defaultIsDeleted, request.ReferenceImageUID, defaultIsDelete)

		utils.HandleError(errDeleteReference, "")

		responseDeleteBank := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data reference berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteBank)
	}
}
