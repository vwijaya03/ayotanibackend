package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"time"

	"golang.org/x/time/rate"
)

// GetRequestVerificationAccount show all data investor that requested to verified their account
func GetRequestVerificationAccount(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `"atn_requestVerificationAccount"."createdAt"`
			sortBy = `desc`
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataRequestVerificationAccounts := 0

		dataRequestVerificationAccounts := services.GetRequestVerificationAccountForAdmin(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataRequestVerificationAccounts := services.GetTotalDataRequestVerificationAccount(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataRequestVerificationAccounts = services.GetTotalDataFilteredRequestVerificationAccount(db, search, utils.DefaultIsDelete)
		}

		if len(dataRequestVerificationAccounts) < 1 {
			dataRequestVerificationAccounts = []models.RequestVerificationAccountWithUserData{}
		}

		responseGetRequestVerificationAccount := struct {
			Code                        int                                             `json:"code"`
			TotalData                   int                                             `json:"totalData"`
			TotalFilteredData           int                                             `json:"totalFilteredData"`
			RequestVerificationAccounts []models.RequestVerificationAccountWithUserData `json:"requestVerificationAccounts"`
		}{200, totalDataRequestVerificationAccounts, totalFilteredDataRequestVerificationAccounts, dataRequestVerificationAccounts}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetRequestVerificationAccount)
	}
}

// PostRequestVerificationAccount untuk ajukan verifikasi akun investor
func PostRequestVerificationAccount(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		UID := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		currentUserUID := middleware.AuthUser.UID

		if middleware.AuthUser.IsVerified == "1" {
			panic("Akun anda sudah diverifikasi")
		}

		requestVerificationAccount := services.GetOneUserRequestVerificationAccount(db, currentUserUID, utils.DefaultIsDelete)

		if requestVerificationAccount.UserUID != "" || len(requestVerificationAccount.UserUID) > 0 {
			panic("Anda telah mengajukan proses verifikasi akun")
		}

		investor := services.GetOneUser(db, currentUserUID, "investor", utils.DefaultIsDelete)
		investorSourceOfFund := services.GetOneUserSourceOfFund(db, currentUserUID, utils.DefaultIsDelete)
		investorBankAccount := services.GetOneUserBankAccountInformation(db, currentUserUID, utils.DefaultIsDelete)
		investorDocument := services.GetOneUserDocument(db, currentUserUID, utils.DefaultIsDelete)
		investorHeir := services.GetOneUserHeir(db, currentUserUID, utils.DefaultIsDelete)

		if investor.Phone == "" || investor.Gender == "" || investor.Pob == "" || investor.Dob == "" || investor.Address == "" || investor.City == "" || investor.Province == "" || investor.PostalCode == "" || investor.Nationallity == "" || investor.Religion == "" || investor.MaritalStatus == "" {
			panic("Silahkan lengkapi data profile terlebih dahulu")
		}

		if investorSourceOfFund.JobUID == "" || investorSourceOfFund.SourceOfFundUID == "" {
			panic("Silahkan lengkapi data sumber dana terlebih dahulu")
		}

		if investorBankAccount.BankUID == "" || investorBankAccount.Branch == "" || investorBankAccount.AccountNumber == "" || investorBankAccount.AccountName == "" {
			panic("Silahkan lengkapi data informasi rekening terlebih dahulu")
		}

		if investorDocument.Ktp == "" || investorDocument.Npwp == "" || investorDocument.KtpImg == "" || investorDocument.NpwpImg == "" {
			panic("Silahkan lengkapi data informasi dokumen terlebih dahulu")
		}

		if investorHeir.Fullname == "" || investorHeir.Phone == "" || investorHeir.Address == "" {
			panic("Silahkan lengkapi data ahli waris terlebih dahulu")
		}

		_, errInsertRequestVerificationAccount := transaction.Exec(`insert into "atn_requestVerificationAccount" ("UID", "userUID", "status", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6)`, UID, currentUserUID, "0", utils.DefaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertRequestVerificationAccount, "")

		responsePostRequestVerificationAccount := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Pengajuan verifikasi akun sedang diproses"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responsePostRequestVerificationAccount)
	}
}

// PostActionRequestVerificationAccount untuk admin melakukan action approve atau reject verifikasi akun investor
func PostActionRequestVerificationAccount(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			UID    string `validate:"required,max:200"`
			Action string `validate:"required,max:10"`
		}{req.FormValue("uid"), req.FormValue("action")}

		utils.Validate(request)

		status := "1"
		message := "Verifikasi akun investor telah diapprove"

		if request.Action == "2" {
			status = "2"
			message = "Verifikasi akun investor telah direject"
		}

		existRequestVerificationAccount := services.GetOneRequestVerificationAccount(db, request.UID, "0")

		if existRequestVerificationAccount.UID == "" || len(existRequestVerificationAccount.UID) < 5 {
			panic("Data request verification account tidak ditemukan")
		}

		if status == "1" {
			_, errUpdatePostActionRequestVerificationAccount := transaction.Exec(`UPDATE atn_users SET "isVerified" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, "1", existRequestVerificationAccount.UserUID, utils.DefaultIsDelete)
			utils.HandleError(errUpdatePostActionRequestVerificationAccount, "")
		} else {
			_, errUpdatePostActionRequestVerificationAccount := transaction.Exec(`UPDATE atn_users SET "isVerified" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, "0", existRequestVerificationAccount.UserUID, utils.DefaultIsDelete)
			utils.HandleError(errUpdatePostActionRequestVerificationAccount, "")
		}

		_, errUpdateWithdraw := transaction.Exec(`UPDATE "atn_requestVerificationAccount" SET "status" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, status, request.UID, utils.DefaultIsDelete)
		utils.HandleError(errUpdateWithdraw, "")

		// Send Notification
		if status == "1" {
			user := services.GetOneUser(db, existRequestVerificationAccount.UserUID, utils.RoleInvestor, utils.DefaultIsDelete)
			title := "Verifikasi Akun AyoTani Berhasil"
			body := "Akun AyoTani anda telah berhasil verifikasi, anda sudah bisa melakukan transaksi pada aplikasi AyoTani"
			fullname := user.Fullname
			from := utils.DefaultFromForEmail
			to := user.Email
			subject := title

			go func() { utils.SendNotification(title, body, "", user.DeviceToken, "") }()
			go func() { utils.SendEmailVerifiedAccount(fullname, from, to, subject) }()
		}

		responsePostRequestVerificationAccount := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, message}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responsePostRequestVerificationAccount)
	}
}
