package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"time"

	"golang.org/x/time/rate"
)

// GetUniqueCode show all data unique code
func GetUniqueCode(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataUniqueCodes := 0

		dataUniqueCodes := services.GetUniqueCodes(db, search, completeSortBy, limit, offset, "0")
		totalDataUniqueCodes := services.GetTotalDataUniqueCode(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataUniqueCodes = services.GetTotalDataFilteredUniqueCode(db, search, utils.DefaultIsDelete)
		}

		if len(dataUniqueCodes) < 1 {
			dataUniqueCodes = []models.UniqueCode{}
		}

		responseGetUniqueCodes := struct {
			Code              int                 `json:"code"`
			TotalData         int                 `json:"totalData"`
			TotalFilteredData int                 `json:"totalFilteredData"`
			UniqueCodes       []models.UniqueCode `json:"uniqueCodes"`
		}{200, totalDataUniqueCodes, totalFilteredDataUniqueCodes, dataUniqueCodes}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetUniqueCodes)
	}
}

// PostAddUniqueCode used for add new unique code
func PostAddUniqueCode(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		uidUniqueCode := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		request := struct {
			UserUID string `validate:"required, max:200"`
			Code    string `validate:"required, max:200"`
		}{req.FormValue("userUID"), req.FormValue("code")}

		utils.Validate(request)

		user := services.GetOneUser(db, request.UserUID, "investor", utils.DefaultIsDelete)
		uniqueCode := services.GetOneUniqueCodeByTheCode(db, request.Code, utils.DefaultIsDelete)

		if user.UID == "" || len(user.UID) < 5 {
			panic("Data investor tidak ditemukan")
		}

		if uniqueCode.UID != "" || len(uniqueCode.UID) > 5 {
			panic("Data kode unik sudah pernah ditambahkan, silahkan masukkan kode unik yang baru")
		}

		_, errInsertUniqueCode := transaction.Exec(`insert into "atn_uniqueCodes" ("UID", "userUID", "code", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6)`, uidUniqueCode, request.UserUID, request.Code, utils.DefaultIsDelete, currentDatetime, currentDatetime)
		utils.HandleError(errInsertUniqueCode, "")

		responseAddUniqueCode := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data kode unik berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddUniqueCode)
	}
}

// PostDeleteUniqueCode used for to delete data unique code
func PostDeleteUniqueCode(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			UID string `validate:"required,max:200"`
		}{req.FormValue("uid")}

		utils.Validate(request)

		_, errDeleteUniqueCode := transaction.Exec(`UPDATE "atn_uniqueCodes" SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, utils.DefaultIsDeleted, request.UID, utils.DefaultIsDelete)

		utils.HandleError(errDeleteUniqueCode, "")

		responseDeleteUniqueCode := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data kode unik berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteUniqueCode)
	}
}
