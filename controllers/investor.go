package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"path/filepath"
	"time"

	"golang.org/x/time/rate"
)

// PostIvLogin untuk login investor
func PostIvLogin(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		var (
			UID, email, fullname, phone, gender, pob, dob, address, city, province, postalCode, nationallity, religion, maritalStatus, role, accessToken, accessTokenExpiry, salt, isVerified sql.NullString

			password []byte
		)

		request := struct {
			Email    string `validate:"email,min:4,max:40"`
			Password string `validate:"min:6,max:100"`
		}{req.FormValue("email"), req.FormValue("password")}

		utils.Validate(request)

		err := db.QueryRow(`SELECT atn_users."UID", atn_users.email, atn_users.password, atn_users.fullname, atn_users.phone, atn_users.gender, atn_users.pob, atn_users.dob, atn_users.address, atn_users.city, atn_users.province, atn_users."postalCode", atn_users.nationallity, atn_users.religion, atn_users."maritalStatus", atn_users.role, atn_users."accessToken", atn_users."accessTokenExpiry", atn_users.salt, atn_users."isVerified" FROM atn_users WHERE atn_users.email = $1 AND atn_users."isDelete" = $2`, request.Email, "0").Scan(&UID, &email, &password, &fullname, &phone, &gender, &pob, &dob, &address, &city, &province, &postalCode, &nationallity, &religion, &maritalStatus, &role, &accessToken, &accessTokenExpiry, &salt, &isVerified)

		if email.String == "" || len(email.String) < 1 {
			panic("Email atau password anda salah")
		}

		actualPassword := utils.Decrypt(password, salt.String)

		if request.Password != string(actualPassword) {
			panic("Email atau password anda salah")
		}

		var (
			newAccessToken, newAccessTokenExpiry string
		)

		isUpdateNewToken := false

		tokenExpiryDate, err := time.Parse("2006-01-02 15:04:05", accessTokenExpiry.String)
		currentDatetime, err := time.Parse("2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05"))

		diff := tokenExpiryDate.Sub(currentDatetime)

		if int(diff.Hours()/24) <= 3 {
			uidStr := utils.GenerateUID()
			currentDatetime := time.Now().Format("2006-01-02 15:04:05")

			newSalt := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime+uidStr), uidStr))
			newAccessTokenExpiry = time.Now().AddDate(0, 1, 0).Format("2006-01-02 15:04:05")
			newAccessToken = fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime+uidStr), newSalt))

			_, errUpdateToken := db.Exec(`UPDATE atn_users SET "accessToken" = $1, "accessTokenExpiry" = $2 WHERE email = $3`, newAccessToken, newAccessTokenExpiry, request.Email)

			if errUpdateToken != nil {
				panic(errUpdateToken.Error())
			}

			isUpdateNewToken = true
		}

		user := models.Investor{
			UID:               UID.String,
			Email:             email.String,
			Fullname:          fullname.String,
			Phone:             phone.String,
			Gender:            gender.String,
			Pob:               pob.String,
			Dob:               dob.String,
			Address:           address.String,
			City:              city.String,
			Province:          province.String,
			PostalCode:        postalCode.String,
			Nationallity:      nationallity.String,
			Religion:          religion.String,
			MaritalStatus:     maritalStatus.String,
			Role:              role.String,
			AccessToken:       accessToken.String,
			AccessTokenExpiry: accessTokenExpiry.String,
			IsVerified:        isVerified.String,
		}

		credential := models.Credential{
			AccessToken:       accessToken.String,
			AccessTokenExpiry: accessTokenExpiry.String,
		}

		if isUpdateNewToken {
			credential.AccessToken = newAccessToken
			credential.AccessTokenExpiry = newAccessTokenExpiry
		}

		//fmt.Println(dt.AddDate(0, 0, 3)) Tambah 3 hari
		//fmt.Println(dt.AddDate(0, 0, -3)) Minus 3 hari

		responseLogin := struct {
			Code       int               `json:"code"`
			Credential models.Credential `json:"credential"`
			User       models.Investor   `json:"user"`
		}{200, credential, user}

		if err != nil {
			panic(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseLogin)
	}
}

// PostIvLogout untuk logout investor
func PostIvLogout(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		var (
			UID sql.NullString
		)

		request := struct {
			Token string `validate:"required,max:200"`
		}{req.Header.Get("token")}

		utils.Validate(request)

		err := db.QueryRow(`SELECT atn_users."UID" FROM atn_users WHERE atn_users."accessToken" = $1 AND atn_users."isDelete" = $2`, request.Token, "0").Scan(&UID)

		if UID.String == "" || len(UID.String) < 1 {
			panic("Request ditolak")
		}

		if err != nil {
			panic(err.Error())
		}

		_, errUpdateToken := db.Exec(`UPDATE atn_users SET "accessTokenExpiry" = $1 WHERE "accessToken" = $2`, "1970-01-01 00:00:00", request.Token)

		if errUpdateToken != nil {
			panic(errUpdateToken.Error())
		}

		responseLogout := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Logout berhasil"}

		if err != nil {
			panic(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseLogout)
	}
}

// PostRegisterInvestor untuk register data investor
func PostRegisterInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		var (
			existEmail string
		)

		userUIDStr := utils.GenerateUID()
		uidSrcOfFund := utils.GenerateUID()
		uidBankAccount := utils.GenerateUID()
		uidDocument := utils.GenerateUID()
		uidHeir := utils.GenerateUID()
		uidBalance := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		salt := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime), userUIDStr))
		accessToken := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime), salt))
		accessTokenExpiry := currentDatetime
		role := "investor"
		isVerified := "0"
		isDelete := "0"
		defaultBalance := 0

		request := struct {
			Email      string `validate:"email,min:4,max:40"`
			Fullname   string `validate:"min:2,max:40"`
			Password   string `validate:"min:6,max:100"`
			UniqueCode string `validate:"max:50"`
		}{req.FormValue("email"), req.FormValue("fullname"), req.FormValue("password"), req.FormValue("uniqueCode")}

		utils.Validate(request)

		err := transaction.QueryRow(`SELECT atn_users.email FROM atn_users WHERE atn_users.email = $1 AND atn_users."isDelete" = $2`, request.Email, "0").Scan(&existEmail)

		utils.HandleError(err, "")

		if existEmail != "" || len(existEmail) > 0 {
			panic("Email telah digunakan")
		}

		if request.UniqueCode != "" || len(request.UniqueCode) > 0 {
			uniqueCode := services.GetOneUniqueCodeByTheCode(db, request.UniqueCode, utils.DefaultIsDelete)

			if uniqueCode.UID == "" || len(uniqueCode.UID) < 5 {
				panic("Data kode unik anda salah")
			}

			userUniqueCodeUID := utils.GenerateUID()

			_, errInsertUserUniqueCode := transaction.Exec(`insert into "atn_usersUniqueCodes" ("UID", "userUID", code, "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6)`, userUniqueCodeUID, userUIDStr, request.UniqueCode, utils.DefaultIsDelete, currentDatetime, currentDatetime)

			utils.HandleError(errInsertUserUniqueCode, "")
		}

		password := utils.Encrypt([]byte(req.FormValue("password")), salt)

		_, errInsertUser := transaction.Exec(`insert into atn_users ("UID", fullname, email, password, role, "accessToken", salt, "accessTokenExpiry","isVerified", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`, userUIDStr, request.Fullname, request.Email, password, role, accessToken, salt, accessTokenExpiry, isVerified, isDelete, currentDatetime, currentDatetime)

		_, errInsertUserSourceOfFunds := transaction.Exec(`insert into "atn_userSourceOfFunds" ("UID", "userUID", "jobUID", "sourceOfFundUID", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7)`, uidSrcOfFund, userUIDStr, "", "", isDelete, currentDatetime, currentDatetime)

		_, errInsertUserBankAccount := transaction.Exec(`insert into "atn_userBankAccountInformations" ("UID", "userUID", "bankUID", branch, "accountNumber", "accountName", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`, uidBankAccount, userUIDStr, "", "", "", "", isDelete, currentDatetime, currentDatetime)

		_, errInsertUserDocument := transaction.Exec(`insert into "atn_userDocumentInformations" ("UID", "userUID", ktp, npwp, "ktpImg", "npwpImg", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`, uidDocument, userUIDStr, "", "", "", "", isDelete, currentDatetime, currentDatetime)

		_, errInsertUserHeir := transaction.Exec(`insert into "atn_userHeirs" ("UID", "userUID", fullname, phone, address, "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`, uidHeir, userUIDStr, "", "", "", isDelete, currentDatetime, currentDatetime)

		_, errInsertUserBalance := transaction.Exec(`insert into "atn_userBalance" ("UID", "userUID", balance, "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6)`, uidBalance, userUIDStr, defaultBalance, isDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertUser, "")
		utils.HandleError(errInsertUserSourceOfFunds, "")
		utils.HandleError(errInsertUserBankAccount, "")
		utils.HandleError(errInsertUserDocument, "")
		utils.HandleError(errInsertUserHeir, "")
		utils.HandleError(errInsertUserBalance, "")

		responseAddInvestor := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Registrasi berhasil, silahkan kembali ke halaman login untuk melanjutkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddInvestor)
	}
}

// PostChangePasswordInvestor untuk ubah password investor
func PostChangePasswordInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			OldPassword string `validate:"required,max:50"`
			NewPassword string `validate:"required,max:50"`
		}{req.FormValue("oldPassword"), req.FormValue("newPassword")}

		currentUserUID := middleware.AuthUser.UID
		dataAuthentication := services.GetOneUserAuthentication(db, currentUserUID, utils.RoleInvestor, utils.DefaultIsDelete)

		actualPassword := utils.Decrypt(dataAuthentication.Password, dataAuthentication.Salt)

		if request.OldPassword != string(actualPassword) {
			panic("Password lama anda salah")
		}

		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		salt := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime), currentUserUID))
		password := utils.Encrypt([]byte(request.NewPassword), salt)

		_, errChangePasswordInvestor := transaction.Exec(`UPDATE "atn_users" SET "password" = $1, "salt" = $2 WHERE "UID" = $3`, password, salt, currentUserUID)

		utils.HandleError(errChangePasswordInvestor, "")

		responseChangePassword := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Password anda berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseChangePassword)
	}
}

// PostResetPassword untuk ubah password investor
func PostResetPassword(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		//defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)

		request := struct {
			Email string `validate:"required,email,max:50"`
		}{req.FormValue("email")}

		newPassword := utils.GenerateRandomNumber(6)
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		salt := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime), request.Email))
		password := utils.Encrypt([]byte(newPassword), salt)
		message := fmt.Sprintf(`Berikut ada password baru anda <b>%s</b>`, newPassword)
		from := utils.DefaultFromForEmail
		to := request.Email
		subject := "Reset Password Ayo Tani"

		go utils.SendEmailResetPassword(transaction, from, to, subject, message, password, salt)

		responseChangePassword := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Password anda berhasil direset"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseChangePassword)
	}
}

// GetInvestor show all data from table investor
func GetInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			UID, email, fullname, phone, gender, pob, dob, address, city, province, postalCode, nationallity, religion, maritalStatus, role, accessToken, accessTokenExpiry, isVerified sql.NullString

			dataUsers []models.Investor
		)

		defaultIsDelete := "0"
		defaultRole := "investor"

		rowsDB, errGetInvestor := db.Query(`SELECT atn_users."UID", atn_users.email, atn_users.fullname, atn_users.phone, atn_users.gender, atn_users.pob, atn_users.dob, atn_users.address, atn_users.city, atn_users.province, atn_users."postalCode", atn_users.nationallity, atn_users.religion, atn_users."maritalStatus", atn_users.role, atn_users."accessToken", atn_users."accessTokenExpiry", atn_users."isVerified" FROM atn_users WHERE atn_users.role = $1 AND atn_users."isDelete" = $2`, defaultRole, defaultIsDelete)
		defer rowsDB.Close()

		utils.HandleError(errGetInvestor, "")

		for rowsDB.Next() {
			rowsDB.Scan(&UID, &email, &fullname, &phone, &gender, &pob, &dob, &address, &city, &province, &postalCode, &nationallity, &religion, &maritalStatus, &role, &accessToken, &accessTokenExpiry, &isVerified)

			dataUsers = append(dataUsers, models.Investor{UID: UID.String, Email: email.String, Fullname: fullname.String, Phone: phone.String, Gender: gender.String, Pob: pob.String, Dob: dob.String, Address: address.String, City: city.String, Province: province.String, PostalCode: postalCode.String, Nationallity: nationallity.String, Religion: religion.String, MaritalStatus: maritalStatus.String, Role: role.String, AccessToken: accessToken.String, AccessTokenExpiry: accessTokenExpiry.String, IsVerified: isVerified.String})
		}

		if len(dataUsers) < 1 {
			dataUsers = []models.Investor{}
		}

		responseGetInvestor := models.ResGetInvestor{
			Result:  dataUsers,
			Code:    200,
			Message: "OK",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetInvestor)
	}
}

// GetSelect2Investor show all data product
func GetSelect2Investor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `"atn_users"."fullname"`
			sortBy = `asc`
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataUsers := 0

		dataUsers := services.GetUsers(db, search, utils.RoleInvestor, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataUsers := services.GetTotalDataUsers(db, utils.RoleInvestor, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataUsers = services.GetTotalDataFilteredUsers(db, search, utils.RoleInvestor, utils.DefaultIsDelete)
		}

		if len(dataUsers) < 1 {
			dataUsers = []models.User{}
		}

		responseGetUsers := struct {
			Code              int           `json:"code"`
			TotalData         int           `json:"totalData"`
			TotalFilteredData int           `json:"totalFilteredData"`
			Users             []models.User `json:"users"`
		}{200, totalDataUsers, totalFilteredDataUsers, dataUsers}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetUsers)
	}
}

// PostAddInvestor untuk menambahkan data investor
func PostAddInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()

		if errTransaction != nil {
			utils.HandleError(errTransaction, "")
		}

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			existEmail string
		)

		userUIDStr := utils.GenerateUID()
		uidSrcOfFund := utils.GenerateUID()
		uidBankAccount := utils.GenerateUID()
		uidDocument := utils.GenerateUID()
		uidHeir := utils.GenerateUID()
		uidBalance := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		salt := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime), userUIDStr))
		accessToken := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime), salt))
		accessTokenExpiry := currentDatetime
		role := "investor"
		isVerified := "0"
		isDelete := "0"
		defaultBalance := 0

		request := struct {
			Email    string `validate:"email,min:4,max:40"`
			Fullname string `validate:"min:2,max:40"`
			Password string `validate:"min:6,max:100"`
		}{req.FormValue("email"), req.FormValue("fullname"), req.FormValue("password")}

		utils.Validate(request)

		err := transaction.QueryRow(`SELECT atn_users.email FROM atn_users WHERE atn_users.email = $1 AND atn_users."isDelete" = $2`, request.Email, "0").Scan(&existEmail)

		utils.HandleError(err, "")

		if existEmail != "" || len(existEmail) > 0 {
			panic("Email telah digunakan")
		}

		password := utils.Encrypt([]byte(req.FormValue("password")), salt)

		_, errInsertUser := transaction.Exec(`insert into atn_users ("UID", fullname, email, password, role, "accessToken", salt, "accessTokenExpiry", "isVerified", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`, userUIDStr, request.Fullname, request.Email, password, role, accessToken, salt, accessTokenExpiry, isVerified, isDelete, currentDatetime, currentDatetime)

		_, errInsertUserSourceOfFunds := transaction.Exec(`insert into "atn_userSourceOfFunds" ("UID", "userUID", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5)`, uidSrcOfFund, userUIDStr, isDelete, currentDatetime, currentDatetime)

		_, errInsertUserBankAccount := transaction.Exec(`insert into "atn_userBankAccountInformations" ("UID", "userUID", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5)`, uidBankAccount, userUIDStr, isDelete, currentDatetime, currentDatetime)

		_, errInsertUserDocument := transaction.Exec(`insert into "atn_userDocumentInformations" ("UID", "userUID", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5)`, uidDocument, userUIDStr, isDelete, currentDatetime, currentDatetime)

		_, errInsertUserHeir := transaction.Exec(`insert into "atn_userHeirs" ("UID", "userUID", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5)`, uidHeir, userUIDStr, isDelete, currentDatetime, currentDatetime)

		_, errInsertUserBalance := transaction.Exec(`insert into "atn_userBalance" ("UID", "userUID", balance, "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6)`, uidBalance, userUIDStr, defaultBalance, isDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertUser, "")
		utils.HandleError(errInsertUserSourceOfFunds, "")
		utils.HandleError(errInsertUserBankAccount, "")
		utils.HandleError(errInsertUserDocument, "")
		utils.HandleError(errInsertUserHeir, "")
		utils.HandleError(errInsertUserBalance, "")

		responseAddInvestor := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data investor berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddInvestor)
	}
}

// PostEditInvestor used for change data investor
func PostEditInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			UserUID       string `validate:"required, max:200"`
			Email         string `validate:"required, email, max:100"`
			Fullname      string `validate:"required, max:100"`
			Phone         string `validate:"max:20"`
			Gender        string `validate:"max:20"`
			Pob           string `validate:"max:30"`
			Dob           string `validate:"max:30"`
			Address       string `validate:"max:50"`
			City          string `validate:"max:30"`
			Province      string `validate:"max:30"`
			PostalCode    string `validate:"max:30"`
			Nationallity  string `validate:"max:30"`
			Religion      string `validate:"max:30"`
			MaritalStatus string `validate:"max:30"`
		}{req.FormValue("userUID"), req.FormValue("email"), req.FormValue("fullname"), req.FormValue("phone"), req.FormValue("gender"), req.FormValue("pob"), req.FormValue("dob"), req.FormValue("address"), req.FormValue("city"), req.FormValue("province"), req.FormValue("postalCode"), req.FormValue("nationallity"), req.FormValue("religion"), req.FormValue("maritalStatus")}

		utils.Validate(request)

		investor := services.GetOneUser(db, req.FormValue("userUID"), "investor", "0")

		if investor.UID == "" || len(investor.UID) < 1 {
			utils.ThrowError("Data investor tidak ditemukan")
		}

		existData := services.GetExistEmailUser(db, req.FormValue("userUID"), req.FormValue("email"))
		existDataPhone := services.GetExistPhoneUser(db, req.FormValue("userUID"), req.FormValue("phone"))

		if existData.Email != "" || len(existData.Email) > 0 {
			utils.ThrowError("Email telah digunakan")
		}

		if existDataPhone.Phone != "" || len(existDataPhone.Phone) > 0 {
			utils.ThrowError("No. HP telah digunakan")
		}

		_, errUpdateUserData := transaction.Exec(`UPDATE "atn_users" SET "email" = $1, "fullname" = $2, "phone" = $3, "gender" = $4, "pob" = $5, "dob" = $6, "address" = $7, "city" = $8, "province" = $9, "postalCode" = $10, "nationallity" = $11, "religion" = $12, "maritalStatus" = $13 WHERE "UID" = $14`, req.FormValue("email"), req.FormValue("fullname"), req.FormValue("phone"), req.FormValue("gender"), req.FormValue("pob"), req.FormValue("dob"), req.FormValue("address"), req.FormValue("city"), req.FormValue("province"), req.FormValue("postalCode"), req.FormValue("nationallity"), req.FormValue("religion"), req.FormValue("maritalStatus"), req.FormValue("userUID"))

		utils.HandleError(errUpdateUserData, "")

		responseEditInvestor := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data investor berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditInvestor)
	}
}

// PostDeleteInvestor used for delete data investor
func PostDeleteInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			UserUID string `validate:"required, max:200"`
		}{req.FormValue("userUID")}

		utils.Validate(request)

		_, errUpdateUserData := transaction.Exec(`UPDATE "atn_users" SET "isDelete" = $1 WHERE "UID" = $2`, "1", req.FormValue("userUID"))

		utils.HandleError(errUpdateUserData, "")

		responseDeleteInvestor := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data investor berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteInvestor)
	}
}

// PostEditInvestorDocument untuk mengubah data dokumen investor
func PostEditInvestorDocument(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		ktpFile, handlerKtpFile, errKtpImg := req.FormFile("ktpImg")
		npwpFile, handlerNpwpFile, errNpwpImg := req.FormFile("npwpImg")

		var (
			targetKtpPath, targetNpwpPath, ktpFileExtension, npwpFileExtension string = "", "", "", ""
			ktpFileSize, npwpFileSize                                          int    = 0, 0
		)

		request := struct {
			Ktp  string `validate:"required, max:50"`
			Npwp string `validate:"required, max:50"`
		}{req.FormValue("ktp"), req.FormValue("npwp")}

		utils.Validate(request)

		if ktpFile != nil {
			ktpFileType := utils.GetFileContentTypeViaFormFile(ktpFile)

			if !(ktpFileType == utils.FileTypeJPEG || ktpFileType == utils.FileTypeJPG || ktpFileType == utils.FileTypePNG) {
				panic("Format foto ktp salah")
			}

			if errKtpImg != http.ErrMissingFile {
				ktpFileExtension = filepath.Ext(handlerKtpFile.Filename)
				ktpFileSize = int(handlerKtpFile.Size)
			}

			if ktpFileSize > 30000000 {
				panic("Ukuran file foto ktp terlalu besar")
			}

			filenameKtp := middleware.AuthUser.Fullname + utils.GenerateUID() + ktpFileExtension
			targetKtpPath = utils.UploadImage(ktpFile, handlerKtpFile, utils.KtpImgPath, filenameKtp, utils.KtpAliasPath)

			_, errUpdateKtpImage := transaction.Exec(`UPDATE "atn_userDocumentInformations" SET "ktpImg" = $1 WHERE "userUID" = $2`, targetKtpPath, middleware.AuthUser.UID)
			utils.HandleError(errUpdateKtpImage, "")
		}

		if npwpFile != nil {
			npwpFileType := utils.GetFileContentTypeViaFormFile(npwpFile)

			if !(npwpFileType == utils.FileTypeJPEG || npwpFileType == utils.FileTypeJPG || npwpFileType == utils.FileTypePNG) {
				panic("Format foto npwp salah")
			}

			if errNpwpImg != http.ErrMissingFile {
				npwpFileExtension = filepath.Ext(handlerNpwpFile.Filename)
				npwpFileSize = int(handlerNpwpFile.Size)
			}

			if npwpFileSize > 30000000 {
				panic("Ukuran file foto npwp terlalu besar")
			}

			filenameNpwp := middleware.AuthUser.Fullname + utils.GenerateUID() + npwpFileExtension
			targetNpwpPath = utils.UploadImage(npwpFile, handlerNpwpFile, utils.NpwpImgPath, filenameNpwp, utils.NpwpAliasPath)

			_, errUpdateNpwpImage := transaction.Exec(`UPDATE "atn_userDocumentInformations" SET "npwpImg" = $1 WHERE "userUID" = $2`, targetNpwpPath, middleware.AuthUser.UID)
			utils.HandleError(errUpdateNpwpImage, "")
		}

		_, errUpdateDocument := transaction.Exec(`UPDATE "atn_userDocumentInformations" SET "ktp" = $1, "npwp" = $2 WHERE "userUID" = $3`, request.Ktp, request.Npwp, middleware.AuthUser.UID)
		utils.HandleError(errUpdateDocument, "")

		responseEditUserDocument := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data dokumen berhasil diperbarui"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditUserDocument)
	}
}

// PostEditInvestorSourceOfFund untuk mengubah data sumber dana investor
func PostEditInvestorSourceOfFund(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			JobUID          string `validate:"required, max:200"`
			SourceOfFundUID string `validate:"required, max:200"`
		}{req.FormValue("jobUID"), req.FormValue("sourceOfFundUID")}

		utils.Validate(request)

		job := services.GetOneJob(db, request.JobUID, "0")
		sourceOfFund := services.GetOneSourceOfFund(db, request.SourceOfFundUID, "0")

		if job.UID == "" {
			panic("Data pekerjaan tidak ditemukan")
		}

		if sourceOfFund.UID == "" {
			panic("Data sumber dana tidak ditemukan")
		}

		_, errUpdateUserSourceOfFund := transaction.Exec(`UPDATE "atn_userSourceOfFunds" SET "jobUID" = $1, "sourceOfFundUID" = $2 WHERE "userUID" = $3`, request.JobUID, request.SourceOfFundUID, middleware.AuthUser.UID)

		utils.HandleError(errUpdateUserSourceOfFund, "")

		responseEditUserSourceOfFund := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data sumber dana berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditUserSourceOfFund)
	}
}

// PostEditInvestorBankAccount untuk mengubah data sumber dana investor
func PostEditInvestorBankAccount(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			BankUID       string `validate:"required, max:200"`
			Branch        string `validate:"required, max:50"`
			AccountNumber string `validate:"required, max:50"`
			AccountName   string `validate:"required, max:50"`
		}{req.FormValue("bankUID"), req.FormValue("branch"), req.FormValue("accountNumber"), req.FormValue("accountName")}

		utils.Validate(request)

		bank := services.GetOneBank(db, request.BankUID, "0")

		if bank.UID == "" {
			panic("Data bank tidak ditemukan")
		}

		_, errUpdateUserBankAccount := transaction.Exec(`UPDATE "atn_userBankAccountInformations" SET "bankUID" = $1, "branch" = $2, "accountNumber" = $3, "accountName" = $4 WHERE "userUID" = $5`, request.BankUID, request.Branch, request.AccountNumber, request.AccountName, middleware.AuthUser.UID)

		utils.HandleError(errUpdateUserBankAccount, "")

		responseEditUserBankAccount := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data informasi rekening berhasil diperbarui"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditUserBankAccount)
	}
}

// PostEditInvestorHeir untuk mengubah data ahli waris investor
func PostEditInvestorHeir(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			Fullname string `validate:"required, max:100"`
			Phone    string `validate:"required, max:50"`
			Address  string `validate:"required, max:100"`
		}{url.QueryEscape(req.FormValue("fullname")), req.FormValue("phone"), url.QueryEscape(req.FormValue("address"))}

		utils.Validate(request)

		_, errUpdateUserHeir := transaction.Exec(`UPDATE "atn_userHeirs" SET "fullname" = $1, "phone" = $2, "address" = $3 WHERE "userUID" = $4`, request.Fullname, request.Phone, request.Address, middleware.AuthUser.UID)

		utils.HandleError(errUpdateUserHeir, "")

		responseEditUserHeir := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data ahli waris berhasil diperbarui"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditUserHeir)
	}
}

// PostEditProfileInvestor edit investor current profile
func PostEditProfileInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			Email         string `validate:"required, email, max:100"`
			Fullname      string `validate:"required, max:100"`
			Phone         string `validate:"max:20"`
			Gender        string `validate:"max:20"`
			Pob           string `validate:"max:30"`
			Dob           string `validate:"max:30"`
			Address       string `validate:"max:50"`
			City          string `validate:"max:30"`
			Province      string `validate:"max:30"`
			PostalCode    string `validate:"max:30"`
			Nationallity  string `validate:"max:30"`
			Religion      string `validate:"max:30"`
			MaritalStatus string `validate:"max:30"`
		}{req.FormValue("email"), req.FormValue("fullname"), req.FormValue("phone"), req.FormValue("gender"), req.FormValue("pob"), req.FormValue("dob"), req.FormValue("address"), req.FormValue("city"), req.FormValue("province"), req.FormValue("postalCode"), req.FormValue("nationallity"), req.FormValue("religion"), req.FormValue("maritalStatus")}

		utils.Validate(request)

		existData := services.GetExistEmailUser(db, middleware.AuthUser.UID, req.FormValue("email"))
		existDataPhone := services.GetExistPhoneUser(db, middleware.AuthUser.UID, req.FormValue("phone"))

		if existData.Email != "" || len(existData.Email) > 0 {
			utils.ThrowError("Email telah digunakan")
		}

		if existDataPhone.Phone != "" || len(existDataPhone.Phone) > 0 {
			utils.ThrowError("No. HP telah digunakan")
		}

		_, errUpdateUserData := transaction.Exec(`UPDATE "atn_users" SET "email" = $1, "fullname" = $2, "phone" = $3, "gender" = $4, "pob" = $5, "dob" = $6, "address" = $7, "city" = $8, "province" = $9, "postalCode" = $10, "nationallity" = $11, "religion" = $12, "maritalStatus" = $13 WHERE "UID" = $14`, req.FormValue("email"), req.FormValue("fullname"), req.FormValue("phone"), req.FormValue("gender"), req.FormValue("pob"), req.FormValue("dob"), req.FormValue("address"), req.FormValue("city"), req.FormValue("province"), req.FormValue("postalCode"), req.FormValue("nationallity"), req.FormValue("religion"), req.FormValue("maritalStatus"), middleware.AuthUser.UID)

		utils.HandleError(errUpdateUserData, "")

		responsePostEditProfileInvestor := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data profile berhasil diperbarui"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responsePostEditProfileInvestor)
	}
}

// GetBalanceInvestor show investor current balance
func GetBalanceInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		userUID := middleware.AuthUser.UID

		userBalance := services.GetUserBalance(db, userUID, utils.DefaultIsDelete)

		responseGetBalanceInvestor := struct {
			Code    int     `json:"code"`
			Balance float64 `json:"balance"`
		}{200, userBalance.Balance}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetBalanceInvestor)
	}
}

// GetProfileInvestor show investor current profile
func GetProfileInvestor(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		userUID := middleware.AuthUser.UID

		investor := services.GetOneUser(db, userUID, "investor", utils.DefaultIsDelete)
		investorSourceOfFund := services.GetOneUserSourceOfFund(db, userUID, utils.DefaultIsDelete)
		investorBankAccount := services.GetOneUserBankAccountInformation(db, userUID, utils.DefaultIsDelete)
		investorDocument := services.GetOneUserDocument(db, userUID, utils.DefaultIsDelete)
		investorHeir := services.GetOneUserHeir(db, userUID, utils.DefaultIsDelete)

		isInvestorDataComplete := 1
		verificationAccountMessage := ""

		if investor.Phone == "" || investor.Gender == "" || investor.Pob == "" || investor.Dob == "" || investor.Address == "" || investor.City == "" || investor.Province == "" || investor.PostalCode == "" || investor.Nationallity == "" || investor.Religion == "" || investor.MaritalStatus == "" {
			isInvestorDataComplete = 0
			verificationAccountMessage += `- Silahkan lengkapi data profile anda, pada menu Data Profil`
		}

		if investorBankAccount.BankUID == "" || investorBankAccount.Branch == "" || investorBankAccount.AccountNumber == "" || investorBankAccount.AccountName == "" {
			isInvestorDataComplete = 0
			verificationAccountMessage += `<br><br>- Silahkan lengkapi data informasi rekening anda, pada menu Data Informasi Rekening`
		}

		if investorDocument.Ktp == "" || investorDocument.Npwp == "" || investorDocument.KtpImg == "" || investorDocument.NpwpImg == "" {
			isInvestorDataComplete = 0
			verificationAccountMessage += `<br><br>- Silahkan lengkapi data informasi dokumen anda, pada menu Data Informasi Dokumen`
		}

		if investorHeir.Fullname == "" || investorHeir.Phone == "" || investorHeir.Address == "" {
			isInvestorDataComplete = 0
			verificationAccountMessage += `<br><br>- Silahkan lengkapi data ahli waris anda, pada menu Data Ahli Waris`
		}

		if investorSourceOfFund.JobUID == "" || investorSourceOfFund.SourceOfFundUID == "" {
			isInvestorDataComplete = 0
			verificationAccountMessage += `<br><br>- Silahkan lengkapi data sumber dana anda, pada menu Data Sumber Dana`
		}

		responseGetProfileInvestor := struct {
			Code                       int         `json:"code"`
			IsInvestorDataComplete     int         `json:"isInvestorDataComplete"`
			VerificationAccountMessage string      `json:"verificationAccountMessage"`
			User                       models.User `json:"user"`
		}{200, isInvestorDataComplete, verificationAccountMessage, investor}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetProfileInvestor)
	}
}

// GetInvestorSourceOfFund show investor current source of fund
func GetInvestorSourceOfFund(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 50
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = "name"
			sortBy = "asc"
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy

		selectedSourceOfFundData := services.GetOneUserSourceOfFund(db, middleware.AuthUser.UID, utils.DefaultIsDelete)
		jobs := services.GetJobs(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)
		sourceOfFunds := services.GetSourceOfFunds(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)

		responseGetSourceOfFundInvestor := struct {
			Code                     int                      `json:"code"`
			SelectedSourceOfFundData models.UserSourceOfFunds `json:"selectedSourceOfFundData"`
			Jobs                     []models.Job             `json:"jobs"`
			SourceOfFunds            []models.SourceOfFunds   `json:"sourceOfFunds"`
		}{200, selectedSourceOfFundData, jobs, sourceOfFunds}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetSourceOfFundInvestor)
	}
}

// GetInvestorBankAccount show investor current bank account
func GetInvestorBankAccount(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 50
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = "name"
			sortBy = "asc"
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy

		selectedBankAccountInformation := services.GetOneUserBankAccountInformation(db, middleware.AuthUser.UID, utils.DefaultIsDelete)
		banks := services.GetBanks(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)

		responseGetBankAccountInvestor := struct {
			Code                               int                               `json:"code"`
			SelectedBankAccountInformationData models.UserBankAccountInformation `json:"selectedBankAccountInformationData"`
			Banks                              []models.Bank                     `json:"banks"`
		}{200, selectedBankAccountInformation, banks}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetBankAccountInvestor)
	}
}

// GetInvestorHeir show investor current heir
func GetInvestorHeir(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		userHeir := services.GetOneUserHeir(db, middleware.AuthUser.UID, utils.DefaultIsDelete)

		responseGetHeirInvestor := struct {
			Code         int             `json:"code"`
			UserHeirData models.UserHeir `json:"userHeirData"`
		}{200, userHeir}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetHeirInvestor)
	}
}

// GetInvestorDocument show investor current document information
func GetInvestorDocument(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		userDocument := services.GetOneUserDocument(db, middleware.AuthUser.UID, utils.DefaultIsDelete)

		responseGetDcoumentInvestor := struct {
			Code             int                            `json:"code"`
			UserDocumentData models.UserDocumentInformation `json:"userDocumentData"`
		}{200, userDocument}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetDcoumentInvestor)
	}
}
