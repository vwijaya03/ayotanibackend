package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"golang.org/x/time/rate"
)

// PostAdminLogin untuk login superadmin
func PostAdminLogin(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		var (
			UID, email, fullname, phone, gender, role, accessToken, accessTokenExpiry, salt sql.NullString

			password []byte
		)

		request := struct {
			Email    string `validate:"email,min:4,max:40"`
			Password string `validate:"min:6,max:100"`
		}{req.FormValue("email"), req.FormValue("password")}

		utils.Validate(request)

		err := db.QueryRow(`SELECT atn_users."UID", atn_users.email, atn_users.password, atn_users.fullname, atn_users.phone, atn_users.gender, atn_users.role, atn_users."accessToken", atn_users."accessTokenExpiry", atn_users.salt FROM atn_users WHERE atn_users.email = $1 AND atn_users."isDelete" = $2`, request.Email, "0").Scan(&UID, &email, &password, &fullname, &phone, &gender, &role, &accessToken, &accessTokenExpiry, &salt)

		if email.String == "" || len(email.String) < 1 {
			panic("Email atau password anda salah")
		}

		actualPassword := utils.Decrypt(password, salt.String)

		if request.Password != string(actualPassword) {
			panic("Email atau password anda salah")
		}

		var (
			newAccessToken, newAccessTokenExpiry string
		)

		isUpdateNewToken := false

		tokenExpiryDate, err := time.Parse("2006-01-02 15:04:05", accessTokenExpiry.String)
		currentDatetime, err := time.Parse("2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05"))

		diff := tokenExpiryDate.Sub(currentDatetime)

		if int(diff.Hours()/24) <= 3 {
			uidStr := utils.GenerateUID()
			currentDatetime := time.Now().Format("2006-01-02 15:04:05")

			newSalt := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime+uidStr), uidStr))
			newAccessTokenExpiry = time.Now().AddDate(0, 1, 0).Format("2006-01-02 15:04:05")
			newAccessToken = fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime+uidStr), newSalt))

			_, errUpdateToken := db.Exec(`UPDATE atn_users SET "accessToken" = $1, "accessTokenExpiry" = $2 WHERE email = $3`, newAccessToken, newAccessTokenExpiry, request.Email)

			if errUpdateToken != nil {
				panic(errUpdateToken.Error())
			}

			isUpdateNewToken = true
		}

		user := models.Superadmin{
			UID:               UID.String,
			Email:             email.String,
			Fullname:          fullname.String,
			Phone:             phone.String,
			Gender:            gender.String,
			Role:              role.String,
			AccessToken:       accessToken.String,
			AccessTokenExpiry: accessTokenExpiry.String,
		}

		credential := models.Credential{
			AccessToken:       accessToken.String,
			AccessTokenExpiry: accessTokenExpiry.String,
		}

		if isUpdateNewToken {
			credential.AccessToken = newAccessToken
			credential.AccessTokenExpiry = newAccessTokenExpiry
		}

		responseLogin := struct {
			Code       int               `json:"code"`
			Credential models.Credential `json:"credential"`
			User       models.Superadmin `json:"user"`
		}{200, credential, user}

		if err != nil {
			panic(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseLogin)
	}
}

// PostAdminLogout untuk logout superadmin
func PostAdminLogout(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		var (
			UID sql.NullString
		)

		request := struct {
			Token string `validate:"required,max:200"`
		}{req.Header.Get("token")}

		utils.Validate(request)

		err := db.QueryRow(`SELECT atn_users."UID" FROM atn_users WHERE atn_users."accessToken" = $1 AND atn_users."isDelete" = $2`, request.Token, "0").Scan(&UID)

		if UID.String == "" || len(UID.String) < 1 {
			panic("Request ditolak")
		}

		if err != nil {
			panic(err.Error())
		}

		_, errUpdateToken := db.Exec(`UPDATE atn_users SET "accessTokenExpiry" = $1 WHERE "accessToken" = $2`, "1970-01-01 00:00:00", request.Token)

		if errUpdateToken != nil {
			panic(errUpdateToken.Error())
		}

		responseLogout := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Logout berhasil"}

		if err != nil {
			panic(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseLogout)
	}
}

// GetSuperadmin show all data superadmin
func GetSuperadmin(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			UID, email, fullname, phone, gender, role sql.NullString
			dataUsers                                 []models.Superadmin
		)

		rowsDBSuperadmin, errGetSuperadmin := db.Query(`SELECT atn_users."UID", atn_users.email, atn_users.fullname, atn_users.phone, atn_users.gender, atn_users.role FROM atn_users WHERE atn_users.role = $1 AND atn_users."isDelete" = $2`, "superadmin", "0")
		defer rowsDBSuperadmin.Close()

		utils.HandleError(errGetSuperadmin, "")

		for rowsDBSuperadmin.Next() {
			rowsDBSuperadmin.Scan(&UID, &email, &fullname, &phone, &gender, &role)

			decodedFullname, errDecode := url.QueryUnescape(fullname.String)
			utils.HandleError(errDecode, "")

			dataUsers = append(dataUsers, models.Superadmin{UID: UID.String, Email: email.String, Fullname: decodedFullname, Phone: phone.String, Gender: gender.String, Role: role.String})
		}

		if len(dataUsers) < 1 {
			dataUsers = []models.Superadmin{}
		}

		responseGetSuperadmin := models.ResGetSuperadmin{
			Result:  dataUsers,
			Code:    200,
			Message: "OK",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetSuperadmin)
	}
}

// PostAddSuperadmin untuk menambahkan data superadmin
func PostAddSuperadmin(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			existEmail string
		)

		userUIDStr := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		salt := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime), userUIDStr))
		accessToken := fmt.Sprintf("%x", utils.Encrypt([]byte(currentDatetime), salt))
		accessTokenExpiry := currentDatetime
		role := "superadmin"
		isDelete := "0"

		request := struct {
			Email    string `validate:"email,min:4,max:40"`
			Fullname string `validate:"min:2,max:40"`
			Password string `validate:"min:6,max:100"`
		}{req.FormValue("email"), url.QueryEscape(req.FormValue("fullname")), req.FormValue("password")}

		utils.Validate(request)

		err := transaction.QueryRow(`SELECT atn_users.email FROM atn_users WHERE atn_users.email = $1 AND atn_users."isDelete" = $2`, request.Email, "0").Scan(&existEmail)

		utils.HandleError(err, "")

		if existEmail != "" || len(existEmail) > 0 {
			panic("Email telah digunakan")
		}

		password := utils.Encrypt([]byte(req.FormValue("password")), salt)

		_, errInsertUser := transaction.Exec(`insert into atn_users ("UID", fullname, email, password, role, "accessToken", salt, "accessTokenExpiry", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`, userUIDStr, request.Fullname, request.Email, password, role, accessToken, salt, accessTokenExpiry, isDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertUser, "")

		responseAddSuperadmin := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data superadmin berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddSuperadmin)
	}
}

// PostEditSuperadmin untuk mengubah data superadmin
func PostEditSuperadmin(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			existEmail, existPhone, validUID, validSalt string
		)

		isDelete := "0"

		request := struct {
			UID      string `validate:"required,max:200"`
			Email    string `validate:"required,max:40"`
			Fullname string `validate:"required,max:40"`
			Phone    string `validate:"max:20"`
			Gender   string `validate:"max:20"`
			Password string `validate:"max:20"`
		}{req.FormValue("uid"), req.FormValue("email"), url.QueryEscape(req.FormValue("fullname")), req.FormValue("phone"), req.FormValue("gender"), req.FormValue("password")}

		utils.Validate(request)

		errUserNotFound := transaction.QueryRow(`SELECT atn_users."UID", atn_users.salt FROM atn_users WHERE atn_users."UID" = $1 AND atn_users."isDelete" = $2`, request.UID, isDelete).Scan(&validUID, &validSalt)

		utils.HandleError(errUserNotFound, "")

		if validUID == "" || len(validUID) < 5 {
			panic("Data superadmin tidak ditemukan")
		}

		errExistEmail := transaction.QueryRow(`SELECT atn_users.email FROM atn_users WHERE atn_users."UID" <> $1 AND atn_users.email = $2 AND atn_users."isDelete" = $3`, request.UID, request.Email, isDelete).Scan(&existEmail)
		errExistPhone := transaction.QueryRow(`SELECT atn_users.phone FROM atn_users WHERE atn_users."UID" <> $1 AND atn_users.phone = $2 AND atn_users."isDelete" = $3`, request.UID, request.Phone, isDelete).Scan(&existPhone)

		utils.HandleError(errExistEmail, "")
		utils.HandleError(errExistPhone, "")

		if existEmail != "" || len(existEmail) > 0 {
			panic("Email telah digunakan")
		}

		if existPhone != "" || len(existPhone) > 0 {
			panic("No. HP telah digunakan")
		}

		if request.Password != "" {
			password := utils.Encrypt([]byte(req.FormValue("password")), validSalt)

			_, errUpdateSuperadmin := transaction.Exec(`UPDATE atn_users SET email = $1, fullname = $2, phone = $3, gender = $4, password = $5 WHERE "UID" = $6 AND "isDelete" = $7`, request.Email, request.Fullname, request.Phone, request.Gender, password, request.UID, isDelete)
			utils.HandleError(errUpdateSuperadmin, "")
		} else {
			_, errUpdateSuperadmin := transaction.Exec(`UPDATE atn_users SET email = $1, fullname = $2, phone = $3, gender = $4 WHERE "UID" = $5 AND "isDelete" = $6`, request.Email, request.Fullname, request.Phone, request.Gender, request.UID, isDelete)
			utils.HandleError(errUpdateSuperadmin, "")
		}

		responseEditSuperadmin := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data superadmin berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditSuperadmin)
	}
}

// PostDeleteSuperadmin untuk menghapus data superadmin
func PostDeleteSuperadmin(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			validUID string
		)

		isDelete := "0"
		isDeleted := "1"

		request := struct {
			UID string `validate:"required,max:200"`
		}{req.FormValue("uid")}

		utils.Validate(request)

		errUserNotFound := transaction.QueryRow(`SELECT atn_users."UID" FROM atn_users WHERE atn_users."UID" = $1 AND atn_users."isDelete" = $2`, request.UID, isDelete).Scan(&validUID)

		utils.HandleError(errUserNotFound, "")

		if validUID == "" || len(validUID) < 5 {
			panic("Data superadmin tidak ditemukan")
		}

		_, errDeleteSuperadmin := transaction.Exec(`UPDATE atn_users SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, isDeleted, request.UID, isDelete)
		utils.HandleError(errDeleteSuperadmin, "")

		responseDeleteSuperadmin := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data superadmin berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteSuperadmin)
	}
}
