package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"golang.org/x/time/rate"
)

// GetJob show all data job
func GetJob(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 50
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = "name"
			sortBy = "asc"
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataJobs := 0

		dataJobs := services.GetJobs(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataJobs := services.GetTotalDataJob(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataJobs = services.GetTotalDataFilteredJob(db, search, utils.DefaultIsDelete)
		}

		if len(dataJobs) < 1 {
			dataJobs = []models.Job{}
		}

		responseGetJob := struct {
			Code              int          `json:"code"`
			TotalData         int          `json:"totalData"`
			TotalFilteredData int          `json:"totalFilteredData"`
			Jobs              []models.Job `json:"jobs"`
		}{200, totalDataJobs, totalFilteredDataJobs, dataJobs}

		// responseGetJob := models.ResGetData{
		// 	Result:  dataJobs,
		// 	Code:    200,
		// 	Message: "OK",
		// }

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetJob)
	}
}

// PostAddJob untuk menambahkan data job
func PostAddJob(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()

		if errTransaction != nil {
			utils.HandleError(errTransaction, "")
		}

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		uidJob := utils.GenerateUID()

		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		defaultIsDelete := "0"

		request := struct {
			Name string `validate:"min:2,max:40"`
		}{url.QueryEscape(req.FormValue("name"))}

		utils.Validate(request)

		_, errInsertJob := transaction.Exec(`insert into atn_jobs ("UID", name, "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5)`, uidJob, request.Name, defaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertJob, "")

		responseAddJob := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data job berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddJob)
	}
}

// PostEditJob untuk mengubah data job
func PostEditJob(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			validUID sql.NullString
		)

		defaultIsDelete := "0"

		request := struct {
			UID  string `validate:"required,max:200"`
			Name string `validate:"required,max:40"`
		}{req.FormValue("uid"), url.QueryEscape(req.FormValue("name"))}

		utils.Validate(request)

		errJobNotFound := transaction.QueryRow(`SELECT atn_jobs."UID" FROM atn_jobs WHERE atn_jobs."UID" = $1 AND atn_jobs."isDelete" = $2`, request.UID, defaultIsDelete).Scan(&validUID)

		utils.HandleError(errJobNotFound, "")

		if validUID.String == "" || len(validUID.String) < 5 {
			panic("Data job tidak ditemukan")
		}

		_, errUpdateJob := transaction.Exec(`UPDATE atn_jobs SET name = $1 WHERE "UID" = $2 AND "isDelete" = $3`, request.Name, request.UID, defaultIsDelete)

		utils.HandleError(errUpdateJob, "")

		responseEditJob := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data job berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditJob)
	}
}

// PostDeleteJob untuk menghapus data job
func PostDeleteJob(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			validUID string
		)

		defaultIsDelete := "0"
		defaultIsDeleted := "1"

		request := struct {
			UID string `validate:"required,max:200"`
		}{req.FormValue("uid")}

		utils.Validate(request)

		errJobNotFound := transaction.QueryRow(`SELECT atn_jobs."UID" FROM atn_jobs WHERE atn_jobs."UID" = $1 AND atn_jobs."isDelete" = $2`, request.UID, defaultIsDelete).Scan(&validUID)

		utils.HandleError(errJobNotFound, "")

		if validUID == "" || len(validUID) < 5 {
			panic("Data job tidak ditemukan")
		}

		_, errDeleteJob := transaction.Exec(`UPDATE atn_jobs SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, defaultIsDeleted, request.UID, defaultIsDelete)
		utils.HandleError(errDeleteJob, "")

		responseDeleteJob := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data job berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteJob)
	}
}
