package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"golang.org/x/time/rate"
)

// GetSOF show all data source of fund
func GetSOF(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 50
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = "name"
			sortBy = "asc"
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataSourceOfFunds := 0

		dataSourceOfFunds := services.GetSourceOfFunds(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataSourceOfFunds := services.GetTotalDataSourceOfFund(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataSourceOfFunds = services.GetTotalDataFilteredSourceOfFund(db, search, utils.DefaultIsDelete)
		}

		if len(dataSourceOfFunds) < 1 {
			dataSourceOfFunds = []models.SourceOfFunds{}
		}

		responseGetSOF := struct {
			Code              int                    `json:"code"`
			TotalData         int                    `json:"totalData"`
			TotalFilteredData int                    `json:"totalFilteredData"`
			SourceOfFunds     []models.SourceOfFunds `json:"sourceOfFunds"`
		}{200, totalDataSourceOfFunds, totalFilteredDataSourceOfFunds, dataSourceOfFunds}

		// responseGetSOF := models.ResGetData{
		// 	Result:  dataSourceOfFunds,
		// 	Code:    200,
		// 	Message: "OK",
		// }

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetSOF)
	}
}

// PostAddSOF untuk menambahkan data source of fund
func PostAddSOF(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()

		if errTransaction != nil {
			utils.HandleError(errTransaction, "")
		}

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		uidSOF := utils.GenerateUID()

		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		defaultIsDelete := "0"

		request := struct {
			Name string `validate:"min:2,max:40"`
		}{url.QueryEscape(req.FormValue("name"))}

		utils.Validate(request)

		_, errInsertSOF := transaction.Exec(`insert into "atn_sourceOfFunds" ("UID", name, "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5)`, uidSOF, request.Name, defaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertSOF, "")

		responseAddSOF := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data sumber dana berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddSOF)
	}
}

// PostEditSOF untuk mengubah data source of fund
func PostEditSOF(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			validUID sql.NullString
		)

		defaultIsDelete := "0"

		request := struct {
			UID  string `validate:"required,max:200"`
			Name string `validate:"required,max:40"`
		}{req.FormValue("uid"), url.QueryEscape(req.FormValue("name"))}

		utils.Validate(request)

		errSOFNotFound := transaction.QueryRow(`SELECT "atn_sourceOfFunds"."UID" FROM "atn_sourceOfFunds" WHERE "atn_sourceOfFunds"."UID" = $1 AND "atn_sourceOfFunds"."isDelete" = $2`, request.UID, defaultIsDelete).Scan(&validUID)

		utils.HandleError(errSOFNotFound, "")

		if validUID.String == "" || len(validUID.String) < 5 {
			panic("Data sumber dana tidak ditemukan")
		}

		_, errUpdateSOF := transaction.Exec(`UPDATE "atn_sourceOfFunds" SET name = $1 WHERE "UID" = $2 AND "isDelete" = $3`, request.Name, request.UID, defaultIsDelete)

		utils.HandleError(errUpdateSOF, "")

		responseEditSOF := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data sumber dana berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditSOF)
	}
}

// PostDeleteSOF untuk menghapus data source of fund
func PostDeleteSOF(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			validUID string
		)

		defaultIsDelete := "0"
		defaultIsDeleted := "1"

		request := struct {
			UID string `validate:"required,max:200"`
		}{req.FormValue("uid")}

		utils.Validate(request)

		errSOFNotFound := transaction.QueryRow(`SELECT "atn_sourceOfFunds"."UID" FROM "atn_sourceOfFunds" WHERE "atn_sourceOfFunds"."UID" = $1 AND "atn_sourceOfFunds"."isDelete" = $2`, request.UID, defaultIsDelete).Scan(&validUID)

		utils.HandleError(errSOFNotFound, "")

		if validUID == "" || len(validUID) < 5 {
			panic("Data sumber dana tidak ditemukan")
		}

		_, errDeleteSOF := transaction.Exec(`UPDATE "atn_sourceOfFunds" SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, defaultIsDeleted, request.UID, defaultIsDelete)
		utils.HandleError(errDeleteSOF, "")

		responseDeleteSOF := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data sumber dana berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteSOF)
	}
}
