package controllers

import (
	"ayotani/middleware"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"

	"golang.org/x/time/rate"
)

// PostUpdateDeviceToken used for update device token users
func PostUpdateDeviceToken(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			DeviceToken string `validate:"max:200"`
		}{req.FormValue("deviceToken")}

		utils.Validate(request)

		_, errPostUpdateDeviceToken := transaction.Exec(`UPDATE atn_users SET "deviceToken" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, request.DeviceToken, middleware.AuthUser.UID, utils.DefaultIsDelete)
		utils.HandleError(errPostUpdateDeviceToken, "")

		responsePostUpdateDeviceToken := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Device token updated"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responsePostUpdateDeviceToken)
	}
}
