package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/time/rate"
)

// PostShareInvestmentProfit untuk memproses hasil investasi yang sudah jatuh tempo sesuai periodenya
func PostShareInvestmentProfit(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()

		if errTransaction != nil {
			utils.HandleError(errTransaction, "")
		}

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		roi := utils.ParseFloat64(req.FormValue("roi"))

		request := struct {
			ProductUID string  `validate:"required,min:1,max:100"`
			ROI        float64 `validate:"required,gt:10,lt:30"`
		}{req.FormValue("productUID"), roi}

		utils.Validate(request)

		product := services.GetOneProductForTransaction(db, request.ProductUID, utils.DefaultIsAvailableProduct, utils.DefaultIsDelete)

		if product.UID == "" || len(product.UID) < 5 {
			panic("Data investasi tidak ditemukan")
		}

		transactions := services.GetTransactionsForShareInvestmentProfit(db, product.UID, utils.DefaultTransactionTypeProductAyoTani, utils.DefaultIsDelete)

		productCreatedDate, errProductCreatedDate := time.Parse("2006-01-02T15:04:05Z", product.CreatedAt)
		utils.HandleError(errProductCreatedDate, "")
		productFinalDate := productCreatedDate.AddDate(0, product.Period, 0)
		totalDays := int(productFinalDate.Sub(productCreatedDate).Hours() / 24)
		from := utils.DefaultFromForEmail
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		for _, v := range transactions {
			transactionDate, errTransactionDate := time.Parse("2006-01-02T15:04:05Z", v.CreatedAt)
			utils.HandleError(errTransactionDate, "")

			transactionDays := int(productFinalDate.Sub(transactionDate).Hours() / 24)
			rate := math.Floor(((float64(transactionDays)/float64(totalDays))*100)*100) / 10000
			rateForInfo := rate * 100
			returnBalance := v.Amount + (((request.ROI / 100) * v.Amount) * rate)

			if transactionDays >= 1 && transactionDays <= totalDays {
				// fmt.Printf("%v / %v createdAt %s rate %.2f return balance %.1f transaction UID %s \n", transactionDays, totalDays, v.CreatedAt, rate, returnBalance, v.UID)
				// fmt.Println(rate)

				userBalance := services.GetUserBalance(db, v.UserUID, utils.DefaultIsDelete)
				newBalance := userBalance.Balance + returnBalance

				services.UpdateUserBalance(transaction, v.UserUID, newBalance, utils.DefaultIsDelete)

				balanceLog := models.BalanceLog{
					FromUserUID:       v.UserUID,
					ToUserUID:         v.UserUID,
					TotalAmount:       returnBalance,
					Description:       fmt.Sprintf(`Hasil akhir investasi %v unit %s.<br>ROI Panen %.0f %s.<br>ROI H-Panen %.0f %s`, v.Quantity, product.Title, request.ROI, "%", rate*100, "%"),
					DescriptionSystem: fmt.Sprintf(`Hasil akhir investasi %v unit %s, productUID %s, dengan rate %v / %v, ROI Panen %.0f %s ROI H-Panen %.0f %s`, v.Quantity, product.Title, product.UID, transactionDays, totalDays, request.ROI, "%", rate*100, "%"),
					Type:              "4",
					IsDelete:          utils.DefaultIsDelete,
				}

				services.InsertBalanceLogs(transaction, balanceLog)

				user := services.GetOneUser(db, v.UserUID, utils.RoleInvestor, utils.DefaultIsDelete)
				title := fmt.Sprintf("Hasil Akhir Unit Investasi %s Telah Dibagikan", product.Title)
				body := "Silahkan cek riwayat transaksi atau email anda untuk informasi lebih detil"
				fullname := user.Fullname
				to := user.Email
				subject := title
				currentDatetimeEmail, _ := time.Parse("2006-01-02 15:04:05", currentDatetime)
				currentDatetimeEmailStr := currentDatetimeEmail.Format("02 January 2006 15:04:05")
				transactionType := "Hasil Akhir Investasi"
				transactionStatus := "Transaksi Berhasil"
				transactionReferralCode := v.UID
				fund := utils.FormatRupiah(int(v.Amount), "Rp.")
				returnBalanceEmail := utils.FormatRupiah(int(returnBalance), "Rp.")
				roiEmail := strconv.FormatFloat(request.ROI, 'f', -1, 64) + " %"
				roiHPanenEmail := strconv.FormatFloat(rateForInfo, 'f', -1, 64) + " %"
				quantityEmail := utils.ParseString(v.Quantity)

				go func() { utils.SendNotification(title, body, "", user.DeviceToken, "") }()
				go func() {
					utils.SendEmailShareInvestment(fullname, from, to, subject, currentDatetimeEmailStr, transactionType, product.Title, quantityEmail, fund, returnBalanceEmail, roiEmail, roiHPanenEmail, transactionReferralCode, transactionStatus)
				}()
			}

		}

		_, errUpdateProduct := transaction.Exec(`UPDATE "atn_products" SET "available" = $1, "roi" = $2 WHERE "UID" = $3 AND "isDelete" = $4`, utils.DefaultIsNotAvailableProduct, request.ROI, product.UID, utils.DefaultIsDelete)
		utils.HandleError(errUpdateProduct, "")

		responseShareInvestmentProfit := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Proses pembagian hasil investasi telah selesai diproses"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseShareInvestmentProfit)
	}
}
