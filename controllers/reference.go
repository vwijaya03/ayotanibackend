package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"net/url"
	"path/filepath"
	"time"

	"golang.org/x/time/rate"
)

// GetReference show all data reference
func GetReference(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataReferences := 0

		dataReferences := services.GetReferencesForAdmin(db, search, completeSortBy, limit, offset, "0")
		totalDataReferences := services.GetTotalDataReference(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataReferences = services.GetTotalDataFilteredNews(db, search, utils.DefaultIsDelete)
		}

		if len(dataReferences) < 1 {
			dataReferences = []models.Reference{}
		}

		responseGetReferences := struct {
			Code              int                `json:"code"`
			TotalData         int                `json:"totalData"`
			TotalFilteredData int                `json:"totalFilteredData"`
			Newses            []models.Reference `json:"newses"`
		}{200, totalDataReferences, totalFilteredDataReferences, dataReferences}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetReferences)
	}
}

// PostAddReference used for add new data reference
func PostAddReference(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			targetThumbnailPath, thumbnailFileExtension string = "", ""
		)

		uidReference := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		defaultIsDelete := "0"
		thumbnailFileSize := 0

		thumbnailFile, handlerThumbnailFile, errThumbnailImg := req.FormFile("thumbnailImg")

		if errThumbnailImg != http.ErrMissingFile {
			thumbnailFileExtension = filepath.Ext(handlerThumbnailFile.Filename)
			thumbnailFileSize = int(handlerThumbnailFile.Size)
		}

		request := struct {
			Title                  string `validate:"required, max:200"`
			Description            string `validate:"required"`
			ThumbnailFileSize      int    `validate:"required"`
			ThumbnailFileExtension string `validate:"required, img"`
		}{url.QueryEscape(req.FormValue("title")), url.QueryEscape(req.FormValue("description")), thumbnailFileSize, thumbnailFileExtension}

		utils.Validate(request)

		filenameThumbnail := "ref-thumbnail-" + utils.GenerateUID() + thumbnailFileExtension
		targetThumbnailPath = utils.UploadImage(thumbnailFile, handlerThumbnailFile, utils.ThumbnailImgPath, filenameThumbnail, utils.ThumbnailAliasPath)

		_, errInsertReference := transaction.Exec(`insert into atn_references ("UID", "userUID", "title", "desc", "thumbnail", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`, uidReference, middleware.AuthUser.UID, request.Title, request.Description, targetThumbnailPath, defaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertReference, "")

		responseAddReference := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data reference berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddReference)
	}
}

// PostEditReference used for to change data reference
func PostEditReference(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			targetThumbnailPath, thumbnailFileExtension string = "", ""
		)

		defaultIsDelete := "0"
		thumbnailFile, handlerThumbnailFile, errThumbnailImg := req.FormFile("thumbnailImg")

		if errThumbnailImg != http.ErrMissingFile {
			thumbnailFileExtension = filepath.Ext(handlerThumbnailFile.Filename)
		}

		request := struct {
			ReferenceUID string `validate:"required,max:200"`
			Title        string `validate:"required,max:200"`
			Description  string `validate:"required,max:5000"`
		}{req.FormValue("referenceUID"), url.QueryEscape(req.FormValue("title")), url.QueryEscape(req.FormValue("description"))}

		utils.Validate(request)

		reference := services.GetOneReference(db, request.ReferenceUID, "0")

		if reference.UID == "" || len(reference.UID) < 1 {
			panic("Data reference tidak ditemukan")
		}

		if thumbnailFile != nil {
			if !(thumbnailFileExtension == utils.ExtJPG || thumbnailFileExtension == utils.ExtJPEG || thumbnailFileExtension == utils.ExtPNG) {
				panic("Format thumbnail salah")
			}

			filenameThumbnail := "ref-thumbnail-" + utils.GenerateUID() + thumbnailFileExtension
			targetThumbnailPath = utils.UploadImage(thumbnailFile, handlerThumbnailFile, utils.ThumbnailImgPath, filenameThumbnail, utils.ThumbnailAliasPath)

			_, errUpdateReference := transaction.Exec(`UPDATE atn_references SET "userUID" = $1, "title" = $2, "desc" = $3, "thumbnail" = $4 WHERE "UID" = $5 AND "isDelete" = $6`, middleware.AuthUser.UID, request.Title, request.Description, targetThumbnailPath, request.ReferenceUID, defaultIsDelete)

			utils.HandleError(errUpdateReference, "")
			utils.DeleteImage(utils.ThumbnailImgPath + reference.Thumbnail)
		} else {
			_, errUpdateReference := transaction.Exec(`UPDATE atn_references SET "userUID" = $1, "title" = $2, "desc" = $3 WHERE "UID" = $4 AND "isDelete" = $5`, middleware.AuthUser.UID, request.Title, request.Description, request.ReferenceUID, defaultIsDelete)

			utils.HandleError(errUpdateReference, "")
		}

		responseEditReference := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data reference berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditReference)
	}
}

// PostDeleteReference used for to delete data reference
func PostDeleteReference(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		defaultIsDelete := "0"
		defaultIsDeleted := "1"

		request := struct {
			ReferenceUID string `validate:"required,max:200"`
		}{req.FormValue("referenceUID")}

		utils.Validate(request)

		_, errDeleteReference := transaction.Exec(`UPDATE atn_references SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, defaultIsDeleted, request.ReferenceUID, defaultIsDelete)

		utils.HandleError(errDeleteReference, "")

		responseDeleteBank := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data reference berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteBank)
	}
}

// GetIvReference show all data reference
func GetIvReference(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Public(db)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `atn_references."createdAt"`
			sortBy = `desc`
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy

		dataReferences := services.GetReferences(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)

		if len(dataReferences) < 1 {
			dataReferences = []models.Reference{}
		}

		responseGetReferences := struct {
			Code   int                `json:"code"`
			Result []models.Reference `json:"results"`
		}{200, dataReferences}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetReferences)
	}
}
