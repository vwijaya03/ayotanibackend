package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"golang.org/x/time/rate"
)

// GetPartner show all data partner
func GetPartner(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 50
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = "fullname"
			sortBy = "asc"
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataPartners := 0

		dataPartners := services.GetPartners(db, search, completeSortBy, limit, offset, "0")
		totalDataPartners := services.GetTotalDataPartner(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataPartners = services.GetTotalDataFilteredPartner(db, search, utils.DefaultIsDelete)
		}

		if len(dataPartners) < 1 {
			dataPartners = []models.Partner{}
		}

		responseGetPartners := struct {
			Code              int              `json:"code"`
			TotalData         int              `json:"totalData"`
			TotalFilteredData int              `json:"totalFilteredData"`
			Partners          []models.Partner `json:"partners"`
		}{200, totalDataPartners, totalFilteredDataPartners, dataPartners}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetPartners)
	}
}

// PostAddPartner untuk menambahkan data partner
func PostAddPartner(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()

		if errTransaction != nil {
			utils.HandleError(errTransaction, "")
		}

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			Fullname string `validate:"min:2,max:100"`
		}{url.QueryEscape(req.FormValue("fullname"))}

		utils.Validate(request)

		uidPartner := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		defaultIsDelete := "0"

		_, errInsertPartner := transaction.Exec(`insert into atn_partners ("UID", fullname, "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5)`, uidPartner, request.Fullname, defaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertPartner, "")

		responseAddPartner := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data partner berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddPartner)
	}
}

// PostEditPartner untuk mengubah data partner
func PostEditPartner(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		defaultIsDelete := "0"

		request := struct {
			UID      string `validate:"required,max:200"`
			Fullname string `validate:"required,max:100"`
		}{req.FormValue("uid"), url.QueryEscape(req.FormValue("fullname"))}

		utils.Validate(request)

		partner := services.GetOnePartner(db, request.UID, "0")

		if partner.UID == "" || len(partner.UID) < 10 {
			panic("Data partner tidak ditemukan")
		}

		_, errUpdatePartner := transaction.Exec(`UPDATE atn_partners SET fullname = $1 WHERE "UID" = $2 AND "isDelete" = $3`, request.Fullname, request.UID, defaultIsDelete)

		utils.HandleError(errUpdatePartner, "")

		responseEditPartner := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data partner berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditPartner)
	}
}

// PostDeletePartner untuk menghapus data partner
func PostDeletePartner(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		defaultIsDelete := "0"
		defaultIsDeleted := "1"

		request := struct {
			UID string `validate:"required,max:200"`
		}{req.FormValue("uid")}

		utils.Validate(request)

		partner := services.GetOnePartner(db, request.UID, "0")

		if partner.UID == "" || len(partner.UID) < 10 {
			panic("Data partner tidak ditemukan")
		}

		_, errDeletePartner := transaction.Exec(`UPDATE atn_partners SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, defaultIsDeleted, request.UID, defaultIsDelete)
		utils.HandleError(errDeletePartner, "")

		responseDeletePartner := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data partner berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeletePartner)
	}
}
