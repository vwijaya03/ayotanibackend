package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"golang.org/x/time/rate"
)

// TransactionHistory used for show transaction from investor
func TransactionHistory(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		userUID := middleware.AuthUser.UID

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			panic("Sort or sort by value is invalid")
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy

		dataTransactions := services.GetTransactions(db, userUID, completeSortBy, limit, offset, utils.DefaultIsDelete)

		if len(dataTransactions) < 1 {
			dataTransactions = []models.Transaction{}
		}

		responseGetTransactions := struct {
			Result []models.Transaction `json:"results"`
		}{dataTransactions}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetTransactions)
	}
}

// BalanceLog used for show balance from investor
func BalanceLog(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		userUID := middleware.AuthUser.UID

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `"atn_balanceLogs"."createdAt"`
			sortBy = `desc`
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy

		dataBalanceLogs := services.GetBalanceLogs(db, userUID, completeSortBy, limit, offset, utils.DefaultIsDelete)

		if len(dataBalanceLogs) < 1 {
			dataBalanceLogs = []models.BalanceLog{}
		}

		responseGetBalanceLogs := struct {
			Code   int                 `json:"code"`
			Result []models.BalanceLog `json:"results"`
		}{200, dataBalanceLogs}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetBalanceLogs)
	}
}

// Topup used for add balance of user
func Topup(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()

		if errTransaction != nil {
			utils.HandleError(errTransaction, "")
		}

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		defaultIsDelete := "0"
		role := "investor"
		amount := utils.ParseFloat64(req.FormValue("amount"))

		request := struct {
			UserUID string  `validate:"required,min:1,max:100"`
			Amount  float64 `validate:"required,gt:10000,lt:1000000000"`
		}{req.FormValue("userUID"), amount}

		utils.Validate(request)

		existUser := services.GetOneUser(db, request.UserUID, role, defaultIsDelete)

		if existUser.UID == "" || len(existUser.UID) < 5 {
			panic("Data user tidak ditemukan")
		}

		userBalance := services.GetUserBalance(db, request.UserUID, defaultIsDelete)

		currentBalance := userBalance.Balance + request.Amount

		_, errUpdateUserBalance := transaction.Exec(`UPDATE "atn_userBalance" SET "balance" = $1 WHERE "userUID" = $2 AND "isDelete" = $3`, currentBalance, request.UserUID, defaultIsDelete)

		utils.HandleError(errUpdateUserBalance, "")

		uidBalanceLog := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		balanceLog := models.BalanceLog{
			UID:               uidBalanceLog,
			FromUserUID:       request.UserUID,
			ToUserUID:         request.UserUID,
			TotalAmount:       amount,
			Description:       "Topup saldo",
			DescriptionSystem: "Topup saldo",
			Type:              "1",
			IsDelete:          defaultIsDelete,
			CreatedAt:         currentDatetime,
			UpdatedAt:         currentDatetime,
		}

		utils.Validate(balanceLog)

		_, errInsertBalanceLog := transaction.Exec(`insert into "atn_balanceLogs" ("UID", "fromUserUID", "toUserUID", "totalAmount", "desc", "descSystem", "type", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`, balanceLog.UID, balanceLog.FromUserUID, balanceLog.ToUserUID, balanceLog.TotalAmount, balanceLog.Description, balanceLog.DescriptionSystem, balanceLog.Type, balanceLog.IsDelete, balanceLog.CreatedAt, balanceLog.UpdatedAt)

		utils.HandleError(errInsertBalanceLog, "")

		responseTopup := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Topup berhasil"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseTopup)
	}
}

// InvestmentPurchase used for buy unit of investation
func InvestmentPurchase(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()

		if errTransaction != nil {
			utils.HandleError(errTransaction, "")
		}

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		if middleware.AuthUser.Role != utils.RoleInvestor {
			panic("Hanya investor yang bisa membeli unit investasi")
		}

		if middleware.AuthUser.IsVerified == "0" {
			panic("Silahkan lengkapi data diri dan dokumen terlebih dahulu, dan ajukan untuk verifikasi akun")
		}

		quantity := utils.ParseInt(req.FormValue("quantity"))

		request := struct {
			ProductUID string `validate:"required,min:1,max:100"`
			Quantity   int    `validate:"required,gt:0,lt:10000"`
		}{req.FormValue("productUID"), quantity}

		utils.Validate(request)

		product := services.GetOneProductForTransaction(db, request.ProductUID, utils.DefaultIsAvailableProduct, utils.DefaultIsDelete)

		if product.UID == "" || len(product.UID) < 5 {
			panic("Data produk tidak ditemukan")
		}

		productCreatedDate, errProductCreatedDate := time.Parse("2006-01-02T15:04:05Z", product.CreatedAt)
		utils.HandleError(errProductCreatedDate, "")
		productFinalDate := productCreatedDate.AddDate(0, product.Period, 0)
		totalDays := int(productFinalDate.Sub(productCreatedDate).Hours() / 24)
		days := int(productFinalDate.Sub(time.Now()).Hours() / 24)

		if days < 1 {
			panic("Produk telah ditutup")
		}

		if days > totalDays {
			panic("Produk belum dibuka")
		}

		totalAmount := float64(quantity) * product.Price
		userBalance := services.GetUserBalance(db, middleware.AuthUser.UID, utils.DefaultIsDelete)

		if product.Stock < 1 {
			panic("Unit investasi telah habis")
		}

		if product.Stock < quantity {
			panic("Pembelian unit investasi melebihi sisa yang tersedia")
		}

		if userBalance.Balance < totalAmount {
			panic("Saldo anda tidak mencukupi")
		}

		currentBalance := userBalance.Balance - totalAmount
		currentStock := product.Stock - quantity

		_, errUpdateProductStock := transaction.Exec(`UPDATE "atn_products" SET "stock" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, currentStock, product.UID, utils.DefaultIsDelete)
		utils.HandleError(errUpdateProductStock, "")

		_, errUpdateUserBalance := transaction.Exec(`UPDATE "atn_userBalance" SET "balance" = $1 WHERE "userUID" = $2 AND "isDelete" = $3`, currentBalance, middleware.AuthUser.UID, utils.DefaultIsDelete)
		utils.HandleError(errUpdateUserBalance, "")

		uidInvestmentPurchaseTrx := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		investmentPurchaseTrx := models.Transaction{UID: uidInvestmentPurchaseTrx, UserUID: middleware.AuthUser.UID, ProductUID: product.UID, Quantity: quantity, Amount: totalAmount, Type: "0", IsDelete: "0", CreatedAt: currentDatetime, UpdatedAt: currentDatetime}

		_, errInsertInvestmentPurchaseTrx := transaction.Exec(`insert into "atn_transactions" ("UID", "userUID", "productUID", "quantity", "amount", "type", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`, investmentPurchaseTrx.UID, investmentPurchaseTrx.UserUID, investmentPurchaseTrx.ProductUID, investmentPurchaseTrx.Quantity, investmentPurchaseTrx.Amount, investmentPurchaseTrx.Type, investmentPurchaseTrx.IsDelete, investmentPurchaseTrx.CreatedAt, investmentPurchaseTrx.UpdatedAt)
		utils.HandleError(errInsertInvestmentPurchaseTrx, "")

		uidBalanceLog := utils.GenerateUID()

		balanceLog := models.BalanceLog{
			UID:               uidBalanceLog,
			FromUserUID:       middleware.AuthUser.UID,
			ToUserUID:         middleware.AuthUser.UID,
			TotalAmount:       totalAmount,
			Description:       fmt.Sprintf(`Pembelian %v unit investasi %s`, investmentPurchaseTrx.Quantity, product.Title),
			DescriptionSystem: fmt.Sprintf(`pembelian %v productUID %s`, investmentPurchaseTrx.Quantity, product.UID),
			Type:              "0",
			IsDelete:          utils.DefaultIsDelete,
			CreatedAt:         currentDatetime,
			UpdatedAt:         currentDatetime,
		}

		utils.Validate(balanceLog)

		_, errInsertBalanceLog := transaction.Exec(`insert into "atn_balanceLogs" ("UID", "fromUserUID", "toUserUID", "totalAmount", "desc", "descSystem", "type", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`, balanceLog.UID, balanceLog.FromUserUID, balanceLog.ToUserUID, balanceLog.TotalAmount, balanceLog.Description, balanceLog.DescriptionSystem, balanceLog.Type, balanceLog.IsDelete, balanceLog.CreatedAt, balanceLog.UpdatedAt)

		utils.HandleError(errInsertBalanceLog, "")

		fullname := middleware.AuthUser.Fullname
		from := utils.DefaultFromForEmail
		to := middleware.AuthUser.Email
		subject := balanceLog.Description
		currentDatetimeEmail, _ := time.Parse("2006-01-02 15:04:05", currentDatetime)
		currentDatetimeEmailStr := currentDatetimeEmail.Format("02 January 2006 15:04:05")
		transactionAction := balanceLog.Description
		transactionType := "Pembelian"
		transactionStatus := "Transaksi Berhasil"
		transactionReferralCode := investmentPurchaseTrx.UID
		amountForEmail := utils.FormatRupiah(int(investmentPurchaseTrx.Amount), "Rp.")

		go func() {
			utils.SendEmailTransaction(fullname, from, to, subject, currentDatetimeEmailStr, transactionType, amountForEmail, transactionReferralCode, transactionStatus, transactionAction)
		}()

		responsePurchaseInvestation := struct {
			Code           int    `json:"code"`
			Message        string `json:"message"`
			AvailableStock int    `json:"availableStock"`
		}{200, "Pembelian unit investasi berhasil", currentStock}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responsePurchaseInvestation)
	}
}
