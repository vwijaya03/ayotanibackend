package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"golang.org/x/time/rate"
)

// GetWithdraw used for show data from investor that request withdraw balance
func GetWithdraw(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `"atn_withdrawals"."createdAt"`
			sortBy = `desc`
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataWithdraw := 0

		dataWithdraws := services.GetWithdraws(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataWithdraws := services.GetTotalDataWithdraw(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataWithdraw = services.GetTotalDataFilteredWithdraw(db, search, utils.DefaultIsDelete)
		}

		if len(dataWithdraws) < 1 {
			dataWithdraws = []models.Withdraw{}
		}

		responseGetWithdraws := struct {
			Code              int               `json:"code"`
			TotalData         int               `json:"totalData"`
			TotalFilteredData int               `json:"totalFilteredData"`
			Withdrawals       []models.Withdraw `json:"withdrawals"`
		}{200, totalDataWithdraws, totalFilteredDataWithdraw, dataWithdraws}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetWithdraws)
	}
}

// PostWithdraw untuk investor request tarik saldo
func PostWithdraw(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		amount := utils.ParseFloat64(req.FormValue("amount"))

		request := struct {
			Amount float64 `validate:"required, lt:3000000000"`
		}{amount}

		utils.Validate(request)

		if middleware.AuthUser.IsVerified == "0" {
			panic("Silahkan lengkapi data diri dan dokumen terlebih dahulu, dan ajukan untuk verifikasi akun")
		}

		UID := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		currentUserUID := middleware.AuthUser.UID

		currentBalance := services.GetUserBalance(db, currentUserUID, utils.DefaultIsDelete)

		if request.Amount > currentBalance.Balance {
			panic("Jumlah penarikan melebihi saldo anda")
		}

		_, errInsertWithdraw := transaction.Exec(`insert into "atn_withdrawals" ("UID", "userUID", "amount", "status", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7)`, UID, currentUserUID, request.Amount, "0", utils.DefaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertWithdraw, "")

		responseWithdraw := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Penarikan saldo sedang diproses"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseWithdraw)
	}
}

// PostActionWithdraw untuk admin melakukan action approve atau reject penarikan saldo investor
func PostActionWithdraw(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			UID    string `validate:"required,max:200"`
			Action string `validate:"required,max:10"`
		}{req.FormValue("uid"), req.FormValue("action")}

		utils.Validate(request)

		status := "1"
		message := "Penarikan saldo investor telah diapprove"
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		if request.Action == "2" {
			status = "2"
			message = "Penarikan saldo investor telah direject"
		}

		existWithdrawal := services.GetOneWithdraw(db, request.UID, "0")

		if existWithdrawal.UID == "" || len(existWithdrawal.UID) < 5 {
			panic("Data penarikan saldo tidak ditemukan")
		}

		if status == "1" {
			userBalance := services.GetUserBalance(db, existWithdrawal.UserUID, utils.DefaultIsDelete)

			if userBalance.Balance < existWithdrawal.Amount {
				panic("Jumlah penarikan saldo melebihi saldo tersisa")
			}

			currentBalance := userBalance.Balance - existWithdrawal.Amount

			_, errUpdateUserBalance := transaction.Exec(`UPDATE "atn_userBalance" SET "balance" = $1 WHERE "userUID" = $2 AND "isDelete" = $3`, currentBalance, existWithdrawal.UserUID, utils.DefaultIsDelete)
			utils.HandleError(errUpdateUserBalance, "")

			uidBalanceLog := utils.GenerateUID()

			balanceLog := models.BalanceLog{
				UID:               uidBalanceLog,
				FromUserUID:       existWithdrawal.UserUID,
				ToUserUID:         existWithdrawal.UserUID,
				TotalAmount:       existWithdrawal.Amount,
				Description:       "Penarikan saldo",
				DescriptionSystem: "Penarikan saldo",
				Type:              "3",
				IsDelete:          utils.DefaultIsDelete,
				CreatedAt:         currentDatetime,
				UpdatedAt:         currentDatetime,
			}

			utils.Validate(balanceLog)

			_, errInsertBalanceLog := transaction.Exec(`insert into "atn_balanceLogs" ("UID", "fromUserUID", "toUserUID", "totalAmount", "desc", "descSystem", "type", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`, balanceLog.UID, balanceLog.FromUserUID, balanceLog.ToUserUID, balanceLog.TotalAmount, balanceLog.Description, balanceLog.DescriptionSystem, balanceLog.Type, balanceLog.IsDelete, balanceLog.CreatedAt, balanceLog.UpdatedAt)
			utils.HandleError(errInsertBalanceLog, "")
		}

		_, errUpdateWithdraw := transaction.Exec(`UPDATE "atn_withdrawals" SET "status" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, status, request.UID, utils.DefaultIsDelete)
		utils.HandleError(errUpdateWithdraw, "")

		// Send Notification
		if status == "1" {
			user := services.GetOneUser(db, existWithdrawal.UserUID, utils.RoleInvestor, utils.DefaultIsDelete)
			title := "Penarikan Saldo Berhasil Diproses"
			body := fmt.Sprintf("Penarikan Saldo sejumlah %s telah diproses, silahkan cek rekening bank anda yang terdaftar pada aplikasi AyoTani", utils.FormatRupiah(int(existWithdrawal.Amount), "Rp."))
			fullname := user.Fullname
			from := utils.DefaultFromForEmail
			to := user.Email
			subject := title
			currentDatetimeEmail, _ := time.Parse("2006-01-02 15:04:05", currentDatetime)
			currentDatetimeEmailStr := currentDatetimeEmail.Format("02 January 2006 15:04:05")
			transactionAction := "penarikan saldo"
			transactionType := "Penarikan"
			transactionStatus := "Transaksi Berhasil"
			transactionReferralCode := existWithdrawal.UID
			amountForEmail := utils.FormatRupiah(int(existWithdrawal.Amount), "Rp.")

			go func() { utils.SendNotification(title, body, "", user.DeviceToken, "") }()
			go func() {
				utils.SendEmailTransaction(fullname, from, to, subject, currentDatetimeEmailStr, transactionType, amountForEmail, transactionReferralCode, transactionStatus, transactionAction)
			}()
		}

		responsePostActionWithdraw := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, message}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responsePostActionWithdraw)
	}
}
