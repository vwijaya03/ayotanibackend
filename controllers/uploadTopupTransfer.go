package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"path/filepath"
	"time"

	"golang.org/x/time/rate"
)

// GetUploadTopupTransfer used for show data from investor that submit their topup transfer
func GetUploadTopupTransfer(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 15
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = `"atn_uploadTopupTransfers"."createdAt"`
			sortBy = `desc`
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataUploadTopupTransfers := 0

		dataUploadTopupTransfers := services.GetUploadTopupTransfers(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataUploadTopupTransfers := services.GetTotalDataUploadTopupTransfer(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataUploadTopupTransfers = services.GetTotalDataFilteredUploadTopupTransfer(db, search, utils.DefaultIsDelete)
		}

		if len(dataUploadTopupTransfers) < 1 {
			dataUploadTopupTransfers = []models.UploadTopupTransfer{}
		}

		responseGetUploadTopupTransfers := struct {
			Code                 int                          `json:"code"`
			TotalData            int                          `json:"totalData"`
			TotalFilteredData    int                          `json:"totalFilteredData"`
			UploadTopupTransfers []models.UploadTopupTransfer `json:"uploadTopupTransfers"`
		}{200, totalDataUploadTopupTransfers, totalFilteredDataUploadTopupTransfers, dataUploadTopupTransfers}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetUploadTopupTransfers)
	}
}

// PostUploadTopupTransfer untuk mengupload bukti topup transfer investor
func PostUploadTopupTransfer(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		imgFile, handlerImgFile, _ := req.FormFile("img")
		imgFileType := utils.GetFileContentTypeViaFormFile(imgFile)
		amount := utils.ParseFloat64(req.FormValue("amount"))

		var (
			targetImgPath, imgFileExtension string = "", ""
			imgFileSize                     int    = 0
		)

		request := struct {
			ImgFileType string  `validate:"required, imageType"`
			Amount      float64 `validate:"required, lt:3000000000"`
		}{imgFileType, amount}

		utils.Validate(request)

		if middleware.AuthUser.IsVerified == "0" {
			panic("Silahkan lengkapi data diri dan dokumen terlebih dahulu, dan ajukan untuk verifikasi akun")
		}

		UID := utils.GenerateUID()
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")
		currentUserUID := middleware.AuthUser.UID
		imgFileExtension = filepath.Ext(handlerImgFile.Filename)
		imgFileSize = int(handlerImgFile.Size)

		if imgFileSize > 2000000 {
			panic("Ukuran gambar terlalu besar")
		}

		filenameImg := middleware.AuthUser.Fullname + utils.GenerateUID() + imgFileExtension
		targetImgPath = utils.UploadImage(imgFile, handlerImgFile, utils.UploadTopupTransferImgPath, filenameImg, utils.UploadTopupTransferImgAliasPath)

		_, errInsertUploadTopupTransfer := transaction.Exec(`insert into "atn_uploadTopupTransfers" ("UID", "userUID", "amount", "img", "status", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`, UID, currentUserUID, request.Amount, targetImgPath, "0", utils.DefaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertUploadTopupTransfer, "")

		responseUploadTopupTransfer := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Bukti transfer berhasil dikirim, dan sedang dalam proses pemeriksaan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseUploadTopupTransfer)
	}
}

// PostActionUploadTopupTransfer untuk admin melakukan action approve atau reject bukti upload topup transfer investor
func PostActionUploadTopupTransfer(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		request := struct {
			UID    string `validate:"required,max:200"`
			Action string `validate:"required,max:10"`
		}{req.FormValue("uid"), req.FormValue("action")}

		utils.Validate(request)

		status := "1"
		message := "Bukti transfer topup investor telah diapprove"
		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		if request.Action == "2" {
			status = "2"
			message = "Bukti transfer topup investor telah direject"
		}

		existUploadTopupTransfer := services.GetOneUploadTopupTransfers(db, request.UID, "0")

		if existUploadTopupTransfer.UID == "" || len(existUploadTopupTransfer.UID) < 5 {
			panic("Data topup transfer tidak ditemukan")
		}

		if status == "1" {
			userBalance := services.GetUserBalance(db, existUploadTopupTransfer.UserUID, utils.DefaultIsDelete)
			currentBalance := userBalance.Balance + existUploadTopupTransfer.Amount

			_, errUpdateUserBalance := transaction.Exec(`UPDATE "atn_userBalance" SET "balance" = $1 WHERE "userUID" = $2 AND "isDelete" = $3`, currentBalance, existUploadTopupTransfer.UserUID, utils.DefaultIsDelete)
			utils.HandleError(errUpdateUserBalance, "")

			uidBalanceLog := utils.GenerateUID()

			balanceLog := models.BalanceLog{
				UID:               uidBalanceLog,
				FromUserUID:       existUploadTopupTransfer.UserUID,
				ToUserUID:         existUploadTopupTransfer.UserUID,
				TotalAmount:       existUploadTopupTransfer.Amount,
				Description:       "Topup saldo via bank transfer",
				DescriptionSystem: "Topup saldo via bank transfer",
				Type:              "2",
				IsDelete:          utils.DefaultIsDelete,
				CreatedAt:         currentDatetime,
				UpdatedAt:         currentDatetime,
			}

			utils.Validate(balanceLog)

			_, errInsertBalanceLog := transaction.Exec(`insert into "atn_balanceLogs" ("UID", "fromUserUID", "toUserUID", "totalAmount", "desc", "descSystem", "type", "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`, balanceLog.UID, balanceLog.FromUserUID, balanceLog.ToUserUID, balanceLog.TotalAmount, balanceLog.Description, balanceLog.DescriptionSystem, balanceLog.Type, balanceLog.IsDelete, balanceLog.CreatedAt, balanceLog.UpdatedAt)
			utils.HandleError(errInsertBalanceLog, "")
		}

		_, errUpdateUploadTopupTransfer := transaction.Exec(`UPDATE "atn_uploadTopupTransfers" SET "status" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, status, request.UID, utils.DefaultIsDelete)
		utils.HandleError(errUpdateUploadTopupTransfer, "")

		// Send Notification
		if status == "1" {
			user := services.GetOneUser(db, existUploadTopupTransfer.UserUID, utils.RoleInvestor, utils.DefaultIsDelete)
			title := "Bukti Transfer Berhasil Diproses"
			body := "Silahkan cek saldo anda kembali pada aplikasi AyoTani"
			fullname := user.Fullname
			from := utils.DefaultFromForEmail
			to := user.Email
			subject := title
			currentDatetimeEmail, _ := time.Parse("2006-01-02 15:04:05", currentDatetime)
			currentDatetimeEmailStr := currentDatetimeEmail.Format("02 January 2006 15:04:05")
			transactionAction := "topup transfer saldo"
			transactionType := "Topup"
			transactionStatus := "Transaksi Berhasil"
			transactionReferralCode := existUploadTopupTransfer.UID
			amountForEmail := utils.FormatRupiah(int(existUploadTopupTransfer.Amount), "Rp.")

			go func() { utils.SendNotification(title, body, "", user.DeviceToken, "") }()
			go func() {
				utils.SendEmailTransaction(fullname, from, to, subject, currentDatetimeEmailStr, transactionType, amountForEmail, transactionReferralCode, transactionStatus, transactionAction)
			}()
		}

		responsePostActionUploadTopupTransfer := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, message}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responsePostActionUploadTopupTransfer)
	}
}
