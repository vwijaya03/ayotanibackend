package controllers

import (
	"ayotani/middleware"
	"ayotani/models"
	"ayotani/services"
	"ayotani/utils"
	"database/sql"
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"golang.org/x/time/rate"
)

// GetBank show all data bank
func GetBank(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer utils.CatchErrJSON(w)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		itemsPerPage := utils.ParseInt(req.FormValue("itemsPerPage"))
		startPage := utils.ParseInt(req.FormValue("startPage"))
		sort := req.FormValue("sort")
		sortBy := req.FormValue("sortBy")
		search := req.FormValue("search")

		if itemsPerPage == 0 {
			itemsPerPage = 50
		}

		if startPage < 1 {
			startPage = 1
		}

		if sort == "" || sortBy == "" {
			sort = "name"
			sortBy = "asc"
		}

		limit := itemsPerPage
		offset := (startPage - 1) * itemsPerPage
		completeSortBy := sort + " " + sortBy
		totalFilteredDataBanks := 0

		dataBanks := services.GetBanks(db, search, completeSortBy, limit, offset, utils.DefaultIsDelete)
		totalDataBanks := services.GetTotalDataBank(db, utils.DefaultIsDelete)

		if search != "" {
			totalFilteredDataBanks = services.GetTotalDataFilteredBank(db, search, utils.DefaultIsDelete)
		}

		if len(dataBanks) < 1 {
			dataBanks = []models.Bank{}
		}

		responseGetBanks := struct {
			Code              int           `json:"code"`
			TotalData         int           `json:"totalData"`
			TotalFilteredData int           `json:"totalFilteredData"`
			Banks             []models.Bank `json:"banks"`
		}{200, totalDataBanks, totalFilteredDataBanks, dataBanks}

		// responseGetBank := models.ResGetData{
		// 	Result:  dataBanks,
		// 	Code:    200,
		// 	Message: "OK",
		// }

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseGetBanks)
	}
}

// PostAddBank untuk menambahkan data bank
func PostAddBank(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()

		if errTransaction != nil {
			utils.HandleError(errTransaction, "")
		}

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		uidBank := utils.GenerateUID()

		currentDatetime := time.Now().Format("2006-01-02 15:04:05")

		defaultIsDelete := "0"

		request := struct {
			Name string `validate:"min:2,max:40"`
		}{url.QueryEscape(req.FormValue("name"))}

		utils.Validate(request)

		_, errInsertBank := transaction.Exec(`insert into atn_banks ("UID", name, "isDelete", "createdAt", "updatedAt") VALUES ($1, $2, $3, $4, $5)`, uidBank, request.Name, defaultIsDelete, currentDatetime, currentDatetime)

		utils.HandleError(errInsertBank, "")

		responseAddBank := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data bank berhasil ditambahkan"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseAddBank)
	}
}

// PostEditBank untuk mengubah data bank
func PostEditBank(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			validUID sql.NullString
		)

		defaultIsDelete := "0"

		request := struct {
			UID  string `validate:"required,max:200"`
			Name string `validate:"required,max:40"`
		}{req.FormValue("uid"), url.QueryEscape(req.FormValue("name"))}

		utils.Validate(request)

		errBankNotFound := transaction.QueryRow(`SELECT atn_banks."UID" FROM atn_banks WHERE atn_banks."UID" = $1 AND atn_banks."isDelete" = $2`, request.UID, defaultIsDelete).Scan(&validUID)

		utils.HandleError(errBankNotFound, "")

		if validUID.String == "" || len(validUID.String) < 5 {
			panic("Data bank tidak ditemukan")
		}

		_, errUpdateBank := transaction.Exec(`UPDATE atn_banks SET name = $1 WHERE "UID" = $2 AND "isDelete" = $3`, request.Name, request.UID, defaultIsDelete)

		utils.HandleError(errUpdateBank, "")

		responseEditBank := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data bank berhasil diubah"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseEditBank)
	}
}

// PostDeleteBank untuk menghapus data bank
func PostDeleteBank(db *sql.DB, routeMethod string, acl []string, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		transaction, errTransaction := db.Begin()
		utils.HandleError(errTransaction, "")

		defer transaction.Commit()
		defer utils.CatchErrJSONWithDbTransaction(w, transaction)

		utils.SetRouteMethod(routeMethod, req, limiter)
		middleware.Auth(db, acl, req)

		var (
			validUID string
		)

		defaultIsDelete := "0"
		defaultIsDeleted := "1"

		request := struct {
			UID string `validate:"required,max:200"`
		}{req.FormValue("uid")}

		utils.Validate(request)

		errBankNotFound := transaction.QueryRow(`SELECT atn_banks."UID" FROM atn_banks WHERE atn_banks."UID" = $1 AND atn_banks."isDelete" = $2`, request.UID, defaultIsDelete).Scan(&validUID)

		utils.HandleError(errBankNotFound, "")

		if validUID == "" || len(validUID) < 5 {
			panic("Data bank tidak ditemukan")
		}

		_, errDeleteBank := transaction.Exec(`UPDATE atn_banks SET "isDelete" = $1 WHERE "UID" = $2 AND "isDelete" = $3`, defaultIsDeleted, request.UID, defaultIsDelete)
		utils.HandleError(errDeleteBank, "")

		responseDeleteBank := struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		}{200, "Data bank berhasil dihapus"}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responseDeleteBank)
	}
}
