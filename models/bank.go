package models

// Bank for model
type Bank struct {
	UID       string `json:"uid" validate:"required,min:1,max:100"`
	Name      string `json:"name" validate:"required,min:3,max:50"`
	IsDelete  string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt string `json:"-" validate:"required,min:6,max:30"`
	UpdatedAt string `json:"-" validate:"required,min:6,max:30"`
}
