package models

// Investor for model
type Investor struct {
	UID               string `json:"uid" validate:"required,min:1,max:100"`
	Email             string `json:"email" validate:"required,email,min:4,max:40"`
	Password          string `json:"-" validate:"required,min:6,max:40"`
	Fullname          string `json:"fullname" validate:"required,min:2,max:40"`
	Phone             string `json:"phone" validate:"max:20"`
	Gender            string `json:"gender" validate:"max:20"`
	Pob               string `json:"pob" validate:"max:30"`
	Dob               string `json:"dob" validate:"max:30"`
	Address           string `json:"address" validate:"max:50"`
	City              string `json:"city" validate:"max:30"`
	Province          string `json:"province" validate:"max:30"`
	PostalCode        string `json:"postalCode" validate:"max:30"`
	Nationallity      string `json:"nationallity" validate:"max:30"`
	Religion          string `json:"religion" validate:"max:30"`
	MaritalStatus     string `json:"maritalStatus" validate:"max:30"`
	Role              string `json:"role" validate:"required,min:2,max:30"`
	Salt              string `json:"-" validate:"required,min:10,max:100"`
	DeviceToken       string `json:"-" validate:"required,min:10,max:200"`
	AccessToken       string `json:"-" validate:"required,min:10,max:100"`
	AccessTokenExpiry string `json:"-" validate:"required,min:6,max:30"`
	IsVerified        string `json:"isVerified" validate:"required,min:1,max:30"`
	IsDelete          string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt         string `json:"-" validate:"max:30"`
	UpdatedAt         string `json:"-" validate:"max:30"`
}
