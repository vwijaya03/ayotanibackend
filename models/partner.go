package models

// Partner for model
type Partner struct {
	UID       string `json:"uid" validate:"required,min:1,max:100"`
	Fullname  string `json:"fullname" validate:"required,min:3,max:50"`
	IsDelete  string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt string `json:"-" validate:"required,min:6,max:30"`
	UpdatedAt string `json:"-" validate:"required,min:6,max:30"`
}
