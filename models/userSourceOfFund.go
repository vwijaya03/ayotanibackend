package models

// UserSourceOfFunds for model
type UserSourceOfFunds struct {
	UID             string `json:"uid" validate:"required,min:1,max:100"`
	UserUID         string `json:"-" validate:"required,min:1,max:100"`
	JobUID          string `json:"jobUID" validate:"max:100"`
	SourceOfFundUID string `json:"sourceOfFundUID" validate:"max:100"`
	IsDelete        string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt       string `json:"-" validate:"required,min:6,max:30"`
	UpdatedAt       string `json:"-" validate:"required,min:6,max:30"`
}
