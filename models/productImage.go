package models

// ProductImage for model
type ProductImage struct {
	UID        string `json:"uid" validate:"required,min:1,max:100"`
	ProductUID string `json:"productUID" validate:"required,min:1,max:100"`
	Img        string `json:"img" validate:"required,min:1,max:200"`
	IsDelete   string `json:"isDelete" validate:"required,min:1,max:30"`
	CreatedAt  string `json:"createdAt" validate:"required,min:6,max:30"`
	UpdatedAt  string `json:"updatedAt" validate:"required,min:6,max:30"`
}
