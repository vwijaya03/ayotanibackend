package models

// UniqueCode for model
type UniqueCode struct {
	UID       string `json:"uid"`
	UserUID   string `json:"userUID"`
	Fullname  string `json:"fullname"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Code      string `json:"code"`
	IsDelete  string `json:"-"`
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"-"`
}
