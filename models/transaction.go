package models

// Transaction for model
type Transaction struct {
	UID          string  `json:"uid" validate:"required,min:1,max:100"`
	UserUID      string  `json:"userUID" validate:"required,min:1,max:100"`
	ProductUID   string  `json:"productUID" validate:"required,min:1,max:100"`
	ProductTitle string  `json:"productTitle" validate:"required,min:1,max:200"`
	Quantity     int     `json:"quantity" validate:"required,gt:0,lt:10000"`
	Amount       float64 `json:"amount" validate:"required,gt:1,lt:1000000000"`
	Type         string  `json:"type" validate:"required,min:1,max:10"`
	IsDelete     string  `json:"-" validate:"required,min:1,max:30"`
	CreatedAt    string  `json:"createdAt" validate:"required,min:6,max:30"`
	UpdatedAt    string  `json:"updatedAt" validate:"required,min:6,max:30"`
}
