package models

// ReferenceImage for model
type ReferenceImage struct {
	UID          string `json:"uid" validate:"required,min:1,max:100"`
	ReferenceUID string `json:"referenceUID" validate:"required,min:1,max:100"`
	Img          string `json:"img" validate:"required,min:1,max:200"`
	IsDelete     string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt    string `json:"-" validate:"required,min:6,max:30"`
	UpdatedAt    string `json:"-" validate:"required,min:6,max:30"`
}
