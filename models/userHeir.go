package models

// UserHeir for model
type UserHeir struct {
	UID       string `json:"uid" validate:"required,min:1,max:100"`
	UserUID   string `json:"-" validate:"required,min:1,max:100"`
	Fullname  string `json:"fullname" validate:"max:40"`
	Phone     string `json:"phone" validate:"max:20"`
	Address   string `json:"address" validate:"max:50"`
	IsDelete  string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt string `json:"-" validate:"required,min:6,max:30"`
	UpdatedAt string `json:"-" validate:"required,min:6,max:30"`
}
