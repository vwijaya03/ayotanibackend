package models

// Religion for model
type Religion struct {
	Name  string `json:"name" validate:"required,min:3,max:50"`
	Value string `json:"value" validate:"required,min:3,max:50"`
}
