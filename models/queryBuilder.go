package models

// QueryBuilder used for contain value of select query builder
type QueryBuilder struct {
	Key   string
	Value interface{}
}
