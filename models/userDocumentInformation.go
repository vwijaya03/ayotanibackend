package models

// UserDocumentInformation for model
type UserDocumentInformation struct {
	UID       string `json:"uid" validate:"required,min:1,max:100"`
	UserUID   string `json:"-" validate:"required,min:1,max:100"`
	Ktp       string `json:"ktp" validate:"max:40"`
	Npwp      string `json:"npwp" validate:"max:40"`
	KtpImg    string `json:"ktpImg" validate:"max:200"`
	NpwpImg   string `json:"npwpImg" validate:"max:200"`
	IsDelete  string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt string `json:"-" validate:"required,min:6,max:30"`
	UpdatedAt string `json:"-" validate:"required,min:6,max:30"`
}
