package models

// ProductActivity for model
type ProductActivity struct {
	UID         string `json:"uid"`
	ProductUID  string `json:"productUID"`
	Description string `json:"description"`
	Thumbnail   string `json:"thumbnail"`
	IsDelete    string `json:"-"`
	CreatedAt   string `json:"createdAt"`
	UpdatedAt   string `json:"-"`
}
