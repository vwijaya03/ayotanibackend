package models

// ResGetData for response with dynamic result model using interface
type ResGetData struct {
	Result  interface{} `json:"result"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
}

// ResGetInvestor for response show all investors
type ResGetInvestor struct {
	Result  []Investor `json:"result"`
	Code    int        `json:"code"`
	Message string     `json:"message"`
}

// ResGetSuperadmin for response show all superadmin
type ResGetSuperadmin struct {
	Result  []Superadmin `json:"result"`
	Code    int          `json:"code"`
	Message string       `json:"message"`
}

// ResPostAddUser for response after add user
type ResPostAddUser struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// ErrResGeneralJSON struct untuk response error yang return nya json
type ErrResGeneralJSON struct {
	Code          int    `json:"code"`
	Message       string `json:"message"`
	DetailMessage string `json:"detailMessage"`
}
