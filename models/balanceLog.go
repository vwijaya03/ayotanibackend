package models

// BalanceLog for model
type BalanceLog struct {
	UID               string  `json:"uid" validate:"required,min:1,max:100"`
	FromUserUID       string  `json:"-" validate:"required,min:1,max:100"`
	ToUserUID         string  `json:"-" validate:"required,min:1,max:100"`
	TotalAmount       float64 `json:"totalAmount" validate:"required"`
	Description       string  `json:"description" validate:"required,min:1,max:1000"`
	DescriptionSystem string  `json:"-" validate:"required,min:1,max:1000"`
	Type              string  `json:"type" validate:"required,min:1,max:10"`
	IsDelete          string  `json:"-" validate:"required,min:1,max:30"`
	CreatedAt         string  `json:"createdAt" validate:"required,min:6,max:30"`
	UpdatedAt         string  `json:"-" validate:"required,min:6,max:30"`
}
