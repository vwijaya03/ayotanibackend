package models

// UploadTopupTransfer for model
type UploadTopupTransfer struct {
	UID       string  `json:"uid"`
	UserUID   string  `json:"userUID"`
	Fullname  string  `json:"fullname"`
	Email     string  `json:"email"`
	Phone     string  `json:"phone"`
	Amount    float64 `json:"amount"`
	Img       string  `json:"img"`
	Status    string  `json:"status"`
	IsDelete  string  `json:"-"`
	CreatedAt string  `json:"createdAt"`
	UpdatedAt string  `json:"-"`
}
