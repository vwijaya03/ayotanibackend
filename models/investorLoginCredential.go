package models

// Credential for model
type Credential struct {
	AccessToken       string `json:"accessToken"`
	AccessTokenExpiry string `json:"accessTokenExpiry"`
}
