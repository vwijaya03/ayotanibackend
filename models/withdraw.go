package models

// Withdraw for model
type Withdraw struct {
	UID       string  `json:"uid"`
	UserUID   string  `json:"userUID"`
	Fullname  string  `json:"fullname"`
	Email     string  `json:"email"`
	Phone     string  `json:"phone"`
	Amount    float64 `json:"amount"`
	Status    string  `json:"status"`
	IsDelete  string  `json:"-"`
	CreatedAt string  `json:"createdAt"`
	UpdatedAt string  `json:"-"`
}
