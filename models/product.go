package models

// Product for model
type Product struct {
	UID        string  `json:"uid" validate:"required,min:1,max:100"`
	PartnerUID string  `json:"partnerUID" validate:"required,min:1,max:100"`
	Title      string  `json:"title" validate:"required,min:1,max:200"`
	Desc       string  `json:"desc" validate:"required,min:1,max:100000"`
	Stock      int     `json:"stock" validate:"required,gt:0,lt:100000"`
	TotalStock int     `json:"totalStock" validate:"required,gt:0,lt:100000"`
	Price      float64 `json:"price" validate:"required,gt:0,lt:1000000000000"`
	Roi        float64 `json:"roi" validate:"required,gt:0,lt:100"`
	Period     int     `json:"period" validate:"required,gt:0,lt:100000"`
	Lat        float64 `json:"lat" validate:"required,gt:-1000000,lt:1000000"`
	Lng        float64 `json:"lng" validate:"required,gt:-1000000,lt:1000000"`
	Thumbnail  string  `json:"thumbnail" validate:"required,min:1,max:100"`
	Available  string  `json:"available" validate:"required,min:1,max:100"`
	IsDelete   string  `json:"-" validate:"required,min:1,max:30"`
	CreatedAt  string  `json:"createdAt" validate:"required,min:6,max:30"`
	UpdatedAt  string  `json:"-" validate:"required,min:6,max:30"`
}
