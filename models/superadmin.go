package models

// Superadmin for model
type Superadmin struct {
	UID               string `json:"uid" validate:"required,min:1,max:100"`
	Email             string `json:"email" validate:"required,email,min:4,max:40"`
	Password          string `json:"-" validate:"required,min:6,max:40"`
	Fullname          string `json:"fullname" validate:"required,min:2,max:40"`
	Phone             string `json:"phone" validate:"max:20"`
	Gender            string `json:"gender" validate:"max:20"`
	Role              string `json:"role" validate:"required,min:2,max:30"`
	Salt              string `json:"-" validate:"required,min:10,max:100"`
	AccessToken       string `json:"-" validate:"required,min:10,max:100"`
	AccessTokenExpiry string `json:"-" validate:"required,min:6,max:30"`
	IsDelete          string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt         string `json:"-" validate:"max:30"`
	UpdatedAt         string `json:"-" validate:"max:30"`
}
