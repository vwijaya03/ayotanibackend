package models

// UserBankAccountInformation for model
type UserBankAccountInformation struct {
	UID           string `json:"uid" validate:"required,min:1,max:100"`
	UserUID       string `json:"-" validate:"required,min:1,max:100"`
	BankUID       string `json:"bankUID" validate:"max:100"`
	Branch        string `json:"branch" validate:"max:40"`
	AccountNumber string `json:"accountNumber" validate:"max:40"`
	AccountName   string `json:"accountName" validate:"max:40"`
	IsDelete      string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt     string `json:"-" validate:"required,min:6,max:30"`
	UpdatedAt     string `json:"-" validate:"required,min:6,max:30"`
}
