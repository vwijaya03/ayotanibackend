package models

// Reference for model
type Reference struct {
	UID       string `json:"uid" validate:"required,min:1,max:100"`
	UserUID   string `json:"userUID" validate:"required,min:1,max:100"`
	Title     string `json:"title" validate:"required,min:5,max:200"`
	Desc      string `json:"desc" validate:"required,min:1,max:100000"`
	Thumbnail string `json:"thumbnail" validate:"required,min:1,max:200"`
	IsDelete  string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt string `json:"createdAt" validate:"required,min:6,max:30"`
	UpdatedAt string `json:"-" validate:"required,min:6,max:30"`
}
