package models

// UserBalance for model
type UserBalance struct {
	UID       string  `json:"uid" validate:"required,min:1,max:100"`
	UserUID   string  `json:"userUID" validate:"required,min:1,max:100"`
	Balance   float64 `json:"balance" validate:"gt:0,lt:5000000000"`
	IsDelete  string  `json:"isDelete" validate:"required,min:1,max:30"`
	CreatedAt string  `json:"createdAt" validate:"required,min:6,max:30"`
	UpdatedAt string  `json:"updatedAt" validate:"required,min:6,max:30"`
}
