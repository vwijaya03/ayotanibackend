package models

// RequestVerificationAccount for model
type RequestVerificationAccount struct {
	UID       string `json:"uid" validate:"required,min:1,max:200"`
	UserUID   string `json:"userUID" validate:"required,min:1,max:200"`
	IsDelete  string `json:"-" validate:"required,min:1,max:30"`
	CreatedAt string `json:"-" validate:"required,min:6,max:30"`
	UpdatedAt string `json:"-" validate:"required,min:6,max:30"`
}

// RequestVerificationAccountWithUserData for model
type RequestVerificationAccountWithUserData struct {
	UID       string `json:"uid" validate:"required,min:1,max:200"`
	UserUID   string `json:"userUID" validate:"required,min:1,max:200"`
	Fullname  string `json:"fullname" validate:"required,min:1,max:200"`
	Email     string `json:"email" validate:"required,min:1,max:200"`
	Phone     string `json:"phone" validate:"required,min:1,max:200"`
	Ktp       string `json:"ktp" validate:"max:40"`
	Npwp      string `json:"npwp" validate:"max:40"`
	KtpImg    string `json:"ktpImg" validate:"max:200"`
	NpwpImg   string `json:"npwpImg" validate:"max:200"`
	Status    string `json:"status" validate:"max:10"`
	CreatedAt string `json:"createdAt" validate:"required,min:6,max:30"`
}
