package utils

import (
	"ayotani/models"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/nfnt/resize"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/time/rate"
)

// UploadTopupTransferImgPath used for get default path for Upload Topup Transfer Image
var UploadTopupTransferImgPath = GetRootDirectory() + "/assets/img/topup-transfer/"

// UploadTopupTransferImgAliasPath used for alias path for Upload Topup Transfer Image
var UploadTopupTransferImgAliasPath = GetRootDirectory() + "/topup-transfer/"

// KtpImgPath used for get default path for KTP Image
var KtpImgPath = GetRootDirectory() + "/assets/img/ktp/"

// KtpAliasPath used for get KTP Image from url
var KtpAliasPath = "/ktp/"

// NpwpImgPath used for get default path for NPWP Image
var NpwpImgPath = GetRootDirectory() + "/assets/img/npwp/"

// NpwpAliasPath used for get NPWP Image from url
var NpwpAliasPath = "/npwp/"

// ThumbnailImgPath used for get default path for Thumbnail Image
var ThumbnailImgPath = GetRootDirectory() + "/assets/img/reference-thumbnail/"

// ThumbnailAliasPath used for get Thumbnail Image from url
var ThumbnailAliasPath = "/thumbnail/"

// ReferenceImgPath used for get default path for Reference Image
var ReferenceImgPath = GetRootDirectory() + "/assets/img/reference-image/"

// ReferenceImgAliasPath used for get Reference Image from url
var ReferenceImgAliasPath = "/reference-image/"

// ProductThumbnailImgPath used for get default path for Product Thumbnail Image
var ProductThumbnailImgPath = GetRootDirectory() + "/assets/img/product-thumbnail-image/"

// ProductThumbnailImgAliasPath used for get Product Thumbnail Image from url
var ProductThumbnailImgAliasPath = "/product-thumbnail-image/"

// ProductActivityThumbnailImgPath used for get default path for Product Activity Thumbnail Image
var ProductActivityThumbnailImgPath = GetRootDirectory() + "/assets/img/product-activity-thumbnail-image/"

// ProductActivityThumbnailImgAliasPath used for get Product Activity Thumbnail Image from url
var ProductActivityThumbnailImgAliasPath = "/product-activity-thumbnail-image/"

// ExtJPG used for extension jpg
var ExtJPG = ".jpg"

// ExtJPEG used for extension jpeg
var ExtJPEG = ".jpeg"

// ExtPNG used for extension png
var ExtPNG = ".png"

// FileTypeJPG used for file type jpg
var FileTypeJPG = "image/jpg"

// FileTypeJPEG used for file type jpeg
var FileTypeJPEG = "image/jpeg"

// FileTypePNG used for file type png
var FileTypePNG = "image/png"

// RoleInvestor used for string value equal to investor
var RoleInvestor = "investor"

// RoleSuperadmin used for string value equal to superadmin
var RoleSuperadmin = "superadmin"

// DefaultIsDelete used for string value equal to is delete 0
var DefaultIsDelete = "0"

// DefaultIsDeleted used for string value equal to is delete 1
var DefaultIsDeleted = "1"

// DefaultTransactionTypeProductAyoTani used for default value for transaction type ayo tani product
var DefaultTransactionTypeProductAyoTani = "0"

// DefaultIsAvailableProduct used for default value for availability product
var DefaultIsAvailableProduct = "yes"

// DefaultIsNotAvailableProduct used for default value for not availability product
var DefaultIsNotAvailableProduct = "no"

// ConfigGmailAppPassword used for app password for gmail
const ConfigGmailAppPassword = "lcegfmmtttrtlsse"

// ConfigSMTPHost used for smtp gmail host
const ConfigSMTPHost = "smtp.gmail.com"

// ConfigSMTPPort used for smtp gmail port
const ConfigSMTPPort = 587

// ConfigEmail used for email
const ConfigEmail = "ktbi.corporation@gmail.com"

// GenerateRandomNumber used for generate random number
func GenerateRandomNumber(length int) string {
	table := [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

	b := make([]byte, length)
	n, err := io.ReadAtLeast(rand.Reader, b, length)

	if n != length {
		panic(err)
	}

	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}

	return string(b)
}

// GetFileContentTypeViaFormFile used for get file type
func GetFileContentTypeViaFormFile(file multipart.File) string {
	str := ""

	if file != nil {
		// Create a buffer to store the header of the file in
		fileHeader := make([]byte, 512)

		// Copy the headers into the FileHeader buffer
		if _, errRead := file.Read(fileHeader); errRead != nil {
			panic(errRead.Error)
		}

		// set position back to start.
		if _, errSeek := file.Seek(0, 0); errSeek != nil {
			panic(errSeek)
		}

		str = http.DetectContentType(fileHeader)
	}

	return str
}

// GetFileContentTypeViaOsFile used for get file type
func GetFileContentTypeViaOsFile(out *os.File) (string, error) {

	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err := out.Read(buffer)
	if err != nil {
		return "", err
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}

// ParseString used for convert int to string
func ParseString(value int) string {
	parsedValue := strconv.Itoa(value)

	return parsedValue
}

// ParseInt used for convert string to int
func ParseInt(value string) int {
	parsedValue, err := strconv.Atoi(value)

	if err != nil {
		parsedValue = 0
	}

	return parsedValue
}

// ParseFloat64 used for convert string to float64
func ParseFloat64(value string) float64 {
	parsedValue, err := strconv.ParseFloat(value, 64)

	if err != nil {
		parsedValue = 0
	}

	return parsedValue
}

// Logger used for log any string to file
func Logger(msg string) {
	currentDate := time.Now().Format("02-Jan-2006")
	currentDatetime := time.Now().Format("02-Jan-2006 15:04:05")

	logFolder := GetRootDirectory() + "/log/"
	logFile := GetRootDirectory() + "/log/ayotani-" + currentDate + ".txt"

	if _, errFolderNotExist := os.Stat(logFolder); os.IsNotExist(errFolderNotExist) {
		os.MkdirAll(logFolder, 0755)
	}

	if !FileExists(logFile) {
		fCreate, err := os.Create(logFile)

		if err != nil {
			fmt.Println(err)
			return
		}

		fCreate.Close()
	}

	f, errOpenFile := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY, 0644)
	HandleError(errOpenFile, "")
	defer f.Close()

	newLine := currentDatetime + "	" + msg
	fmt.Fprintln(f, newLine)
}

// FileExists used for to check if a file exist
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// GetRootDirectory untuk mengambil path root project
func GetRootDirectory() string {
	directory, err := os.Getwd()
	HandleError(err, "")

	return directory
}

// SetRouteMethod untuk set method on spesific function in route
func SetRouteMethod(m string, r *http.Request, limiter *rate.Limiter) {
	if m != r.Method {
		panic("Method not allowed, expected " + m + " found " + r.Method)
	}

	if limiter.Allow() == false {
		panic("Too many request")
	}
}

// CreateErrorResponse used for return struct for custom error response
func CreateErrorResponse(responseCode int, responseMessage string, responseDetailMessage string) models.ErrResGeneralJSON {
	response := models.ErrResGeneralJSON{
		Code:          responseCode,
		Message:       responseMessage,
		DetailMessage: responseDetailMessage,
	}

	return response
}

// CatchErrJSON untuk catch error yang return nya json
func CatchErrJSON(w http.ResponseWriter) {
	if r := recover(); r != nil {
		if reflect.ValueOf(r).Kind().String() == "struct" {
			data := r.(models.ErrResGeneralJSON)

			Logger(data.Message)

			errResponse := models.ErrResGeneralJSON{
				Code:          data.Code,
				Message:       data.Message,
				DetailMessage: data.DetailMessage,
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(errResponse)
		} else {
			errMessage := fmt.Sprintf("%v", r)
			errDetailMessage := fmt.Sprintf("Recovered from : %v", r)

			Logger(errMessage)

			errResponse := models.ErrResGeneralJSON{
				Code:          http.StatusBadRequest,
				Message:       errMessage,
				DetailMessage: errDetailMessage,
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(errResponse)
		}

		return
	}
}

// CatchErrJSONWithDbTransaction untuk catch error yang return nya json dan ada dbTransactionnya
func CatchErrJSONWithDbTransaction(w http.ResponseWriter, transaction *sql.Tx) {
	if r := recover(); r != nil {
		transaction.Rollback()

		errorMessage := fmt.Sprintf("%v", r)

		Logger(errorMessage)

		if reflect.ValueOf(r).Kind().String() == "struct" {
			data := r.(models.ErrResGeneralJSON)

			errResponse := models.ErrResGeneralJSON{
				Code:          data.Code,
				Message:       data.Message,
				DetailMessage: data.DetailMessage,
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(errResponse)
		} else {
			errResponse := models.ErrResGeneralJSON{
				Code:          http.StatusBadRequest,
				Message:       errorMessage,
				DetailMessage: errorMessage,
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(errResponse)
		}
	}
}

// HandleError untuk handle common error
func HandleError(err error, msg string) {
	if err != sql.ErrNoRows {
		if msg != "" || len(msg) > 0 {
			if err != nil {
				panic(msg)
			}
		} else {
			if err != nil {
				panic(err.Error())
			}
		}
	}
}

// ThrowError used for throw panic, even when there are no error
func ThrowError(msg string) {
	if msg != "" || len(msg) > 0 {
		panic(msg)
	}
}

// GenerateUID untuk langsung dapat string UID
func GenerateUID() string {
	uuidgen, err := exec.Command("uuidgen").Output()

	if err != nil {
		panic(err.Error)
	}

	uuidStr := strings.TrimSpace(fmt.Sprintf("%s", uuidgen))

	return uuidStr
}

// HashPassword untuk hash plain password
func HashPassword(password string) string {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)

	if err != nil {
		panic(err.Error())
	}

	return string(bytes)
}

// ComparePassword untuk compare plain pass dengan hashed pass
func ComparePassword(hashedPwd string, plainPwd []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

// Encrypt untuk encrypt string, usage -> ciphertext := utils.Encrypt([]byte("Hello World"), "password")
func Encrypt(data []byte, passphrase string) []byte {
	block, _ := aes.NewCipher([]byte(createHash(passphrase)))
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext
}

// Decrypt untuk decrypt string, usage -> plaintext := utils.Decrypt(ciphertext, "password")
func Decrypt(data []byte, passphrase string) []byte {
	if len(data) < 1 {
		uidStr := GenerateUID()

		return []byte(uidStr)
	}

	key := []byte(createHash(passphrase))
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}

	return plaintext
}

// UploadImage used for upload ktp image
func UploadImage(file multipart.File, handler *multipart.FileHeader, imgPath string, imgName string, aliasPath string) string {
	var fileResizedLocation string

	filename := imgName
	fileLocation := filepath.Join(imgPath, filename)

	if _, err := os.Stat(fileLocation); os.IsNotExist(err) {
		os.MkdirAll(imgPath, 0755)
	}

	targetFile, errOpenFile := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0777)
	HandleError(errOpenFile, "")

	defer targetFile.Close()

	if _, err := io.Copy(targetFile, file); err != nil {
		HandleError(err, "")
	}

	uploadedFile, errOpenFile := os.Open(fileLocation)
	HandleError(errOpenFile, "")

	if filepath.Ext(handler.Filename) == ".jpeg" {
		fileResizedLocation = HandleJpegImage(uploadedFile, imgPath, filename, aliasPath)
	} else if filepath.Ext(handler.Filename) == ".jpg" {
		fileResizedLocation = HandleJpegImage(uploadedFile, imgPath, filename, aliasPath)
	} else if filepath.Ext(handler.Filename) == ".png" {
		fileResizedLocation = HandlePngImage(uploadedFile, imgPath, filename, aliasPath)
	}

	file.Close()

	return fileResizedLocation
}

// HandleJpegImage used for handle uploaded file with extension jpeg
func HandleJpegImage(uploadedFile *os.File, imgPath string, filename string, aliasPath string) string {
	img, errJpegDecode := jpeg.Decode(uploadedFile)
	HandleError(errJpegDecode, "")

	uploadedFile.Close()

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	m := resize.Resize(1000, 0, img, resize.Bicubic)

	fileResizedLocation := filepath.Join(imgPath, filename)
	// aliasFileResizedLocation := filepath.Join(aliasPath, filename)
	aliasFileResizedLocation := filename

	out, errOsCreate := os.Create(fileResizedLocation)
	HandleError(errOsCreate, "")

	defer out.Close()

	// write new image to file
	jpeg.Encode(out, m, nil)

	return aliasFileResizedLocation
}

// HandlePngImage used for handle uploaded file with extension jpeg
func HandlePngImage(uploadedFile *os.File, imgPath string, filename string, aliasPath string) string {
	img, errJpegDecode := png.Decode(uploadedFile)
	HandleError(errJpegDecode, "")

	uploadedFile.Close()

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	m := resize.Resize(1000, 0, img, resize.Bicubic)

	fileResizedLocation := filepath.Join(imgPath, filename)
	// aliasFileResizedLocation := filepath.Join(aliasPath, filename)
	aliasFileResizedLocation := filename

	out, errOsCreate := os.Create(fileResizedLocation)
	HandleError(errOsCreate, "")

	defer out.Close()

	// write new image to file
	png.Encode(out, m)

	return aliasFileResizedLocation
}

// DeleteImage used for delete exist image
func DeleteImage(path string) {
	err := os.Remove(path)
	HandleError(err, "")
}

// UploadImage used for upload image
// func UploadImage(file multipart.File, handler *multipart.FileHeader, imgPath string, imgName string) string {
// 	filename := imgName
// 	fullImgPath := GetRootDirectory() + imgPath
// 	fileLocation := filepath.Join(fullImgPath, filename)

// 	if _, err := os.Stat(fileLocation); os.IsNotExist(err) {
// 		os.Mkdir(fullImgPath, 0755)
// 	}

// 	targetFile, errOpenFile := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0777)
// 	HandleError(errOpenFile, "")

// 	defer targetFile.Close()

// 	if _, err := io.Copy(targetFile, file); err != nil {
// 		HandleError(err, "")
// 	}

// 	uploadedFile, errOpenFile := os.Open(fileLocation)
// 	img, errJpegDecode := png.Decode(uploadedFile)

// 	HandleError(errOpenFile, "")
// 	HandleError(errJpegDecode, "")

// 	uploadedFile.Close()

// 	// resize to width 1000 using Lanczos resampling
// 	// and preserve aspect ratio
// 	m := resize.Resize(1000, 0, img, resize.Bicubic)

// 	fileResizedLocation := filepath.Join(fullImgPath, filename)

// 	out, errOsCreate := os.Create(fileResizedLocation)
// 	HandleError(errOsCreate, "")

// 	defer out.Close()

// 	// write new image to file
// 	png.Encode(out, m)

// 	file.Close()

// 	return imgPath + filename
// }
