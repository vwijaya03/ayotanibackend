package utils

import (
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

type validateStruct interface {
}

// Validate untuk validasi data dengan kriteria tertentu
func Validate(dataStruct interface{}) {
	val := reflect.ValueOf(dataStruct)

	for i := 0; i < val.Type().NumField(); i++ {
		// fmt.Println(val.Type().Field(i).Tag.Get("validate"))
		//fmt.Println(val.FieldByName(val.Type().Field(i).Name), val.Type().Field(i).Name, val.Type().Field(i).Tag.Get("json"))

		rules := strings.Split(val.Type().Field(i).Tag.Get("validate"), ",")

		for _, v := range rules {
			if strings.Contains(v, "required") {
				// nilai := val.FieldByName(val.Type().Field(i).Name)

				if val.Field(i).Type() == reflect.TypeOf("") {
					str := val.Field(i).Interface().(string)

					if len(str) < 1 || str == "" {
						panic(val.Type().Field(i).Name + " tidak boleh kosong")
					}
				} else if val.Field(i).Type() == reflect.TypeOf(0) {
					value := val.Field(i).Interface().(int)

					if value == 0 {
						panic(val.Type().Field(i).Name + " tidak boleh kosong")
					}
				} else if val.Field(i).Type() == reflect.TypeOf(1.2) {
					value := val.Field(i).Interface().(float64)

					if value == 0 {
						panic(val.Type().Field(i).Name + " tidak boleh kosong")
					}
				}
			}

			if strings.Contains(v, "min:") {
				if val.Field(i).Type() == reflect.TypeOf("") {
					splitMin := strings.Split(v, ":")
					splitMinVal, err := strconv.Atoi(splitMin[1])
					str := val.Field(i).Interface().(string)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					if len(str) < splitMinVal {
						panic("Minimum karakter " + val.Type().Field(i).Name + " adalah " + splitMin[1])
					}
				} else {
					panic("Terjadi kesalahan validate " + val.Type().Field(i).Name + ", func min hanya untuk string")
				}
			}

			if strings.Contains(v, "max:") {
				if val.Field(i).Type() == reflect.TypeOf("") {
					splitMax := strings.Split(v, ":")
					splitMaxVal, err := strconv.Atoi(splitMax[1])
					str := val.Field(i).Interface().(string)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					if len(str) > splitMaxVal {
						panic("Maksimum karakter " + val.Type().Field(i).Name + " adalah " + splitMax[1])
					}
				} else {
					panic("Terjadi kesalahan validate " + val.Type().Field(i).Name + ", func max hanya untuk string")
				}
			}

			if strings.Contains(v, "email") {
				if val.Field(i).Type() == reflect.TypeOf("") {
					str := val.Field(i).Interface().(string)
					regex := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)

					if !regex.MatchString(str) {
						panic("Format email salah")
					}
				} else {
					panic("Terjadi kesalahan validate " + val.Type().Field(i).Name + ", func email hanya untuk string")
				}
			}

			if strings.Contains(v, "gte:") {
				if val.Field(i).Type() == reflect.TypeOf(0) {
					splitGte := strings.Split(v, ":")
					splitGteVal, err := strconv.Atoi(splitGte[1])
					value := val.Field(i).Interface().(int)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					defMinValue := splitGteVal + 1
					strDefMinValue := strconv.Itoa(defMinValue)

					if len(strDefMinValue) < 1 {
						HandleError(err, "default value strDefMinValue empty")
					}

					if value <= splitGteVal {
						panic("Minimum nilai " + val.Type().Field(i).Name + " adalah " + strDefMinValue)
					}
				} else if val.Field(i).Type() == reflect.TypeOf(1.2) {
					splitGte := strings.Split(v, ":")
					splitGteVal, err := strconv.ParseFloat(splitGte[1], 64)
					value := val.Field(i).Interface().(float64)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					defMinValue := splitGteVal + 1
					strDefMinValue := strconv.Itoa(int(defMinValue))

					if len(strDefMinValue) < 1 {
						HandleError(err, "default value strDefMinValue empty")
					}

					if value <= splitGteVal {
						panic("Minimum nilai " + val.Type().Field(i).Name + " adalah " + strDefMinValue)
					}
				} else {
					panic("Terjadi kesalahan validate " + val.Type().Field(i).Name + ", func gte hanya untuk int")
				}
			}

			if strings.Contains(v, "gt:") {
				if val.Field(i).Type() == reflect.TypeOf(0) {
					splitGte := strings.Split(v, ":")
					splitGteVal, err := strconv.Atoi(splitGte[1])
					value := val.Field(i).Interface().(int)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					if value < splitGteVal {
						panic("Minimum nilai " + val.Type().Field(i).Name + " adalah " + splitGte[1])
					}
				} else if val.Field(i).Type() == reflect.TypeOf(1.2) {
					splitGte := strings.Split(v, ":")
					splitGteVal, err := strconv.ParseFloat(splitGte[1], 64)
					value := val.Field(i).Interface().(float64)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					if value < splitGteVal {
						panic("Minimum nilai " + val.Type().Field(i).Name + " adalah " + splitGte[1])
					}
				} else {
					panic("Terjadi kesalahan validate " + val.Type().Field(i).Name + ", func gt hanya untuk int")
				}
			}

			if strings.Contains(v, "lte:") {
				if val.Field(i).Type() == reflect.TypeOf(0) {
					splitLte := strings.Split(v, ":")
					splitLteVal, err := strconv.Atoi(splitLte[1])
					value := val.Field(i).Interface().(int)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					defMinValue := splitLteVal - 1
					strDefMaxValue := strconv.Itoa(defMinValue)

					if len(strDefMaxValue) < 1 {
						HandleError(err, "default value strDefMaxValue empty")
					}

					if value >= splitLteVal {
						panic("Maximum nilai " + val.Type().Field(i).Name + " adalah " + strDefMaxValue)
					}
				} else if val.Field(i).Type() == reflect.TypeOf(1.2) {
					splitLte := strings.Split(v, ":")
					splitLteVal, err := strconv.ParseFloat(splitLte[1], 64)
					value := val.Field(i).Interface().(float64)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					defMinValue := splitLteVal - 1
					strDefMaxValue := strconv.Itoa(int(defMinValue))

					if len(strDefMaxValue) < 1 {
						HandleError(err, "default value strDefMaxValue empty")
					}

					if value >= splitLteVal {
						panic("Maximum nilai " + val.Type().Field(i).Name + " adalah " + strDefMaxValue)
					}
				} else {
					panic("Terjadi kesalahan validate " + val.Type().Field(i).Name + ", func lte hanya untuk int")
				}
			}

			if strings.Contains(v, "lt:") {
				if val.Field(i).Type() == reflect.TypeOf(0) {
					splitLte := strings.Split(v, ":")
					splitLteVal, err := strconv.Atoi(splitLte[1])
					value := val.Field(i).Interface().(int)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					if value > splitLteVal {
						panic("Maximum nilai " + val.Type().Field(i).Name + " adalah " + splitLte[1])
					}
				} else if val.Field(i).Type() == reflect.TypeOf(1.2) {
					splitLte := strings.Split(v, ":")
					splitLteVal, err := strconv.ParseFloat(splitLte[1], 64)
					value := val.Field(i).Interface().(float64)

					HandleError(err, "nilai validate min dari model "+val.Type().Field(i).Name+" tidak boleh kosong")

					if value > splitLteVal {
						panic("Maximum nilai " + val.Type().Field(i).Name + " adalah " + splitLte[1])
					}
				} else {
					panic("Terjadi kesalahan validate " + val.Type().Field(i).Name + ", func lt hanya untuk int")
				}
			}

			if strings.Contains(v, "img") {
				allowedExtensions := []string{".png", ".jpg", ".jpeg"}
				str := val.Field(i).Interface().(string)
				isError := true

				for _, val := range allowedExtensions {
					if val == str {
						isError = false
						break
					}
				}

				if isError {
					panic("Format gambar salah")
				}
			}

			if strings.Contains(v, "imageType") {
				allowedImgType := []string{FileTypeJPEG, FileTypeJPG, FileTypePNG}
				str := val.Field(i).Interface().(string)
				isError := true

				for _, val := range allowedImgType {
					if val == str {
						isError = false
						break
					}
				}

				if isError {
					panic("Format gambar salah")
				}
			}
		}
	}
}
