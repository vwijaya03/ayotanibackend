package utils

import (
	"database/sql"
	"fmt"

	"gopkg.in/gomail.v2"
)

// DefaultFromForEmail used for default from for send email
var DefaultFromForEmail = "AyoTani <support@ayotani.co.id>"

// SendEmailResetPassword used for send email for reset password, verified account and etc
func SendEmailResetPassword(transaction *sql.Tx, from string, to string, subject string, message string, password []byte, salt string) {
	dialer := gomail.NewDialer(
		ConfigSMTPHost,
		ConfigSMTPPort,
		ConfigEmail,
		ConfigGmailAppPassword,
	)

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", from)
	//mailer.SetHeader("To", "recipient1@gmail.com", "emaillain@gmail.com")
	mailer.SetHeader("To", to)
	//mailer.SetAddressHeader("Cc", "tralalala@gmail.com", "Tra Lala La")
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", message)
	//mailer.Attach("./sample.png")

	_, errChangePasswordInvestor := transaction.Exec(`UPDATE "atn_users" SET "password" = $1, "salt" = $2 WHERE "email" = $3`, password, salt, to)
	if errChangePasswordInvestor != nil {
		Logger(errChangePasswordInvestor.Error())
		transaction.Rollback()
		return
	}

	errDialer := dialer.DialAndSend(mailer)
	if errDialer != nil {
		Logger(errDialer.Error())
		transaction.Rollback()
		return
	}

	transaction.Commit()
}

// SendEmailTransaction used for send email for notification that topup transfer or withdrawal has been processed
func SendEmailTransaction(fullname string, from string, to string, subject string, transactionDateTime string, transactionType string, amount string, transactionUID string, transactionStatus string, action string) {
	dialer := gomail.NewDialer(
		ConfigSMTPHost,
		ConfigSMTPPort,
		ConfigEmail,
		ConfigGmailAppPassword,
	)

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", from)
	mailer.SetHeader("To", to)
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", TransactionHTML(fullname, transactionDateTime, transactionType, amount, transactionUID, transactionStatus, action))

	errDialer := dialer.DialAndSend(mailer)
	if errDialer != nil {
		Logger(errDialer.Error())
		return
	}
}

// TransactionHTML used for email body message with HTML format
func TransactionHTML(fullname string, transactionDateTime string, transactionType string, amount string, transactionUID string, transactionStatus string, action string) string {
	html := fmt.Sprintf(`
	<p>Halo %s.<br>
	Terima kasih telah melakukan %s, berikut merupakan informasi transaksi yang telah Anda lakukan:
	</p>

	<p>
	Tanggal/Jam					: %s <br>
	Jenis Transaksi				: %s <br>
	Jumlah						: %s <br>
	Kode Referensi				: %s <br>
	Status						: %s <br>
	</p>

	<p>Kami menyarankan Anda untuk menyimpan email ini sebagai referensi dari transaksi AyoTani Anda.<br> 
	Semoga informasi ini bermanfaat bagi Anda.</p>

	<p>Hormat kami,<br>

	PT. Ayo Tani Berjaya</p>
	`, fullname, action, transactionDateTime, transactionType, amount, transactionUID, transactionStatus)

	return html
}

// SendEmailVerifiedAccount used for send email for notification that investor's account has been verified
func SendEmailVerifiedAccount(fullname string, from string, to string, subject string) {
	dialer := gomail.NewDialer(
		ConfigSMTPHost,
		ConfigSMTPPort,
		ConfigEmail,
		ConfigGmailAppPassword,
	)

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", from)
	mailer.SetHeader("To", to)
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", VerifiedAccountHTML(fullname))

	errDialer := dialer.DialAndSend(mailer)
	if errDialer != nil {
		Logger(errDialer.Error())
		return
	}
}

// VerifiedAccountHTML used for email body message with HTML format
func VerifiedAccountHTML(fullname string) string {
	html := fmt.Sprintf(`
	<p>Halo %s.<br>
	Selamat akun AyoTani kamu telah diverifikasi. Sekarang kamu bisa melakukan transaksi yang ada pada Aplikasi AyoTani.
	</p>
	`, fullname)

	return html
}

// SendEmailShareInvestment used for send email for notification that unit investment already shared to investor
func SendEmailShareInvestment(fullname string, from string, to string, subject string, transactionDateTime string, transactionType string, unitName string, unitQuantity string, fund string, totalFundInvestmentResult string, roi string, roiHPanen string, transactionUID string, transactionStatus string) {
	dialer := gomail.NewDialer(
		ConfigSMTPHost,
		ConfigSMTPPort,
		ConfigEmail,
		ConfigGmailAppPassword,
	)

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", from)
	mailer.SetHeader("To", to)
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", ShareInvestmentHTML(fullname, transactionDateTime, transactionType, unitName, unitQuantity, fund, totalFundInvestmentResult, roi, roiHPanen, transactionUID, transactionStatus))

	errDialer := dialer.DialAndSend(mailer)
	if errDialer != nil {
		Logger(errDialer.Error())
		return
	}
}

// ShareInvestmentHTML used for email body share investment message with HTML format
func ShareInvestmentHTML(fullname string, transactionDateTime string, transactionType string, unitName string, unitQuantity string, fund string, totalFundInvestmentResult string, roi string, roiHPanen string, transactionUID string, transactionStatus string) string {
	html := fmt.Sprintf(`
	<p>Halo %s.<br>
	Berikut merupakan informasi hasil akhir investasi dari AyoTani:
	</p>

	<p>
	Tanggal/Jam					: %s <br>
	Jenis Transaksi				: %s <br>
	Nama Unit					: %s <br>
	Jumlah Unit					: %s <br>
	Modal Yang Disetor			: %s <br>
	Hasil Akhir Investasi		: %s <br>
	ROI							: %s <br>
	ROI H - Panen				: %s <br>
	Kode Referensi				: %s <br>
	Status						: %s <br>
	</p>

	<p>Kami menyarankan Anda untuk menyimpan email ini sebagai referensi dari transaksi AyoTani Anda.<br> 
	Semoga informasi ini bermanfaat bagi Anda.</p>

	<p>Hormat kami,<br>

	PT. Ayo Tani Berjaya</p>
	`, fullname, transactionDateTime, transactionType, unitName, unitQuantity, fund, totalFundInvestmentResult, roi, roiHPanen, transactionUID, transactionStatus)

	return html
}
