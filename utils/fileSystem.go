package utils

import (
	"net/http"
	"strings"
)

// NeuteredFileSystem as struct
type NeuteredFileSystem struct {
	Fs http.FileSystem
}

// Open used for hide list of files if file not found
func (nfs NeuteredFileSystem) Open(path string) (http.File, error) {
	f, err := nfs.Fs.Open(path)
	if err != nil {
		return nil, err
	}

	s, err := f.Stat()
	if s.IsDir() {
		index := strings.TrimSuffix(path, "/") + "/index.html"
		if _, err := nfs.Fs.Open(index); err != nil {
			return nil, err
		}
	}

	return f, nil
}
