package utils

import (
	"context"
	"fmt"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"google.golang.org/api/option"
)

// SendNotification used for send FCM notification to spesific device or subscribed topic
func SendNotification(title string, body string, image string, deviceToken string, topic string) {
	dir := GetRootDirectory() + "/assets/google_application_credentials/ayotani-8dee8-firebase-adminsdk-3wzmu-cc3373ef6a.json"

	opt := option.WithCredentialsFile(dir)
	app, errInitialize := firebase.NewApp(context.Background(), nil, opt)

	if errInitialize != nil {
		Logger(fmt.Sprintf("error initializing app: %v\n", errInitialize))
	}

	// Obtain a messaging.Client from the App.
	ctx := context.Background()
	client, errClient := app.Messaging(ctx)

	if errClient != nil {
		Logger(fmt.Sprintf("error getting Messaging client: %v\n", errClient))
	}

	message := &messaging.Message{}

	if deviceToken != "" || len(deviceToken) > 0 {
		message = &messaging.Message{
			Token: deviceToken,
			Notification: &messaging.Notification{
				Title: title,
				Body:  body,
			},
		}
	}

	if topic != "" || len(topic) > 0 {
		message = &messaging.Message{
			Topic: topic,
			Notification: &messaging.Notification{
				Title: title,
				Body:  body,
			},
		}
	}

	response, errClientSend := client.Send(ctx, message)
	if errClientSend != nil {
		Logger(errClientSend.Error())
	}

	// Response is a message ID string.
	Logger("Successfully sent message: " + response)
}
