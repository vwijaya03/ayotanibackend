package utils

// ACLAll used for allow all role to access
var ACLAll = []string{"admin", "investor", "superadmin"}

// ACLAS used for allow admin & superadmin role to access
var ACLAS = []string{"admin", "superadmin"}

// ACLAdmin used for allow role admin to access
var ACLAdmin = []string{"admin"}

// ACLInvestor used for allow role investor to access
var ACLInvestor = []string{"investor"}

// ACLSuperadmin used for allow role superadmin to access
var ACLSuperadmin = []string{"superadmin"}

// ACLPublic used for without role to access
var ACLPublic = []string{}

// ACLGetInvestor used for acl API get investor
var ACLGetInvestor = []string{"superadmin", "admin"}

// ACLPostAddInvestor used for acl API post add investor
var ACLPostAddInvestor = []string{"superadmin", "admin"}

// ACLPostEditInvestor used for acl API post edit investor
var ACLPostEditInvestor = []string{"superadmin", "admin"}

// ACLPostDeleteInvestor used for acl API post delete investor
var ACLPostDeleteInvestor = []string{"superadmin", "admin"}

// ACLGetSuperadmin used for acl API get superadmin
var ACLGetSuperadmin = []string{"superadmin"}

// ACLPostAddSuperadmin used for acl API post add superadmin
var ACLPostAddSuperadmin = []string{"superadmin"}

// ACLPostEditSuperadmin used for acl API post edit superadmin
var ACLPostEditSuperadmin = []string{"superadmin"}

// ACLPostDeleteSuperadmin used for acl API post delete superadmin
var ACLPostDeleteSuperadmin = []string{"superadmin"}
