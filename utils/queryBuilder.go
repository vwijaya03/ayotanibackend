package utils

import (
	"ayotani/models"
	"fmt"
	"strings"
)

// type pair struct {
// 	Key string
// 	Value string
//  }

//  mapPagination := []pair{{Key:`OFFSET`,Value:offset},
// 	{Key:`ROWS FETCH FIRST`,Value: limit},
// 	{Key:`ROW ONLY:keyOnly`}}

// SelectQueryBuilder used for combine WHERE, ORDER BY and OFFSET ROWS FETCH FIRST ROW ONLY Query
func SelectQueryBuilder(where []models.QueryBuilder, orderBy []models.QueryBuilder, pagination []models.QueryBuilder) (string, []interface{}) {
	var (
		index                                                            int    = 1
		fullQueryBuilder, multipleLikeQueryBuilder                       string = "", ""
		values                                                           []interface{}
		slicesWhere, slicesMultipleLike, slicesOrderBy, slicesPagination []string
	)

	if len(where) > 0 {
		isMultipleLike := false

		for _, vWhere := range where {
			if strings.Contains(vWhere.Key, "like") {
				vWhere.Key = strings.Replace(vWhere.Key, ":like", "", 1)
				slicesWhere = append(slicesWhere, fmt.Sprintf("%s like $%s", vWhere.Key, ParseString(index)))

				fullQueryBuilder = strings.Join(slicesWhere, " AND ")
			} else if strings.Contains(vWhere.Key, "multipleLike") {
				isMultipleLike = true
				vWhere.Key = strings.Replace(vWhere.Key, ":multipleLike", "", 1)

				slicesMultipleLike = append(slicesMultipleLike, fmt.Sprintf("%s like $%s", vWhere.Key, ParseString(index)))
				multipleLikeQueryBuilder = strings.Join(slicesMultipleLike, " OR ")
			} else {
				slicesWhere = append(slicesWhere, fmt.Sprintf("%s = $%s", vWhere.Key, ParseString(index)))

				fullQueryBuilder = strings.Join(slicesWhere, " AND ")
			}

			values = append(values, vWhere.Value)

			index++
		}

		if isMultipleLike {
			fullQueryBuilder = fullQueryBuilder + fmt.Sprintf(" AND (%s) ", multipleLikeQueryBuilder)
		}

		// fmt.Println(slicesMultipleLike)
		// fmt.Println(multipleLikeQueryBuilder)
		// fmt.Println(fullQueryBuilder)
	}

	if len(orderBy) > 0 {
		for _, vOrderBy := range orderBy {
			if strings.Contains(vOrderBy.Key, "keyOnly") {
				vOrderBy.Key = strings.Replace(vOrderBy.Key, ":keyOnly", "", 1)
				slicesOrderBy = append(slicesOrderBy, fmt.Sprintf("%s", vOrderBy.Key))
			} else {
				slicesOrderBy = append(slicesOrderBy, fmt.Sprintf("%s $%s", vOrderBy.Key, ParseString(index)))
				values = append(values, vOrderBy.Value)

				index++
			}
		}

		fullQueryBuilder = fullQueryBuilder + " "
		fullQueryBuilder = fullQueryBuilder + strings.Join(slicesOrderBy, " ")
	}

	if len(pagination) > 0 {
		for _, vPagination := range pagination {
			if strings.Contains(vPagination.Key, "keyOnly") {
				vPagination.Key = strings.Replace(vPagination.Key, ":keyOnly", "", 1)
				slicesPagination = append(slicesPagination, fmt.Sprintf("%s", vPagination.Key))
			} else {
				slicesPagination = append(slicesPagination, fmt.Sprintf("%s $%s", vPagination.Key, ParseString(index)))
				values = append(values, vPagination.Value)
			}

			index++
		}

		fullQueryBuilder = fullQueryBuilder + " "
		fullQueryBuilder = fullQueryBuilder + strings.Join(slicesPagination, " ")
	}

	return fullQueryBuilder, values
}
