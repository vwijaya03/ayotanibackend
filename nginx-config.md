#user  nobody;
# you must set worker processes based on your CPU cores, nginx does not benefit from setting more than that
worker_processes  auto; #some last versions calculate it automatically

# number of file descriptors used for nginx
# the limit for the maximum FDs on the server is usually set by the OS.
# if you don't set FD's then OS settings will be used which is by default 2000
#worker_rlimit_nofile 100000;
worker_rlimit_nofile 2000;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

pid		/var/run/nginx.pid;

# provides the configuration file context in which the directives that affect connection processing are specified.
events {
	# determines how much clients will be served per worker
    # max clients = worker_connections * worker_processes
    # max clients is also limited by the number of socket connections available on the system (~64k)
    # worker_connections 4000;
    worker_connections 2000;

    # optimized to serve many clients with each thread, essential for linux -- for testing environment
    #use epoll;

    # accept as many connections as possible, may flood worker connections if set too low -- for testing environment
    #multi_accept on;
}

error_log /usr/local/apps/nginx/var/log/error_log debug;

http {
	# cache informations about FDs, frequently accessed files
    # can boost performance, but you need to test those values
    # open_file_cache max=200000 inactive=20s;
    open_file_cache max=1000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    # to boost I/O on HDD we can disable access logs
    access_log off;

	include	/usr/local/apps/nginx/etc/mime.types;
	default_type  application/octet-stream;

	log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
					  '$status $body_bytes_sent "$http_referer" '
					  '"$http_user_agent" "$http_x_forwarded_for"';

	access_log /usr/local/apps/nginx/var/log/web.access.log  main;

	# Client max body size for data size requested to the web server
	client_max_body_size 50M;

	# if the request body size is more than the buffer size, then the entire (or partial)
	# request body is written into a temporary file
	client_body_buffer_size  128k;

	# buffer size for reading client request header -- for testing environment
	#client_header_buffer_size 3m;
	client_header_buffer_size 1m;

	# maximum number and size of buffers for large headers to read from client request
	large_client_header_buffers 4 256k;

	# read timeout for the request body from client -- for testing environment
	#client_body_timeout   3m;
	client_body_timeout   1m;

	# how long to wait for the client to send a request header -- for testing environment
	#client_header_timeout 3m;
	client_header_timeout 1m;

	# copies data between one FD and other from within the kernel
    # faster than read() + write()
	sendfile		on;

	# send headers in one piece, it is better than sending them one by one
	tcp_nopush	 on;

	# don't buffer data sent, good for small data bursts in real time
    tcp_nodelay on;

	# reduce the data that needs to be sent over network -- for testing environment
    gzip on;
	
	# gzip_static on; 
    # gzip_min_length 10240; # default 20
    gzip_min_length 20;
    gzip_comp_level 1;
    gzip_vary on;
    gzip_disable msie6;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types
        # text/html is always compressed by HttpGzipModule
        text/css
        text/javascript
        text/xml
        text/plain
        text/x-component
        application/javascript
        application/x-javascript
        application/json
        application/xml
        application/rss+xml
        application/atom+xml
        font/truetype
        font/opentype
        application/vnd.ms-fontobject
        image/svg+xml;
	
	# allow the server to close connection on non responding client, this will free up memory
    reset_timedout_connection on;

    # if client stop responding, free up memory -- default 60
    send_timeout 60;

	# server will close connection after this time -- default 60
	keepalive_timeout  60;

	# number of requests client can make over keep-alive -- for testing environment
    #keepalive_requests 100000;
	
	# If your domain names are long, increase this parameter.
	server_names_hash_bucket_size 64;
	
	# To hide the version number in headers
	server_tokens off;
	
	# limit the number of connections per single IP
	limit_conn_zone $binary_remote_addr zone=conn_limit_per_ip:10m;

	# limit the number of requests for a given session
	limit_req_zone $binary_remote_addr zone=req_limit_per_ip:10m rate=50r/s;

	server {
		listen 80;

		server_name		ayotani.co.id www.ayotani.co.id;
    	return 301 https://ayotani.co.id$request_uri;
	}

	server {
		listen 80;

		server_name		admin.ayotani.co.id www.admin.ayotani.co.id;
    	return 301 https://admin.ayotani.co.id$request_uri;
	}

	server {
		listen 80;

		server_name		dev-cp.ayotani.co.id www.dev-cp.ayotani.co.id;
    	root /opt/ayotani/public;

		add_header X-Frame-Options "SAMEORIGIN";
		add_header X-XSS-Protection "1; mode=block";
		add_header X-Content-Type-Options "nosniff";

		index index.html index.htm index.php;

		charset utf-8;

		location / {
			try_files $uri $uri/ /index.php?$query_string;
		}

		location = /favicon.ico { access_log off; log_not_found off; }
		location = /robots.txt  { access_log off; log_not_found off; }

		error_page 404 /index.php;

		location ~ \.php$ {
			fastcgi_pass unix:/usr/local/apps/php73/var/php73_9001.sock;
			#fastcgi_index index.php; # commented because to prevent download file index.php if path not found
			fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
			include fastcgi_params;
		}

		location ~ /\.(?!well-known).* {
			deny all;
		}
	}

	server {
		listen 443 ssl;

		# ssl on; # deprecated
		ssl_certificate /etc/letsencrypt/live/ayotani.co.id/fullchain.pem;
		ssl_certificate_key /etc/letsencrypt/live/ayotani.co.id/privkey.pem;
		
		limit_conn conn_limit_per_ip 10;
    	limit_req zone=req_limit_per_ip burst=5 nodelay;

		server_name		ayotani.co.id www.ayotani.co.id;
		root /opt/ayotani/public;

		add_header X-Frame-Options "SAMEORIGIN";
		add_header X-XSS-Protection "1; mode=block";
		add_header X-Content-Type-Options "nosniff";

		index index.html index.htm index.php;

		charset utf-8;

		location / {
			try_files $uri $uri/ /index.php?$query_string;
		}

		location = /favicon.ico { access_log off; log_not_found off; }
		location = /robots.txt  { access_log off; log_not_found off; }

		error_page 404 /index.php;

		location ~ \.php$ {
			fastcgi_pass unix:/usr/local/apps/php73/var/php73_9001.sock;
			#fastcgi_index index.php; # commented because to prevent download file index.php if path not found
			fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
			include fastcgi_params;
		}

		location ~ /\.(?!well-known).* {
			deny all;
		}

		location /s/a {
			proxy_pass http://localhost:8080;
		}
	}

	server {
		listen 443 ssl;

		# ssl on; # deprecated
		ssl_certificate /etc/letsencrypt/live/admin.ayotani.co.id/fullchain.pem;
		ssl_certificate_key /etc/letsencrypt/live/admin.ayotani.co.id/privkey.pem;
		
		limit_conn conn_limit_per_ip 10;
    	limit_req zone=req_limit_per_ip burst=10 nodelay;

		server_name		admin.ayotani.co.id	www.admin.ayotani.co.id;
		root /opt/ayotaniadmin/public;

		add_header X-Frame-Options "SAMEORIGIN";
		add_header X-XSS-Protection "1; mode=block";
		add_header X-Content-Type-Options "nosniff";

		index index.html index.htm index.php;

		charset utf-8;

		location / {
			try_files $uri $uri/ /index.php?$query_string;
		}

		location = /favicon.ico { access_log off; log_not_found off; }
		location = /robots.txt  { access_log off; log_not_found off; }

		error_page 404 /index.php;

		location ~ \.php$ {
			fastcgi_pass unix:/usr/local/apps/php73/var/php73_9001.sock;
			#fastcgi_index index.php; # commented because to prevent download file index.php if path not found
			fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
			include fastcgi_params;
		}

		location ~ /\.(?!well-known).* {
			deny all;
		}
	}

	include /usr/local/apps/nginx/etc/conf.d/*.conf;
}